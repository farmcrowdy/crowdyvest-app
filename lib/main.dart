import 'package:crowdyvest/src/projects/portfolio.dart';
import 'package:flutter/material.dart';
import 'package:async_loader/async_loader.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'src/colors.dart';
import 'src/dashboard/dashboard.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:background_fetch/background_fetch.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:io';
import 'src/engine/values.dart';
import 'src/intro_pages/intro_page.dart';
import 'package:http/http.dart' as http;
import 'src/models/vscontrolpojo.dart';
import 'src/onboard/updateYourApp.dart';
import 'src/notification_pages/redirectToProjectDetails.dart';

void backgroundFetchHeadlessTask() async {
  print('[BackgroundFetch] Headless event received.');
  BackgroundFetch.finish();
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
    BackgroundFetch.registerHeadlessTask(backgroundFetchHeadlessTask);
  });
}

FirebaseAnalytics analytics = FirebaseAnalytics();
class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  static FirebaseAnalyticsObserver observer =
  FirebaseAnalyticsObserver(analytics: analytics);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appName,
      theme: _buildHelloTheme(),
      home: MyHomePage(title: appName, observer: observer, ),
      debugShowCheckedModeBanner: false,
      navigatorObservers: <NavigatorObserver>[observer],
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.observer}) : super(key: key);

  final String title;
  final FirebaseAnalyticsObserver observer;

  @override
  _MyHomePageState createState() => _MyHomePageState(observer);
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  final FirebaseAnalyticsObserver observer;

  _MyHomePageState(this.observer);

  setupPushNotification() {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.getToken().then((token){
      print('My token:  '+token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        final dynamic dataDyn = message['body'] ?? message;
        String data = dataDyn.toString();
        _showItemDialog(data.split("##")[1].substring(2).trim());
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        _navigateToDetail(message);
      },

      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
        _navigateToDetail(message);

      },
    );
     // _firebaseMessaging.subscribeToTopic("open_up");

  }



  _navigateToDetail(Map<String, dynamic> message) {
    final dynamic dataDyn = message['body'] ?? message;
    String data = dataDyn.toString();
   // _showItemDialog(data.split("##")[1].substring(0,6).replaceAll("}", ''));



    Navigator.push(context, MaterialPageRoute(builder: (context) => (data.split("##").length>1)
        ? RedirectToProjectDetails(int.parse(data.split("##")[1].substring(0,7).replaceAll("}", '').replaceAll(",", "")), observer)
        :
    FarmShopPageState(observer)));
  }
  Widget _buildDialog(BuildContext context, dynamic message) {
    return AlertDialog(
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: const Text('CLOSE'),
          onPressed: () {
            Navigator.pop(context, false);
          },
        ),
        FlatButton(
          child: const Text('SHOW'),
          onPressed: () {
            Navigator.pop(context, true);
          },
        ),
      ],
    );
  }

  void _showItemDialog(String message) {
    showDialog<bool>(
      context: context,
      builder: (_) => _buildDialog(context, message),
    ).then((bool shouldNavigate) {
      if (shouldNavigate == true) {
        //_navigateToDetail(message);
      }
    });
  }
  static VsControlPojo _vsControlPojor;

  Future<void> initPlatformState() async {
    // Configure BackgroundFetch.
    BackgroundFetch.configure(BackgroundFetchConfig(
        minimumFetchInterval: 30,
        stopOnTerminate: false,
        enableHeadless: true
    ), () async {

      //if (Platform.isIOS)
        setupPushNotification();
      BackgroundFetch.finish();

    }).then((int status) {
      print('[FC_BackgroundFetch] SUCCESS');
    }).catchError((e) {
      print('[FC_BackgroundFetch] ERROR: $e');
    });

  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(Platform.isIOS)initPlatformState();
    setupPushNotification();
  }


   Future<int>  checkIfShouldReferUpdate() async {
    int action = 0;

    //LINK TO CHECK AND REDIRECT TO FORCE UPDATE, IF NECESSARY.
    await http.get("https://api.sheety.co/d4927a12-3ac8-42fa-9ecd-503eca2936d5").then((response) {
      if(response.statusCode == 200) {
        final vsControlPojo = vsControlPojoFromJson(response.body);
        if(vsControlPojo[0].version > appCurrentVersion) {
          _vsControlPojor = vsControlPojo[0];
          action = 1;
         // return true;
        }
        //else return false;
      }
      //else return false;
    }).catchError((onError) {
      //return false;
    });

    SharedPreferences prefs = await SharedPreferences.getInstance();
    int counter = (prefs.getInt('loginStatus') ?? 0);
    if(counter ==0) {
     action = 2;
    }
    else {
      action = 3;
    }

    return action;
  }

  @override
  Widget build(BuildContext context) {

    Widget resultFromUpdates(int data) {
      switch(data) {
        case 1:
          return UpdateYourApp(_vsControlPojor, observer);
          break;
        case 2:
          var shortestSide = MediaQuery
              .of(context)
              .size
              .shortestSide;
          var useMobileLayout = shortestSide < 600;
          return IntroPageDesign(useMobileLayout, observer);

          break;

        default:
          return DashboardPageState(observer);
          break;
      }
      //return SizedBox(width: 0, height: 0,);
    }

    var _asyncLoader = new AsyncLoader(
      initState: () async => await checkIfShouldReferUpdate(),
      renderLoad: () => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/just_cv_logo.png', width: 160, height: 160,),
            SizedBox(height: 8,),
            Container(
                padding: EdgeInsets.all(26),
                child: Text('Do Good. Earn Well.', style: TextStyle(fontSize: 26, color: greenStart, fontWeight: FontWeight.bold), textAlign: TextAlign.center,))
          ],
        ),
      ),
      renderSuccess: ({data}) => resultFromUpdates(data),
    );


    //checkForLatestAppUpdates(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: _asyncLoader
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

ThemeData _buildHelloTheme() {
  final ThemeData myLightTheme = ThemeData.light();
  return myLightTheme.copyWith(
    accentColor: kShrineBrown900,
    primaryColor:  kShrineBrown900,

    buttonTheme: myLightTheme.buttonTheme.copyWith(
      buttonColor: kShrinePink100,
      textTheme: ButtonTextTheme.normal,
    ),
    scaffoldBackgroundColor: kShrineBackgroundWhite,
    cardColor: kShrineBackgroundWhite,
    textSelectionColor: kShrinePink100,
    errorColor: kShrineErrorRed,

    textTheme: _buildShrineTextTheme(myLightTheme.textTheme),
    primaryTextTheme: _buildShrineTextTheme(myLightTheme.primaryTextTheme),
    accentTextTheme: _buildShrineTextTheme(myLightTheme.accentTextTheme),
    primaryIconTheme: myLightTheme.iconTheme.copyWith(
        color: kShrineBrown900
    ),
  );
}

TextTheme _buildShrineTextTheme(TextTheme base) {
  return base.copyWith(
    headline: base.headline.copyWith(
      fontWeight: FontWeight.w500,
    ),
    title: base.title.copyWith(
        fontSize: 18
    ),
    caption: base.caption.copyWith(
      fontWeight: FontWeight.w400,
      fontSize: 14,
    ),
  ).apply(
    fontFamily: 'Rubik',
    displayColor: kShrineBrown900,
    bodyColor: kShrineBrown900,
  );
}
