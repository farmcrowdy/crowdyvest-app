// To parse this JSON data, do
//
//     final banksPojo = banksPojoFromJson(jsonString);

import 'dart:convert';

BanksPojo banksPojoFromJson(String str) {
  final jsonData = json.decode(str);
  return BanksPojo.fromJson(jsonData);
}

String banksPojoToJson(BanksPojo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class BanksPojo {
  List<BankListDatum> data;

  BanksPojo({
    this.data,
  });

  factory BanksPojo.fromJson(Map<String, dynamic> json) => new BanksPojo(
    data: new List<BankListDatum>.from(json["data"].map((x) => BankListDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class BankListDatum {
  int id;
  String bankName;
  String createdAt;
  String updatedAt;

  BankListDatum({
    this.id,
    this.bankName,
    this.createdAt,
    this.updatedAt,
  });

  factory BankListDatum.fromJson(Map<String, dynamic> json) => new BankListDatum(
    id: json["id"],
    bankName: json["bank_name"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "bank_name": bankName,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
