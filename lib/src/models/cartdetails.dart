// To parse this JSON data, do
//
//     final cartDetails = cartDetailsFromJson(jsonString);

import 'dart:convert';

CartDetails cartDetailsFromJson(String str) {
  final jsonData = json.decode(str);
  return CartDetails.fromJson(jsonData);
}

String cartDetailsToJson(CartDetails data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class CartDetails {
  List<Datum> data;

  CartDetails({
    this.data,
  });

  factory CartDetails.fromJson(Map<String, dynamic> json) => new CartDetails(
    data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int id;
  int userId;
  int farmId;
  int sponsorUnit;
  String createdAt;
  String updatedAt;
  FarmDetails farmDetails;
  String farmtype;

  Datum({
    this.id,
    this.userId,
    this.farmId,
    this.sponsorUnit,
    this.createdAt,
    this.updatedAt,
    this.farmDetails,
    this.farmtype,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
    id: json["id"],
    userId: json["user_id"],
    farmId: json["farm_id"],
    sponsorUnit: json["sponsor_unit"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    farmDetails: FarmDetails.fromJson(json["farm_details"]),
    farmtype: json["farmtype"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "farm_id": farmId,
    "sponsor_unit": sponsorUnit,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "farm_details": farmDetails.toJson(),
    "farmtype": farmtype,
  };
}

class FarmDetails {
  int id;
  int farmtypeId;
  int stateId;
  int localId;
  int statusId;
  int farmCycle;
  int harvestPeriod;
  int totalUnit;
  int unitLeft;
  int roi;
  int pricePerunit;
  String slug;
  String farmImage;
  String farmDescription;
  int feat;
  String createdAt;
  String updatedAt;
  String startDate;
  String endDate;
  int publish;
  Farmtype farmtype;

  FarmDetails({
    this.id,
    this.farmtypeId,
    this.stateId,
    this.localId,
    this.statusId,
    this.farmCycle,
    this.harvestPeriod,
    this.totalUnit,
    this.unitLeft,
    this.roi,
    this.pricePerunit,
    this.slug,
    this.farmImage,
    this.farmDescription,
    this.feat,
    this.createdAt,
    this.updatedAt,
    this.startDate,
    this.endDate,
    this.publish,
    this.farmtype,
  });

  factory FarmDetails.fromJson(Map<String, dynamic> json) => new FarmDetails(
    id: json["id"],
    farmtypeId: json["farmtype_id"],
    stateId: json["state_id"],
    localId: json["local_id"],
    statusId: json["status_id"],
    farmCycle: json["farm_cycle"],
    harvestPeriod: json["harvest_period"],
    totalUnit: json["total_unit"],
    unitLeft: json["unit_left"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    slug: json["slug"],
    farmImage: json["farm_image"],
    farmDescription: json["farm_description"],
    feat: json["feat"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    startDate: json["start_date"],
    endDate: json["end_date"],
    publish: json["publish"],
    farmtype: Farmtype.fromJson(json["farmtype"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farmtype_id": farmtypeId,
    "state_id": stateId,
    "local_id": localId,
    "status_id": statusId,
    "farm_cycle": farmCycle,
    "harvest_period": harvestPeriod,
    "total_unit": totalUnit,
    "unit_left": unitLeft,
    "roi": roi,
    "price_perunit": pricePerunit,
    "slug": slug,
    "farm_image": farmImage,
    "farm_description": farmDescription,
    "feat": feat,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "start_date": startDate,
    "end_date": endDate,
    "publish": publish,
    "farmtype": farmtype.toJson(),
  };
}

class Farmtype {
  int id;
  String farmtype;
  String createdAt;
  String updatedAt;

  Farmtype({
    this.id,
    this.farmtype,
    this.createdAt,
    this.updatedAt,
  });

  factory Farmtype.fromJson(Map<String, dynamic> json) => new Farmtype(
    id: json["id"],
    farmtype: json["farmtype"],
    createdAt: json["created_at"] == null ? null : json["created_at"],
    updatedAt: json["updated_at"] == null ? null : json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farmtype": farmtype,
    "created_at": createdAt == null ? null : createdAt,
    "updated_at": updatedAt == null ? null : updatedAt,
  };
}
