// To parse this JSON data, do
//
//     final consentPojo = consentPojoFromJson(jsonString);

import 'dart:convert';

List<ConsentPojo> consentPojoFromJson(String str) => new List<ConsentPojo>.from(json.decode(str).map((x) => ConsentPojo.fromJson(x)));

String consentPojoToJson(List<ConsentPojo> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class ConsentPojo {
  String consentContent;

  ConsentPojo({
    this.consentContent,
  });

  factory ConsentPojo.fromJson(Map<String, dynamic> json) => new ConsentPojo(
    consentContent: json["consent_content"],
  );

  Map<String, dynamic> toJson() => {
    "consent_content": consentContent,
  };
}
