// To parse this JSON data, do
//
//     final faqPojo = faqPojoFromJson(jsonString);

import 'dart:convert';

List<FaqPojo> faqPojoFromJson(String str) => new List<FaqPojo>.from(json.decode(str).map((x) => FaqPojo.fromJson(x)));

String faqPojoToJson(List<FaqPojo> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class FaqPojo {
  String question;
  String answer;

  FaqPojo({
    this.question,
    this.answer,
  });

  factory FaqPojo.fromJson(Map<String, dynamic> json) => new FaqPojo(
    question: json["question"],
    answer: json["answer"],
  );

  Map<String, dynamic> toJson() => {
    "question": question,
    "answer": answer,
  };
}
