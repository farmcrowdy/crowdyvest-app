// To parse this JSON data, do
//
//     final farmUpdatePojo = farmUpdatePojoFromJson(jsonString);

import 'dart:convert';

FarmUpdatePojo farmUpdatePojoFromJson(String str) {
  final jsonData = json.decode(str);
  return FarmUpdatePojo.fromJson(jsonData);
}

String farmUpdatePojoToJson(FarmUpdatePojo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FarmUpdatePojo {
  List<UpdateDatum> data;

  FarmUpdatePojo({
    this.data,
  });

  factory FarmUpdatePojo.fromJson(Map<String, dynamic> json) => new FarmUpdatePojo(
    data: new List<UpdateDatum>.from(json["data"].map((x) => UpdateDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class UpdateDatum {
  int id;
  int groupId;
  int userId;
  String createdAt;
  String updatedAt;
  int farmId;
  FarmDetails farmDetails;
  List<Update> update;

  UpdateDatum({
    this.id,
    this.groupId,
    this.userId,
    this.createdAt,
    this.updatedAt,
    this.farmId,
    this.farmDetails,
    this.update,
  });

  factory UpdateDatum.fromJson(Map<String, dynamic> json) => new UpdateDatum(
    id: json["id"],
    groupId: json["group_id"],
    userId: json["user_id"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    farmId: json["farm_id"],
    farmDetails: FarmDetails.fromJson(json["farm_details"]),
    update: new List<Update>.from(json["update"].map((x) => Update.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "group_id": groupId,
    "user_id": userId,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "farm_id": farmId,
    "farm_details": farmDetails.toJson(),
    "update": new List<dynamic>.from(update.map((x) => x.toJson())),
  };
}

class FarmDetails {
  int farmtypeId;
  String slug;
  int roi;
  int pricePerunit;
  String farmImage;
  Farmtype farmtype;

  FarmDetails({
    this.farmtypeId,
    this.slug,
    this.roi,
    this.pricePerunit,
    this.farmImage,
    this.farmtype,
  });

  factory FarmDetails.fromJson(Map<String, dynamic> json) => new FarmDetails(
    farmtypeId: json["farmtype_id"],
    slug: json["slug"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    farmImage: json["farm_image"],
    farmtype: Farmtype.fromJson(json["farmtype"]),
  );

  Map<String, dynamic> toJson() => {
    "farmtype_id": farmtypeId,
    "slug": slug,
    "roi": roi,
    "price_perunit": pricePerunit,
    "farm_image": farmImage,
    "farmtype": farmtype.toJson(),
  };
}

class Farmtype {
  String farmtype;

  Farmtype({
    this.farmtype,
  });

  factory Farmtype.fromJson(Map<String, dynamic> json) => new Farmtype(
    farmtype: json["farmtype"],
  );

  Map<String, dynamic> toJson() => {
    "farmtype": farmtype,
  };
}

class Update {
  int id;
  int farmId;
  String groupId;
  String title;
  String subtitle;
  int publish;
  String createdAt;

  Update({
    this.id,
    this.farmId,
    this.groupId,
    this.title,
    this.subtitle,
    this.publish,
    this.createdAt,
  });

  factory Update.fromJson(Map<String, dynamic> json) => new Update(
    id: json["id"],
    farmId: json["farm_id"],
    groupId: json["group_id"],
    title: json["title"],
    subtitle: json["subtitle"],
    publish: json["publish"],
    createdAt: json["created_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farm_id": farmId,
    "group_id": groupId,
    "title": title,
    "subtitle": subtitle,
    "publish": publish,
    "created_at": createdAt,
  };
}
