// To parse this JSON data, do
//
//     final userProfilePojo = userProfilePojoFromJson(jsonString);

import 'dart:convert';

UserProfilePojo userProfilePojoFromJson(String str) {
  final jsonData = json.decode(str);
  return UserProfilePojo.fromJson(jsonData);
}

String userProfilePojoToJson(UserProfilePojo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class UserProfilePojo {
  Data data;
  int userId;

  UserProfilePojo({
    this.data,
    this.userId,
  });

  factory UserProfilePojo.fromJson(Map<String, dynamic> json) => new UserProfilePojo(
    data: Data.fromJson(json["data"]),
    userId: json["user_id"],
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
    "user_id": userId,
  };
}

class Data {
  int id;
  int roleId;
  String email;
  int status;
  String createdAt;
  dynamic updatedAt;
  int verified;
  String fullName;
  Profile profile;

  Data({
    this.id,
    this.roleId,
    this.email,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.verified,
    this.fullName,
    this.profile,
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
    id: json["id"],
    roleId: json["role_id"],
    email: json["email"],
    status: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    verified: json["verified"],
    fullName: json["full_name"],
    profile: Profile.fromJson(json["profile"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "email": email,
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "verified": verified,
    "full_name": fullName,
    "profile": profile.toJson(),
  };
}

class Profile {
  int id;
  int userId;
  String dateOfBirth;
  String gender;
  String phoneNumber;
  String nationality;
  String occupation;
  String address;
  String acctName;
  String acctNumber;
  dynamic nextkinFname;
  dynamic nextkinSname;
  String nextkinRelationship;
  dynamic nextkinEmail;
  String nextkinMobile;
  dynamic nextkinAddress;
  dynamic facebook;
  dynamic twitter;
  dynamic instagram;
  dynamic linkedin;
  int completeProfile;
  String createdAt;
  String updatedAt;
  String countryId;
  int stateproId;
  String city;
  String bankId;
  String propix;

  Profile({
    this.id,
    this.userId,
    this.dateOfBirth,
    this.gender,
    this.phoneNumber,
    this.nationality,
    this.occupation,
    this.address,
    this.acctName,
    this.acctNumber,
    this.nextkinFname,
    this.nextkinSname,
    this.nextkinRelationship,
    this.nextkinEmail,
    this.nextkinMobile,
    this.nextkinAddress,
    this.facebook,
    this.twitter,
    this.instagram,
    this.linkedin,
    this.completeProfile,
    this.createdAt,
    this.updatedAt,
    this.countryId,
    this.stateproId,
    this.city,
    this.bankId,
    this.propix,
  });

  factory Profile.fromJson(Map<String, dynamic> json) => new Profile(
    id: json["id"],
    userId: json["user_id"],
    dateOfBirth: json["date_of_birth"],
    gender: json["gender"],
    phoneNumber: json["phone_number"],
    nationality: json["nationality"],
    occupation: json["occupation"],
    address: json["address"],
    acctName: json["acct_name"],
    acctNumber: json["acct_number"],
    nextkinFname: json["nextkin_fname"],
    nextkinSname: json["nextkin_sname"],
    nextkinRelationship: json["nextkin_relationship"],
    nextkinEmail: json["nextkin_email"],
    nextkinMobile: json["nextkin_mobile"],
    nextkinAddress: json["nextkin_address"],
    facebook: json["facebook"],
    twitter: json["twitter"],
    instagram: json["instagram"],
    linkedin: json["linkedin"],
    completeProfile: json["complete_profile"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    countryId: json["country_id"],
    stateproId: json["statepro_id"],
    city: json["city"],
    bankId: json["bank_id"],
    propix: json["propix"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "date_of_birth": dateOfBirth,
    "gender": gender,
    "phone_number": phoneNumber,
    "nationality": nationality,
    "occupation": occupation,
    "address": address,
    "acct_name": acctName,
    "acct_number": acctNumber,
    "nextkin_fname": nextkinFname,
    "nextkin_sname": nextkinSname,
    "nextkin_relationship": nextkinRelationship,
    "nextkin_email": nextkinEmail,
    "nextkin_mobile": nextkinMobile,
    "nextkin_address": nextkinAddress,
    "facebook": facebook,
    "twitter": twitter,
    "instagram": instagram,
    "linkedin": linkedin,
    "complete_profile": completeProfile,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "country_id": countryId,
    "statepro_id": stateproId,
    "city": city,
    "bank_id": bankId,
    "propix": propix,
  };
}
