// To parse this JSON data, do
//
//     final oneFarmPojo = oneFarmPojoFromJson(jsonString);

import 'dart:convert';

OneFarmPojo oneFarmPojoFromJson(String str) => OneFarmPojo.fromJson(json.decode(str));

String oneFarmPojoToJson(OneFarmPojo data) => json.encode(data.toJson());

class OneFarmPojo {
  Data data;

  OneFarmPojo({
    this.data,
  });

  factory OneFarmPojo.fromJson(Map<String, dynamic> json) => OneFarmPojo(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  int id;
  String farmTitle;
  int companyId;
  int insuranceId;
  int farmtypeId;
  int stateId;
  int localId;
  int statusId;
  int farmCycle;
  int harvestPeriod;
  int totalUnit;
  int unitLeft;
  int roi;
  int pricePerunit;
  String slug;
  String farmImage;
  String farmDescription;
  int feat;
  DateTime createdAt;
  DateTime updatedAt;
  DateTime startDate;
  DateTime endDate;
  dynamic statusinfo;
  int publish;

  Data({
    this.id,
    this.farmTitle,
    this.companyId,
    this.insuranceId,
    this.farmtypeId,
    this.stateId,
    this.localId,
    this.statusId,
    this.farmCycle,
    this.harvestPeriod,
    this.totalUnit,
    this.unitLeft,
    this.roi,
    this.pricePerunit,
    this.slug,
    this.farmImage,
    this.farmDescription,
    this.feat,
    this.createdAt,
    this.updatedAt,
    this.startDate,
    this.endDate,
    this.statusinfo,
    this.publish,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    farmTitle: json["farm_title"],
    companyId: json["company_id"],
    insuranceId: json["insurance_id"],
    farmtypeId: json["farmtype_id"],
    stateId: json["state_id"],
    localId: json["local_id"],
    statusId: json["status_id"],
    farmCycle: json["farm_cycle"],
    harvestPeriod: json["harvest_period"],
    totalUnit: json["total_unit"],
    unitLeft: json["unit_left"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    slug: json["slug"],
    farmImage: json["farm_image"],
    farmDescription: json["farm_description"],
    feat: json["feat"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    startDate: DateTime.parse(json["start_date"]),
    endDate: DateTime.parse(json["end_date"]),
    statusinfo: json["statusinfo"],
    publish: json["publish"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farm_title": farmTitle,
    "company_id": companyId,
    "insurance_id": insuranceId,
    "farmtype_id": farmtypeId,
    "state_id": stateId,
    "local_id": localId,
    "status_id": statusId,
    "farm_cycle": farmCycle,
    "harvest_period": harvestPeriod,
    "total_unit": totalUnit,
    "unit_left": unitLeft,
    "roi": roi,
    "price_perunit": pricePerunit,
    "slug": slug,
    "farm_image": farmImage,
    "farm_description": farmDescription,
    "feat": feat,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "start_date": startDate.toIso8601String(),
    "end_date": endDate.toIso8601String(),
    "statusinfo": statusinfo,
    "publish": publish,
  };
}
