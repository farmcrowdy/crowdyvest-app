// To parse this JSON data, do
//
//     final farmUpdateDetailsPojo = farmUpdateDetailsPojoFromJson(jsonString);

import 'dart:convert';

FarmUpdateDetailsPojo farmUpdateDetailsPojoFromJson(String str) {
  final jsonData = json.decode(str);
  return FarmUpdateDetailsPojo.fromJson(jsonData);
}

String farmUpdateDetailsPojoToJson(FarmUpdateDetailsPojo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FarmUpdateDetailsPojo {
  List<UpdateDetailsDatum> data;

  FarmUpdateDetailsPojo({
    this.data,
  });

  factory FarmUpdateDetailsPojo.fromJson(Map<String, dynamic> json) => new FarmUpdateDetailsPojo(
    data: new List<UpdateDetailsDatum>.from(json["data"].map((x) => UpdateDetailsDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class UpdateDetailsDatum {
  int id;
  int farmId;
  String groupId;
  String title;
  String subtitle;
  String message;
  String createdAt;
  String updatedAt;
  int publish;

  UpdateDetailsDatum({
    this.id,
    this.farmId,
    this.groupId,
    this.title,
    this.subtitle,
    this.message,
    this.createdAt,
    this.updatedAt,
    this.publish,
  });

  factory UpdateDetailsDatum.fromJson(Map<String, dynamic> json) => new UpdateDetailsDatum(
    id: json["id"],
    farmId: json["farm_id"],
    groupId: json["group_id"],
    title: json["title"],
    subtitle: json["subtitle"],
    message: json["message"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    publish: json["publish"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farm_id": farmId,
    "group_id": groupId,
    "title": title,
    "subtitle": subtitle,
    "message": message,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "publish": publish,
  };
}

class Farmtype {
  String farmtype;

  Farmtype({
    this.farmtype,
  });

  factory Farmtype.fromJson(Map<String, dynamic> json) => new Farmtype(
    farmtype: json["farmtype"],
  );

  Map<String, dynamic> toJson() => {
    "farmtype": farmtype,
  };
}
