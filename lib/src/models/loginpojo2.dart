// To parse this JSON data, do
//
//     final fcLogin2 = fcLogin2FromJson(jsonString);

import 'dart:convert';

FcLogin2 fcLogin2FromJson(String str) {
  final jsonData = json.decode(str);
  return FcLogin2.fromJson(jsonData);
}

String fcLogin2ToJson(FcLogin2 data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FcLogin2 {
  Data data;

  FcLogin2({
    this.data,
  });

  factory FcLogin2.fromJson(Map<String, dynamic> json) => new FcLogin2(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  int id;
  int roleId;
  String email;
  int status;
  String createdAt;
  String updatedAt;
  int verified;
  String fullName;
  String first_name;
  String last_name;

  Data({
    this.id,
    this.roleId,
    this.email,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.verified,
    this.fullName,
    this.first_name,
    this.last_name
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
    id: json["id"],
    roleId: json["role_id"],
    email: json["email"],
    status: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    verified: json["verified"],
    fullName: json["full_name"],
    first_name: json["first_name"],
    last_name: json["last_name"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "role_id": roleId,
    "email": email,
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "verified": verified,
    "full_name": fullName,
    "first_name": first_name,
    "last_name": last_name
  };
}
