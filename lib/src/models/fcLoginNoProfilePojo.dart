// To parse this JSON data, do
//
//     final fcLogin = fcLoginFromJsonNoPojo(jsonString);

import 'dart:convert';

FcLogin fcLoginFromJsonNoPojo(String str) => FcLogin.fromJson(json.decode(str));

String fcLoginToJson(FcLogin data) => json.encode(data.toJson());

class FcLogin {
  Data data;

  FcLogin({
    this.data,
  });

  factory FcLogin.fromJson(Map<String, dynamic> json) => new FcLogin(
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "data": data.toJson(),
  };
}

class Data {
  int id;
  //int roleId;
  //String email;
  //int status;
  //DateTime createdAt;
  //DateTime updatedAt;
  //int verified;
  //String fullName;
  //dynamic designation;
  //String firstName;
  //String lastName;
 // String phoneNumber;
  int consent;
  //Profile profile;

  Data({
    this.id,
   // this.roleId,
    //this.email,
    //this.status,
    //this.createdAt,
    //this.updatedAt,
    //this.verified,
    //this.fullName,
    //this.designation,
    //this.firstName,
    //this.lastName,
    //this.phoneNumber,
    this.consent,
  //  this.profile,
  });

  factory Data.fromJson(Map<String, dynamic> json) => new Data(
    id: json["id"],
    //roleId: json["role_id"],
    //email: json["email"],
    //status: json["status"],
    //createdAt: DateTime.parse(json["created_at"]),
    //updatedAt: DateTime.parse(json["updated_at"]),
    //verified: json["verified"],
    //fullName: json["full_name"],
    //designation: json["designation"],
    //firstName: json["first_name"],
    //lastName: json["last_name"],
    //phoneNumber: json["phone_number"],
    consent: json["consent"],
  //  profile: Profile.fromJson(json["profile"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    //"role_id": roleId,
    //"email": email,
    //"status": status,
    //"created_at": createdAt.toIso8601String(),
    //"updated_at": updatedAt.toIso8601String(),
    //"verified": verified,
    //"full_name": fullName,
    //"designation": designation,
    //"first_name": firstName,
    //"last_name": lastName,
    //"phone_number": phoneNumber,
    "consent": consent,
   // "profile": profile.toJson(),
  };
}


