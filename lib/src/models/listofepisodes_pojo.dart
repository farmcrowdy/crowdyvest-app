// To parse this JSON data, do
//
//     final listOfEpisodes = listOfEpisodesFromJson(jsonString);

import 'dart:convert';

ListOfEpisodes listOfEpisodesFromJson(String str) {
  final jsonData = json.decode(str);
  return ListOfEpisodes.fromJson(jsonData);
}

String listOfEpisodesToJson(ListOfEpisodes data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class ListOfEpisodes {
  Response response;

  ListOfEpisodes({
    this.response,
  });

  factory ListOfEpisodes.fromJson(Map<String, dynamic> json) => new ListOfEpisodes(
    response: Response.fromJson(json["response"]),
  );

  Map<String, dynamic> toJson() => {
    "response": response.toJson(),
  };
}

class Response {
  List<Item> items;
  String nextUrl;

  Response({
    this.items,
    this.nextUrl,
  });

  factory Response.fromJson(Map<String, dynamic> json) => new Response(
    items: new List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
    nextUrl: json["next_url"],
  );

  Map<String, dynamic> toJson() => {
    "items": new List<dynamic>.from(items.map((x) => x.toJson())),
    "next_url": nextUrl,
  };
}

class Item {
  int episodeId;
  String type;
  String title;
  int duration;
  int showId;
  int authorId;
  String siteUrl;
  String imageUrl;
  String imageOriginalUrl;
  String publishedAt;
  bool downloadEnabled;
  String waveformUrl;

  Item({
    this.episodeId,
    this.type,
    this.title,
    this.duration,
    this.showId,
    this.authorId,
    this.siteUrl,
    this.imageUrl,
    this.imageOriginalUrl,
    this.publishedAt,
    this.downloadEnabled,
    this.waveformUrl,
  });

  factory Item.fromJson(Map<String, dynamic> json) => new Item(
    episodeId: json["episode_id"],
    type: json["type"],
    title: json["title"],
    duration: json["duration"],
    showId: json["show_id"],
    authorId: json["author_id"],
    siteUrl: json["site_url"],
    imageUrl: json["image_url"],
    imageOriginalUrl: json["image_original_url"],
    publishedAt: json["published_at"],
    downloadEnabled: json["download_enabled"],
    waveformUrl: json["waveform_url"],
  );

  Map<String, dynamic> toJson() => {
    "episode_id": episodeId,
    "type": type,
    "title": title,
    "duration": duration,
    "show_id": showId,
    "author_id": authorId,
    "site_url": siteUrl,
    "image_url": imageUrl,
    "image_original_url": imageOriginalUrl,
    "published_at": publishedAt,
    "download_enabled": downloadEnabled,
    "waveform_url": waveformUrl,
  };
}
