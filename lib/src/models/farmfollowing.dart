// To parse this JSON data, do
//
//     final farmFollowing = farmFollowingFromJson(jsonString);

import 'dart:convert';

FarmFollowing farmFollowingFromJson(String str) {
  final jsonData = json.decode(str);
  return FarmFollowing.fromJson(jsonData);
}

String farmFollowingToJson(FarmFollowing data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FarmFollowing {
  bool data;

  FarmFollowing({
    this.data,
  });

  factory FarmFollowing.fromJson(Map<String, dynamic> json) => new FarmFollowing(
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "data": data,
  };
}
