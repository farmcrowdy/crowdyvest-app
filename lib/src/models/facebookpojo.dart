// To parse this JSON data, do
//
//     final facebookParse = facebookParseFromJson(jsonString);

import 'dart:convert';

FacebookParse facebookParseFromJson(String str) {
  final jsonData = json.decode(str);
  return FacebookParse.fromJson(jsonData);
}

String facebookParseToJson(FacebookParse data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FacebookParse {
  String name;
  String firstName;
  String lastName;
  String email;
  String id;

  FacebookParse({
    this.name,
    this.firstName,
    this.lastName,
    this.email,
    this.id,
  });

  factory FacebookParse.fromJson(Map<String, dynamic> json) => new FacebookParse(
    name: json["name"],
    firstName: json["first_name"],
    lastName: json["last_name"],
    email: json["email"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name,
    "first_name": firstName,
    "last_name": lastName,
    "email": email,
    "id": id,
  };
}
