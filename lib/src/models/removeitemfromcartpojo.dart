// To parse this JSON data, do
//
//     final removeItemFromCart = removeItemFromCartFromJson(jsonString);

import 'dart:convert';

RemoveItemFromCart removeItemFromCartFromJson(String str) {
  final jsonData = json.decode(str);
  return RemoveItemFromCart.fromJson(jsonData);
}

String removeItemFromCartToJson(RemoveItemFromCart data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class RemoveItemFromCart {
  String message;

  RemoveItemFromCart({
    this.message,
  });

  factory RemoveItemFromCart.fromJson(Map<String, dynamic> json) => new RemoveItemFromCart(
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "message": message,
  };
}
