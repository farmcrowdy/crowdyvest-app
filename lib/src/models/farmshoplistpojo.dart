// To parse this JSON data, do
//
//     final shoplist = shoplistFromJson(jsonString);

import 'dart:convert';

Shoplist shoplistFromJson(String str) {
  final jsonData = json.decode(str);
  return Shoplist.fromJson(jsonData);
}

String shoplistToJson(Shoplist data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Shoplist {
  List<Datum> data;

  Shoplist({
    this.data,
  });

  factory Shoplist.fromJson(Map<String, dynamic> json) => new Shoplist(
    data: new List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int id;
  int farmtypeId;
  int stateId;
  int localId;
  int statusId;
  int farmCycle;
  int harvestPeriod;
  int totalUnit;
  int unitLeft;
  int roi;
  int pricePerunit;
  String slug;
  String farmImage;
  String farmDescription;
  int feat;
  String createdAt;
  String updatedAt;
  String startDate;
  String endDate;
  int publish;
  String farmtypename;
  String statename;
  String localname;
  Farmtype farmtype;
  FarmState state;
  Local local;
  InsuranceDetails insuranceDetails;

  Datum({
    this.id,
    this.farmtypeId,
    this.stateId,
    this.localId,
    this.statusId,
    this.farmCycle,
    this.harvestPeriod,
    this.totalUnit,
    this.unitLeft,
    this.roi,
    this.pricePerunit,
    this.slug,
    this.farmImage,
    this.farmDescription,
    this.feat,
    this.createdAt,
    this.updatedAt,
    this.startDate,
    this.endDate,
    this.publish,
    this.farmtypename,
    this.statename,
    this.localname,
    this.farmtype,
    this.state,
    this.local,
    this.insuranceDetails,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => new Datum(
    id: json["id"],
    farmtypeId: json["farmtype_id"],
    stateId: json["state_id"],
    localId: json["local_id"],
    statusId: json["status_id"],
    farmCycle: json["farm_cycle"],
    harvestPeriod: json["harvest_period"],
    totalUnit: json["total_unit"],
    unitLeft: json["unit_left"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    slug: json["slug"],
    farmImage: json["farm_image"],
    farmDescription: json["farm_description"],
    feat: json["feat"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    startDate: json["start_date"],
    endDate: json["end_date"],
    publish: json["publish"],
    farmtypename: json["farmtypename"],
    statename: json["statename"],
    localname: json["localname"],
    farmtype: Farmtype.fromJson(json["farmtype"]),
    state: FarmState.fromJson(json["state"]),
    local: Local.fromJson(json["local"]),
    insuranceDetails: InsuranceDetails.fromJson(json["insurance_details"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farmtype_id": farmtypeId,
    "state_id": stateId,
    "local_id": localId,
    "status_id": statusId,
    "farm_cycle": farmCycle,
    "harvest_period": harvestPeriod,
    "total_unit": totalUnit,
    "unit_left": unitLeft,
    "roi": roi,
    "price_perunit": pricePerunit,
    "slug": slug,
    "farm_image": farmImage,
    "farm_description": farmDescription,
    "feat": feat,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "start_date": startDate,
    "end_date": endDate,
    "publish": publish,
    "farmtypename": farmtypename,
    "statename": statename,
    "localname": localname,
    "farmtype": farmtype.toJson(),
    "state": state.toJson(),
    "local": local.toJson(),
    "insurance_details": insuranceDetails.toJson(),
  };
}

class Farmtype {
  int id;
  String farmtype;
  String createdAt;
  String updatedAt;

  Farmtype({
    this.id,
    this.farmtype,
    this.createdAt,
    this.updatedAt,
  });

  factory Farmtype.fromJson(Map<String, dynamic> json) => new Farmtype(
    id: json["id"],
    farmtype: json["farmtype"],
    createdAt: json["created_at"] == null ? null : json["created_at"],
    updatedAt: json["updated_at"] == null ? null : json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farmtype": farmtype,
    "created_at": createdAt == null ? null : createdAt,
    "updated_at": updatedAt == null ? null : updatedAt,
  };
}
class InsuranceDetails {
  int id;
  Title title;
  String description;
  String logo;
  DateTime createdAt;
  DateTime updatedAt;

  InsuranceDetails({
    this.id,
    this.title,
    this.description,
    this.logo,
    this.createdAt,
    this.updatedAt,
  });

  factory InsuranceDetails.fromJson(Map<String, dynamic> json) => new InsuranceDetails(
    id: json["id"],
    title: titleValues.map[json["title"]],
    description: json["description"],
    logo: json["logo"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": titleValues.reverse[title],
    "description": description,
    "logo": logo,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

enum Logo { AIICO_1566320994_PNG, AXA_PNG, LWA_1564489447_PNG }

final logoValues = new EnumValues({
  "aiico_1566320994.png": Logo.AIICO_1566320994_PNG,
  "axa.png": Logo.AXA_PNG,
  "LWA_1564489447.png": Logo.LWA_1564489447_PNG
});

enum Title { AIICO, AXA_MANSARD, LEADWAY_ASSURANCE }

final titleValues = new EnumValues({
  "AIICO": Title.AIICO,
  "AXA Mansard": Title.AXA_MANSARD,
  "Leadway Assurance": Title.LEADWAY_ASSURANCE
});


class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}

class Local {
  int localId;
  int stateId;
  String localName;

  Local({
    this.localId,
    this.stateId,
    this.localName,
  });

  factory Local.fromJson(Map<String, dynamic> json) => new Local(
    localId: json["local_id"],
    stateId: json["state_id"],
    localName: json["local_name"],
  );

  Map<String, dynamic> toJson() => {
    "local_id": localId,
    "state_id": stateId,
    "local_name": localName,
  };
}

class FarmState {
  int stateId;
  String name;

  FarmState({
    this.stateId,
    this.name,
  });

  factory FarmState.fromJson(Map<String, dynamic> json) => new FarmState(
    stateId: json["state_id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "state_id": stateId,
    "name": name,
  };
}
