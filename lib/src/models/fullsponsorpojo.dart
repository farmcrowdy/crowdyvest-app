// To parse this JSON data, do
//
//     final fullSponsorPojo = fullSponsorPojoFromJson(jsonString);

import 'dart:convert';

FullSponsorPojo fullSponsorPojoFromJson(String str) {
  final jsonData = json.decode(str);
  return FullSponsorPojo.fromJson(jsonData);
}

String fullSponsorPojoToJson(FullSponsorPojo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FullSponsorPojo {
  List<FullSponsorDatum> data;

  FullSponsorPojo({
    this.data,
  });

  factory FullSponsorPojo.fromJson(Map<String, dynamic> json) => new FullSponsorPojo(
    data: new List<FullSponsorDatum>.from(json["data"].map((x) => FullSponsorDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class FullSponsorDatum {
  int id;
  int userId;
  int farmId;
  int paymentId;
  int sponsorUnit;
  dynamic repaymentStatus;
  String createdAt;
  String updatedAt;
  int completeProfile;
  String startDate;
  String endDate;
  FarmDetails farmDetails;

  FullSponsorDatum({
    this.id,
    this.userId,
    this.farmId,
    this.paymentId,
    this.sponsorUnit,
    this.repaymentStatus,
    this.createdAt,
    this.updatedAt,
    this.completeProfile,
    this.startDate,
    this.endDate,
    this.farmDetails,
  });

  factory FullSponsorDatum.fromJson(Map<String, dynamic> json) => new FullSponsorDatum(
    id: json["id"],
    userId: json["user_id"],
    farmId: json["farm_id"],
    paymentId: json["payment_id"],
    sponsorUnit: json["sponsor_unit"],
    repaymentStatus: json["repayment_status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    completeProfile: json["complete_profile"],
    startDate: json["start_date"],
    endDate: json["end_date"],
    farmDetails: FarmDetails.fromJson(json["farm_details"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "farm_id": farmId,
    "payment_id": paymentId,
    "sponsor_unit": sponsorUnit,
    "repayment_status": repaymentStatus,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "complete_profile": completeProfile,
    "start_date": startDate,
    "end_date": endDate,
    "farm_details": farmDetails.toJson(),
  };
}

class FarmDetails {
  int farmtypeId;
  String slug;
  int roi;
  int pricePerunit;
  String farmImage;
  Farmtype farmtype;

  FarmDetails({
    this.farmtypeId,
    this.slug,
    this.roi,
    this.pricePerunit,
    this.farmImage,
    this.farmtype,
  });

  factory FarmDetails.fromJson(Map<String, dynamic> json) => new FarmDetails(
    farmtypeId: json["farmtype_id"],
    slug: json["slug"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    farmImage: json["farm_image"],
    farmtype: Farmtype.fromJson(json["farmtype"]),
  );

  Map<String, dynamic> toJson() => {
    "farmtype_id": farmtypeId,
    "slug": slug,
    "roi": roi,
    "price_perunit": pricePerunit,
    "farm_image": farmImage,
    "farmtype": farmtype.toJson(),
  };
}

class Farmtype {
  String farmtype;

  Farmtype({
    this.farmtype,
  });

  factory Farmtype.fromJson(Map<String, dynamic> json) => new Farmtype(
    farmtype: json["farmtype"],
  );

  Map<String, dynamic> toJson() => {
    "farmtype": farmtype,
  };
}
