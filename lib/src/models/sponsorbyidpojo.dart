// To parse this JSON data, do
//
//     final sponsorById = sponsorByIdFromJson(jsonString);

import 'dart:convert';

SponsorById sponsorByIdFromJson(String str) {
  final jsonData = json.decode(str);
  return SponsorById.fromJson(jsonData);
}

String sponsorByIdToJson(SponsorById data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class SponsorById {
  List<SponsorDatum> data;

  SponsorById({
    this.data,
  });

  factory SponsorById.fromJson(Map<String, dynamic> json) => new SponsorById(
    data: new List<SponsorDatum>.from(json["data"].map((x) => SponsorDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class SponsorDatum {
  int id;
  int userId;
  int farmId;
  int paymentId;
  int sponsorUnit;
  String repaymentStatus;
  String createdAt;
  String updatedAt;
  int completeProfile;
  String startDate;
  String endDate;
  FarmDetails farmDetails;

  SponsorDatum({
    this.id,
    this.userId,
    this.farmId,
    this.paymentId,
    this.sponsorUnit,
    this.repaymentStatus,
    this.createdAt,
    this.updatedAt,
    this.completeProfile,
    this.startDate,
    this.endDate,
    this.farmDetails,
  });

  factory SponsorDatum.fromJson(Map<String, dynamic> json) => new SponsorDatum(
    id: json["id"],
    userId: json["user_id"],
    farmId: json["farm_id"],
    paymentId: json["payment_id"],
    sponsorUnit: json["sponsor_unit"],
    repaymentStatus: json["repayment_status"] == null ? null : json["repayment_status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    completeProfile: json["complete_profile"],
    startDate: json["start_date"],
    endDate: json["end_date"],
    farmDetails: FarmDetails.fromJson(json["farm_details"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "farm_id": farmId,
    "payment_id": paymentId,
    "sponsor_unit": sponsorUnit,
    "repayment_status": repaymentStatus == null ? null : repaymentStatus,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "complete_profile": completeProfile,
    "start_date": startDate,
    "end_date": endDate,
    "farm_details": farmDetails.toJson(),
  };
}

class FarmDetails {
  int farmtypeId;
  String slug;
  int roi;
  int pricePerunit;
  FarmtypeClass farmtype;

  FarmDetails({
    this.farmtypeId,
    this.slug,
    this.roi,
    this.pricePerunit,
    this.farmtype,
  });

  factory FarmDetails.fromJson(Map<String, dynamic> json) => new FarmDetails(
    farmtypeId: json["farmtype_id"],
    slug: json["slug"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    farmtype: FarmtypeClass.fromJson(json["farmtype"]),
  );

  Map<String, dynamic> toJson() => {
    "farmtype_id": farmtypeId,
    "slug": slug,
    "roi": roi,
    "price_perunit": pricePerunit,
    "farmtype": farmtype.toJson(),
  };
}

class FarmtypeClass {
  FarmtypeEnum farmtype;

  FarmtypeClass({
    this.farmtype,
  });

  factory FarmtypeClass.fromJson(Map<String, dynamic> json) => new FarmtypeClass(
    farmtype: farmtypeEnumValues.map[json["farmtype"]],
  );

  Map<String, dynamic> toJson() => {
    "farmtype": farmtypeEnumValues.reverse[farmtype],
  };
}

enum FarmtypeEnum { MAIZE, POULTRY, RICE, SOYA_BEANS, GINGER, TOMATOES }

final farmtypeEnumValues = new EnumValues({
  "ginger": FarmtypeEnum.GINGER,
  "maize": FarmtypeEnum.MAIZE,
  "poultry": FarmtypeEnum.POULTRY,
  "rice": FarmtypeEnum.RICE,
  "soya beans": FarmtypeEnum.SOYA_BEANS,
  "tomatoes": FarmtypeEnum.TOMATOES
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
