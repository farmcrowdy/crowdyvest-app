// To parse this JSON data, do
//
//     final fullFollowedPojo = fullFollowedPojoFromJson(jsonString);

import 'dart:convert';

FullFollowedPojo fullFollowedPojoFromJson(String str) {
  final jsonData = json.decode(str);
  return FullFollowedPojo.fromJson(jsonData);
}

String fullFollowedPojoToJson(FullFollowedPojo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FullFollowedPojo {
  List<FollowedDatum> data;

  FullFollowedPojo({
    this.data,
  });

  factory FullFollowedPojo.fromJson(Map<String, dynamic> json) => new FullFollowedPojo(
    data: new List<FollowedDatum>.from(json["data"].map((x) => FollowedDatum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class FollowedDatum {
  int id;
  int farmtypeId;
  int stateId;
  int localId;
  int statusId;
  int farmCycle;
  int harvestPeriod;
  int totalUnit;
  int unitLeft;
  int roi;
  int pricePerunit;
  String slug;
  String farmImage;
  String farmDescription;
  int feat;
  String createdAt;
  String updatedAt;
  String startDate;
  String endDate;
  int publish;
  String farmtypename;
  String statename;
  String localname;
  Farmtype farmtype;
  Local local;

  FollowedDatum({
    this.id,
    this.farmtypeId,
    this.stateId,
    this.localId,
    this.statusId,
    this.farmCycle,
    this.harvestPeriod,
    this.totalUnit,
    this.unitLeft,
    this.roi,
    this.pricePerunit,
    this.slug,
    this.farmImage,
    this.farmDescription,
    this.feat,
    this.createdAt,
    this.updatedAt,
    this.startDate,
    this.endDate,
    this.publish,
    this.farmtypename,
    this.statename,
    this.localname,
    this.farmtype,
    this.local,
  });

  factory FollowedDatum.fromJson(Map<String, dynamic> json) => new FollowedDatum(
    id: json["id"],
    farmtypeId: json["farmtype_id"],
    stateId: json["state_id"],
    localId: json["local_id"],
    statusId: json["status_id"],
    farmCycle: json["farm_cycle"],
    harvestPeriod: json["harvest_period"],
    totalUnit: json["total_unit"],
    unitLeft: json["unit_left"],
    roi: json["roi"],
    pricePerunit: json["price_perunit"],
    slug: json["slug"],
    farmImage: json["farm_image"],
    farmDescription: json["farm_description"],
    feat: json["feat"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    startDate: json["start_date"],
    endDate: json["end_date"],
    publish: json["publish"],
    farmtypename: json["farmtypename"],
    statename: json["statename"],
    localname: json["localname"],
    farmtype: Farmtype.fromJson(json["farmtype"]),
    local: Local.fromJson(json["local"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farmtype_id": farmtypeId,
    "state_id": stateId,
    "local_id": localId,
    "status_id": statusId,
    "farm_cycle": farmCycle,
    "harvest_period": harvestPeriod,
    "total_unit": totalUnit,
    "unit_left": unitLeft,
    "roi": roi,
    "price_perunit": pricePerunit,
    "slug": slug,
    "farm_image": farmImage,
    "farm_description": farmDescription,
    "feat": feat,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "start_date": startDate,
    "end_date": endDate,
    "publish": publish,
    "farmtypename": farmtypename,
    "statename": statename,
    "localname": localname,
    "farmtype": farmtype.toJson(),
    "local": local.toJson(),
  };
}

class Farmtype {
  int id;
  String farmtype;
  String createdAt;
  String updatedAt;

  Farmtype({
    this.id,
    this.farmtype,
    this.createdAt,
    this.updatedAt,
  });

  factory Farmtype.fromJson(Map<String, dynamic> json) => new Farmtype(
    id: json["id"],
    farmtype: json["farmtype"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "farmtype": farmtype,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class Local {
  int localId;
  int stateId;
  String localName;

  Local({
    this.localId,
    this.stateId,
    this.localName,
  });

  factory Local.fromJson(Map<String, dynamic> json) => new Local(
    localId: json["local_id"],
    stateId: json["state_id"],
    localName: json["local_name"],
  );

  Map<String, dynamic> toJson() => {
    "local_id": localId,
    "state_id": stateId,
    "local_name": localName,
  };
}

