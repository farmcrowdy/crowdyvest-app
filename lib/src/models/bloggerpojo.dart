// To parse this JSON data, do
//
//     final blogFeedPojo = blogFeedPojoFromJson(jsonString);

import 'dart:convert';

List<BlogFeedPojo> blogFeedPojoFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<BlogFeedPojo>.from(jsonData.map((x) => BlogFeedPojo.fromJson(x)));
}

String blogFeedPojoToJson(List<BlogFeedPojo> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class BlogFeedPojo {
  int id;
  String date;
  String dateGmt;
  Guid guid;
  String modified;
  String modifiedGmt;
  String slug;
  String type;
  String link;
  Guid title;
  String jetpackFeaturedMediaUrl;

  BlogFeedPojo({
    this.id,
    this.date,
    this.dateGmt,
    this.guid,
    this.modified,
    this.modifiedGmt,
    this.slug,
    this.type,
    this.link,
    this.title,
    this.jetpackFeaturedMediaUrl,
  });

  factory BlogFeedPojo.fromJson(Map<String, dynamic> json) => new BlogFeedPojo(
    id: json["id"],
    date: json["date"],
    dateGmt: json["date_gmt"],
    guid: Guid.fromJson(json["guid"]),
    modified: json["modified"],
    modifiedGmt: json["modified_gmt"],
    slug: json["slug"],
    type: json["type"],
    link: json["link"],
    title: Guid.fromJson(json["title"]),
    jetpackFeaturedMediaUrl: json["jetpack_featured_media_url"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "date": date,
    "date_gmt": dateGmt,
    "guid": guid.toJson(),
    "modified": modified,
    "modified_gmt": modifiedGmt,
    "slug": slug,
    "type": type,
    "link": link,
    "title": title.toJson(),
    "jetpack_featured_media_url": jetpackFeaturedMediaUrl,
  };
}

class Guid {
  String rendered;

  Guid({
    this.rendered,
  });

  factory Guid.fromJson(Map<String, dynamic> json) => new Guid(
    rendered: json["rendered"],
  );

  Map<String, dynamic> toJson() => {
    "rendered": rendered,
  };
}
