// To parse this JSON data, do
//
//     final farmStatus = farmStatusFromJson(jsonString);

import 'dart:convert';

FarmStatus farmStatusFromJson(String str) {
  final jsonData = json.decode(str);
  return FarmStatus.fromJson(jsonData);
}

String farmStatusToJson(FarmStatus data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class FarmStatus {
  List<DatumStatusStatus> data;

  FarmStatus({
    this.data,
  });

  factory FarmStatus.fromJson(Map<String, dynamic> json) => new FarmStatus(
    data: new List<DatumStatusStatus>.from(json["data"].map((x) => DatumStatusStatus.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "data": new List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DatumStatusStatus {
  int id;
  String name;
  String createdAt;
  String updatedAt;

  DatumStatusStatus({
    this.id,
    this.name,
    this.createdAt,
    this.updatedAt,
  });

  factory DatumStatusStatus.fromJson(Map<String, dynamic> json) => new DatumStatusStatus(
    id: json["id"],
    name: json["name"],
    createdAt: json["created_at"] == null ? null : json["created_at"],
    updatedAt: json["updated_at"] == null ? null : json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "created_at": createdAt == null ? null : createdAt,
    "updated_at": updatedAt == null ? null : updatedAt,
  };
}
