import 'dart:convert';

import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/analytics/actions_lytics.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:percent_indicator/percent_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'bankdetails.dart';
import '../cart/cart.dart';
import '../colors.dart';
import 'contact_details.dart';
import '../dashboard/dashboard.dart';
import '../projects/portfolio.dart';
import '../engine/noAnimationPageRouter.dart';
import '../models/userprofilepojo.dart';
import '../models/loginpojo2.dart';
import 'nextofkin_details.dart';
import 'profiledetails.dart';
import 'socialmedia_details.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:async/async.dart';
import '../webpage.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io' show Platform;
import '../engine/values.dart';
import '../onboard/resetpassword.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';


class MainProfile extends StatefulWidget{
  final FirebaseAnalyticsObserver observer;


  MainProfile(this.observer);

  @override
  MainProfileState createState()=> new MainProfileState(observer);
}
SharedPreferences prefs;
class MainProfileState extends State<MainProfile> with RouteAware {
  int userId = 0;
  String fname,
      sname,
      email,
      propix = "",
      numbOfCycle;
  bool isAndroid = false;

  final FirebaseAnalyticsObserver observer;
  MainProfileState(this.observer);

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsMainProfile(observer);
  }








  _getUserData() async {
    prefs = await SharedPreferences.getInstance();

    setState(() {
      if (Platform.isAndroid) {
        isAndroid = true;
      }
      userId = (prefs.getInt('userid') ?? 0);
      print("user id is: $userId");
      fname = prefs.getString('fname')[0].toUpperCase() + prefs.getString('fname').substring(1).toLowerCase();
      print("fname is $fname");
      sname = "";
      email = prefs.get('email');
      print("email is $email");
      propix = prefs.get("propix");
      numbOfCycle = prefs.getString('nextendofcycle');
    });

    final response = await http.get(
      baseUrl + '/api/userdetails/' + userId.toString(), headers: {
      "x-authorization": "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
    },
    );

    //userDetail = userProfilePojoFromJson(data);
    return response.body;
  }

  _asyncLoaderEpisodes() =>
      new AsyncLoader(
          initState: () async => await _getUserData(),
          renderLoad: () => new LinearProgressIndicator(),
          renderError: ([error]) =>
          new Text(
            'Sorry, there was an error loading. Please check internet connection',
            style: TextStyle(color: Colors.black),),
          renderSuccess: ({data}) => previewData(data)

      );

  _asyncLoaderEpisodesTablet() =>
      new AsyncLoader(
          initState: () async => await _getUserData(),
          renderLoad: () => new LinearProgressIndicator(),
          renderError: ([error]) =>
          new Text(
            'Sorry, there was an error loading. Please check internet connection',
            style: TextStyle(color: Colors.black),),
          renderSuccess: ({data}) => previewDataTablet(data)

      );


  void logoutFunction(BuildContext context) {
    showDialog(context: (context), builder: (context) {
      return AlertDialog(
        title: Text("Log out", style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
        content: Text("Are you sure you want log out of Crowdyvest app?", style: TextStyle(color: Colors.black),),
        actions: <Widget>[
          FlatButton(onPressed:()=>Navigator.of(context).pop() , child: Text("No", style: TextStyle(color: Colors.black),),),

          FlatButton(onPressed: () async{
            Navigator.of(context).pop();
            userLogsOut(observer);
            ProgressDialog pr = new ProgressDialog(context,);
            if (!pr.isShowing()) {
              pr.show();
              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.clear();
              pr.style(message:"Logging out...");
              new Timer(new Duration(seconds: 2), () async {
                pr.hide();
                exit(0);
              });
            }
          }, child: Text("Yes", style: TextStyle(fontWeight: FontWeight.bold),)),



        ],
      );
    });

  }

  doUpdateLocalDB(UserProfilePojo pojo) async {
    /* Directory appDocDir = await getApplicationDocumentsDirectory();
    final path = appDocDir.path+ '/farmcrowdy.db';
    final db = ObjectDB(path);
    db.open();
    //db.remove({"id": userId});
    await db.insert();

    Map<dynamic, dynamic> mapper= await db.last({'id': 1,});
    print('fucking this at nationality'+ mapper.toString());
    db.close();
    */
  }

  Widget previewDataTablet(String data) {
    int percentageCompleted;
    var userDetail;
    bool loadSuccess = true;
    String bankId;
    try {
      userDetail = userProfilePojoFromJson(data);
      doUpdateLocalDB(userDetail);

      int totalFields = 28;
      int unfilled = 0;

      if (userDetail.data.profile.dateOfBirth == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.gender == null) unfilled = unfilled + 1;;
      if (userDetail.data.profile.phoneNumber == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nationality == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.occupation == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.address == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.acctName == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.acctNumber == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinFname == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinSname == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinRelationship == null)
        unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinEmail == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinMobile == null)
        unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinAddress == null)
        unfilled = unfilled + 1;
      if (userDetail.data.profile.facebook == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.twitter == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.instagram == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.linkedin == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.countryId == 0) unfilled = unfilled + 1;
      if (userDetail.data.profile.stateproId == 0) unfilled = unfilled + 1;
      if (userDetail.data.profile.city == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.bankId == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.propix == null) {
        unfilled = unfilled + 1;
        prefs.setString("propix", " ");
      }
      else
        prefs.setString("propix", userDetail.data.profile.propix);
      bankId = userDetail.data.profile.bankId;

      percentageCompleted =
          ((((totalFields - unfilled) / totalFields) * 100) + 7).round();
      if (percentageCompleted > 100) percentageCompleted = 100;
    }
    catch (e) {
      print("Profile error: " + e.toString());
      userDetail = fcLogin2FromJson(data);
      bankId = 1.toString();
      loadSuccess = false;
      percentageCompleted = 15;
    }


    return Builder(builder: (contexter) =>
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 30,),

            Text('Your profile is ' + percentageCompleted.toString() +
                '% complete',
              style: TextStyle(color: Colors.black, fontSize: 22),),
            SizedBox(height: 8,),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                LinearPercentIndicator(
                  width: 300,
                  lineHeight: 8.0,
                  percent: percentageCompleted / 100,
                  animation: true,
                  animateFromLastPercent: true,
                  animationDuration: 2000,
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  backgroundColor: Colors.grey[300],
                  progressColor: greenStart,
                ),
              ],

            ),

            SizedBox(height: 16,),
            Divider(height: 2, color: Colors.grey[300],),
            SizedBox(height: 16,),
            Container(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
              child: Text(
                'Click any of the icons below to view or update profile',
                style: TextStyle(color: Colors.grey, fontSize: 26),),
            ),

            SizedBox(height: 20,),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[

                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      ProfileDetails(
                          userDetail.data.fullName, userDetail.data.email,
                          loadSuccess
                              ? userDetail.data.profile.phoneNumber
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.dateOfBirth
                              : null,
                          loadSuccess ? userDetail.data.profile.gender : null,
                          loadSuccess
                              ? userDetail.data.profile.nationality
                              : null
                      )));
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(20),
                      width: 150.0,
                      height: 150.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/group1.png"),
                    ),
                    SizedBox(height: 16,),
                    Text('Personal \n details', style: TextStyle(
                        color: Colors.grey[800], fontSize: 22, height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),

                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      ContactDetails(
                          loadSuccess ? userDetail.data.profile.address : null,
                          loadSuccess ? userDetail.data.profile.city : null)));
                }, child: Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(22),
                      width: 150.0,
                      height: 150.0,
                      // margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/contact.png"),
                    ),
                    SizedBox(height: 16,),
                    Text('Contact \n details',
                        style: TextStyle(color: Colors.grey[800], fontSize: 22, height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),

                GestureDetector(onTap: () {
                  if (loadSuccess) {
                    Navigator.push(
                        contexter, MaterialPageRoute(builder: (context) =>
                        BankDetails(
                          userDetail.data.profile.bankId,
                          userDetail.data.profile.acctName,
                          userDetail.data.profile.acctNumber,
                        )));
                  }
                  else {
                    Navigator.push(
                        contexter, MaterialPageRoute(builder: (context) =>
                        BankDetails(
                          "Choose a bank",
                          null,
                          null,
                        )));
                  }
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      height: 150.0,
                      width: 150.0,
                      padding: EdgeInsets.all(20),
                      //margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,
                      ),
                      child: Image.asset(
                        "assets/group2.png", width: 100, height: 100,),


                    ),
                    SizedBox(height: 16,),
                    Text('Bank \n details',
                      style: TextStyle(color: Colors.grey[800], fontSize: 22, height: 1.2),
                      textAlign: TextAlign.center,)
                  ],
                )),

              ],
            ),


            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      NextOfKinDetails(
                          loadSuccess
                              ? userDetail.data.profile.nextkinFname
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinSname
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinEmail
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinMobile
                              : null,
                          loadSuccess ? userDetail.data.profile
                              .nextkinRelationship : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinAddress
                              : null
                      )));
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(20),
                      width: 150.0,
                      height: 150.0,
                      margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/family.png"),
                    ),
                    SizedBox(height: 16,),
                    Text('Next of kin \n details', style: TextStyle(
                        color: Colors.grey[800], fontSize: 22, height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),


                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      SocialMediaDetails(
                          loadSuccess ? userDetail.data.profile.facebook : null,
                          loadSuccess ? userDetail.data.profile.twitter : null,
                          loadSuccess ? userDetail.data.profile.linkedin : null,
                          loadSuccess ? userDetail.data.profile.instagram : null
                      )));
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(18),
                      width: 150.0,
                      height: 150.0,
                      margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/social_m.png"),
                    ),
                    SizedBox(height: 16,),
                    Text('Social media \n details',
                        style: TextStyle(color: Colors.grey[800],fontSize: 22, height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),


                GestureDetector(onTap: () {
                  logoutFunction(contexter);
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(22),
                      width: 150.0,
                      height: 150.0,
                      margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/logout.png"),
                    ),
                    SizedBox(height: 16,),
                    Text('Log out \n    ',
                      style: TextStyle(color: Colors.grey[800], fontSize: 22, height: 1.2),
                      textAlign: TextAlign.center,)
                  ],
                )),

              ],
            )


          ],
        )
    );
  }

  Widget previewData(String data) {
    int percentageCompleted;
    var userDetail;
    bool loadSuccess = true;
    String bankId;
    try {
      userDetail = userProfilePojoFromJson(data);
      doUpdateLocalDB(userDetail);

      int totalFields = 28;
      int unfilled = 0;

      if (userDetail.data.profile.dateOfBirth == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.gender == null) unfilled = unfilled + 1;;
      if (userDetail.data.profile.phoneNumber == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nationality == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.occupation == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.address == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.acctName == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.acctNumber == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinFname == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinSname == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinRelationship == null)
        unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinEmail == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinMobile == null)
        unfilled = unfilled + 1;
      if (userDetail.data.profile.nextkinAddress == null)
        unfilled = unfilled + 1;
      if (userDetail.data.profile.facebook == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.twitter == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.instagram == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.linkedin == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.countryId == 0) unfilled = unfilled + 1;
      if (userDetail.data.profile.stateproId == 0) unfilled = unfilled + 1;
      if (userDetail.data.profile.city == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.bankId == null) unfilled = unfilled + 1;
      if (userDetail.data.profile.propix == null) {
        unfilled = unfilled + 1;
        prefs.setString("propix", " ");
      }
      else
        prefs.setString("propix", userDetail.data.profile.propix);
      bankId = userDetail.data.profile.bankId;

      percentageCompleted =
          ((((totalFields - unfilled) / totalFields) * 100) + 7).round();
      if (percentageCompleted > 100) percentageCompleted = 100;
    }
    catch (e) {
      print("Profile error: " + e.toString());
      userDetail = fcLogin2FromJson(data);
      bankId = 1.toString();
      loadSuccess = false;
      percentageCompleted = 15;
    }


    return Builder(builder: (contexter) =>
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 30,),

            Text('Your profile is ' + percentageCompleted.toString() +
                '% complete',
              style: TextStyle(color: Colors.black, fontSize: 16),),
            SizedBox(height: 8,),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                LinearPercentIndicator(
                  width: 300,
                  lineHeight: 8.0,
                  percent: percentageCompleted / 100,
                  animation: true,
                  animateFromLastPercent: true,
                  animationDuration: 2000,
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  backgroundColor: Colors.grey[300],
                  progressColor: greenStart,
                ),
              ],

            ),

            SizedBox(height: 16,),
            Divider(height: 2, color: Colors.grey[300],),
            SizedBox(height: 16,),
            Container(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
              child: Text(
                'Click any of the icons below to view or update profile',
                style: TextStyle(color: Colors.grey, fontSize: 14),),
            ),

            SizedBox(height: 20,),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[

                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      ProfileDetails(
                          userDetail.data.fullName, userDetail.data.email,
                          loadSuccess
                              ? userDetail.data.profile.phoneNumber
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.dateOfBirth
                              : null,
                          loadSuccess ? userDetail.data.profile.gender : null,
                          loadSuccess
                              ? userDetail.data.profile.nationality
                              : null
                      )));
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(20),
                      width: 100.0,
                      height: 100.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/group1.png"),
                    ),
                    SizedBox(height: 8,),
                    Text('Personal \n details', style: TextStyle(
                        color: Colors.grey[800], fontSize: 14, height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),

                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      ContactDetails(
                          loadSuccess ? userDetail.data.profile.address : null,
                          loadSuccess ? userDetail.data.profile.city : null)));
                }, child: Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(22),
                      width: 100.0,
                      height: 100.0,
                      // margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/contact.png"),
                    ),
                    SizedBox(height: 8,),
                    Text('Contact \n details',
                        style: TextStyle(color: Colors.grey[800], height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),

                GestureDetector(onTap: () {
                  if (loadSuccess) {
                    Navigator.push(
                        contexter, MaterialPageRoute(builder: (context) =>
                        BankDetails(
                          userDetail.data.profile.bankId,
                          userDetail.data.profile.acctName,
                          userDetail.data.profile.acctNumber,
                        )));
                  }
                  else {
                    Navigator.push(
                        contexter, MaterialPageRoute(builder: (context) =>
                        BankDetails(
                          "Choose a bank",
                          null,
                          null,
                        )));
                  }
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      height: 100.0,
                      width: 100.0,
                      padding: EdgeInsets.all(20),
                      //margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,
                      ),
                      child: Image.asset(
                        "assets/group2.png", width: 100, height: 100,),


                    ),
                    SizedBox(height: 8,),
                    Text('Bank \n details',
                      style: TextStyle(color: Colors.grey[800], height: 1.2),
                      textAlign: TextAlign.center,)
                  ],
                )),

              ],
            ),


            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      NextOfKinDetails(
                          loadSuccess
                              ? userDetail.data.profile.nextkinFname
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinSname
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinEmail
                              : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinMobile
                              : null,
                          loadSuccess ? userDetail.data.profile
                              .nextkinRelationship : null,
                          loadSuccess
                              ? userDetail.data.profile.nextkinAddress
                              : null
                      )));
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(20),
                      width: 100.0,
                      height: 100.0,
                      margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/family.png"),
                    ),
                    SizedBox(height: 8,),
                    Text('Next of kin \n details', style: TextStyle(
                        color: Colors.grey[800], fontSize: 14, height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),


                GestureDetector(onTap: () {
                  Navigator.push(
                      contexter, MaterialPageRoute(builder: (context) =>
                      SocialMediaDetails(
                          loadSuccess ? userDetail.data.profile.facebook : null,
                          loadSuccess ? userDetail.data.profile.twitter : null,
                          loadSuccess ? userDetail.data.profile.linkedin : null,
                          loadSuccess ? userDetail.data.profile.instagram : null
                      )));
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(18),
                      width: 100.0,
                      height: 100.0,
                      margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/social_m.png"),
                    ),
                    SizedBox(height: 8,),
                    Text('Social media \n details',
                        style: TextStyle(color: Colors.grey[800], height: 1.2),
                        textAlign: TextAlign.center)
                  ],
                )),


                GestureDetector(onTap: () {
                  logoutFunction(contexter);
                }, child:
                Column(
                  children: <Widget>[
                    new Container(
                      padding: EdgeInsets.all(22),
                      width: 100.0,
                      height: 100.0,
                      margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                      decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.grey[300],
                            offset: Offset(0.01, 0.02),
                            blurRadius: 10.0,
                          ),
                        ],
                        color: Colors.white,

                      ),
                      child: Image.asset("assets/logout.png"),
                    ),
                    SizedBox(height: 8,),
                    Text('Log out \n    ',
                      style: TextStyle(color: Colors.grey[800], height: 1.2),
                      textAlign: TextAlign.center,)
                  ],
                )),

              ],
            )


          ],
        )
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  void updatePicture(BuildContext contexting) async {
    ProgressDialog pr = new ProgressDialog(contexting,);
    if (!pr.isShowing()) {
      File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);

      pr.style(message:"Updating...");
      pr.show();

      SharedPreferences prefs = await SharedPreferences.getInstance();
      int userId = (prefs.getInt('userid') ?? 1);

      var stream = new http.ByteStream(
          DelegatingStream.typed(imageFile.openRead()));
      var length = await imageFile.length();

      var uri = Uri.parse(baseUrl + "/api/userdetails");
      var request = new http.MultipartRequest("POST", uri);
      var multipartFile = new http.MultipartFile('propix', stream, length,
          filename: basename(imageFile.path));
      //contentType: new MediaType('image', 'png'));

      request.headers["x-authorization"] =
      "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD";
      request.fields["id"] = userId.toString();

      request.files.add(multipartFile);
      var response = await request.send();
      print(response.statusCode);
      response.stream.transform(utf8.decoder).listen((value) {
        print(value);
      });
      pr.hide();

      if (response.statusCode == 200) {
        Scaffold.of(contexting).showSnackBar(SnackBar(
          content: Text('Update successful...'),
          duration: Duration(seconds: 4),));
      }
      else {
        Scaffold.of(contexting).showSnackBar(SnackBar(
          content: Text('Failed, please try again...'),
          duration: Duration(seconds: 4),));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build


    void onTabTapped(int index) {
      switch (index) {
        case 0:
          Navigator.push(context, NoAnimationPageRouter(builder: (context) => DashboardPageState(observer)));
          break;
        case 1:
          Navigator.push(context, NoAnimationPageRouter(builder: (context) => FarmShopPageState(observer)));
          break;

      }
    }


    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    if (useMobileLayout) {
      return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(leading: IconButton(
              icon: Icon(Icons.keyboard_backspace, color: Colors.white,),
              onPressed: () {
                Navigator.pop(context);
              }),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                child: IconButton(
                  icon: Icon(Icons.shopping_cart),
                  color: greenStart,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => CartPage(observer)));
                  },),
              )
            ],
            backgroundColor: darkGreen,
            elevation: 0,
          ),
          body: Builder(builder: (contexting) =>
              SingleChildScrollView(
                child: Stack(
                  fit: StackFit.passthrough,
                  children: <Widget>[

                    Container(
                      decoration: BoxDecoration(color: darkGreen),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            width: 80.0,
                            height: 80.0,
                            margin: EdgeInsets.fromLTRB(16, 0, 0, 16),
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(
                                      baseUrl + '/storage/propixs/' + propix),
                                )
                            ),
                          ),
                          SizedBox(width: 16,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 6,),
                              Text('$fname', style: TextStyle(
                                  fontSize: 20, color: Colors.white),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,),
                              SizedBox(height: 4,),
                              Text('$email', style: TextStyle(
                                  fontSize: 14, color: Colors.white),),

                              SizedBox(height: 8,),

                              GestureDetector(onTap: () {
                                Navigator.push(contexting, MaterialPageRoute(
                                    builder: (contexting) => ResetPassword()));
                              },
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        color: greenStart, width: 1.0),

                                  ), child: Text('Change Password', style: TextStyle(color: greenStart),),

                                ),
                              ),

                              SizedBox(height: 8,),
                              GestureDetector(onTap: () {
                                if (Platform.isAndroid) {
                                  updatePicture(contexting);
                                }
                              },
                                child: isAndroid ? Container(
                                  width: 0, height: 0,) : Container(
                                  width: 0, height: 0,),
                              ),

                              SizedBox(height: 70,),

                            ],
                          ),

                        ],

                      ),
                    ),

                    Container(
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              bottomLeft: Radius.circular(0),
                              bottomRight: Radius.circular(0),
                              topRight: Radius.circular(15)),
                          // boxShadow: <BoxShadow>[


                        ),
                        //decoration: BoxDecoration(color: Colors.white),
                        margin: EdgeInsets.fromLTRB(0, 140, 0, 0),
                        child: _asyncLoaderEpisodes()
                    )


                  ],
                ),
              )
          ),
          bottomNavigationBar:
          Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[500],
                  offset: Offset(0.01, 0.02),
                  blurRadius: 5.0,
                ),
              ],


            ),
            child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,

                onTap: onTabTapped, // new
                items: [
                  new BottomNavigationBarItem(
                      icon: Container(
                        child: Image.asset("assets/dash13.png"),
                        width: 24,
                        height: 24,),
                      title: Text(
                        Dashboard, style: TextStyle(color: Colors.black),),
                      backgroundColor: Colors.grey[600]
                  ),

                  new BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shopping_basket, color: darkGreen, size: 24,),
                      title: Text(
                        Portfolio, style: TextStyle(color: Colors.black),),
                      backgroundColor: Colors.grey[600]
                  ),
                  new BottomNavigationBarItem(
                      icon: Container(
                        child: Image.asset("assets/dash14.png"),
                        width: 24,
                        height: 24,),
                      title: Text("Profile", style: TextStyle(
                          color: greenStart, fontWeight: FontWeight.bold),),
                      backgroundColor: Colors.grey[600]),

                ]
            ),
          )
      );
    }
    else {
      return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(leading: IconButton(
              icon: Icon(Icons.keyboard_backspace, color: Colors.white,),
              onPressed: () {
                Navigator.pop(context);
              }),
            actions: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                child: IconButton(
                  icon: Icon(Icons.shopping_cart),
                  color: greenStart,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => CartPage(observer)));
                  },),
              )
            ],
            backgroundColor: darkGreen,
            elevation: 0,
          ),
          body: Builder(builder: (contexting) =>
              SingleChildScrollView(
                child: Stack(
                  fit: StackFit.passthrough,
                  children: <Widget>[

                    Container(
                      decoration: BoxDecoration(color: darkGreen),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                            width: 100.0,
                            height: 100.0,
                            margin: EdgeInsets.fromLTRB(32, 0, 16, 16),
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      baseUrl + '/storage/propixs/' + propix),
                                )
                            ),
                          ),
                          SizedBox(width: 16,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 6,),
                              Text('$fname', style: TextStyle(
                                  fontSize: 26, color: Colors.white),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,),
                              SizedBox(height: 4,),

                              Text('$email', style: TextStyle(
                                  fontSize: 22, color: Colors.white),),

                              SizedBox(height: 8,),

                              GestureDetector(onTap: () {
                                Navigator.push(contexting, MaterialPageRoute(
                                    builder: (contexting) =>ResetPassword()));
                              },
                                child: Container(
                                  padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(5),
                                    border: Border.all(
                                        color: greenStart, width: 1.0),

                                  ), child: Text('Change Password', style: TextStyle(fontSize: 22, color: greenStart),),

                                ),
                              ),

                              SizedBox(height: 8,),
                              GestureDetector(onTap: () {
                                if (Platform.isAndroid) {
                                  updatePicture(contexting);
                                }
                              },
                                child: isAndroid ? Container(
                                  width: 0, height: 0,) : Container(
                                  width: 0, height: 0,),
                              ),

                              SizedBox(height: 70,),

                            ],
                          ),

                        ],

                      ),
                    ),

                    Container(
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              bottomLeft: Radius.circular(0),
                              bottomRight: Radius.circular(0),
                              topRight: Radius.circular(15)),
                          // boxShadow: <BoxShadow>[


                        ),
                        //decoration: BoxDecoration(color: Colors.white),
                        margin: EdgeInsets.fromLTRB(0, 140, 0, 0),
                        child: _asyncLoaderEpisodesTablet()
                    )


                  ],
                ),
              )
          ),
          bottomNavigationBar:
          Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[500],
                  offset: Offset(0.01, 0.02),
                  blurRadius: 5.0,
                ),
              ],


            ),
            child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,

                onTap: onTabTapped, // new
                items: [
                  new BottomNavigationBarItem(
                      icon: Container(
                        child: Image.asset("assets/dash13.png"),
                        width: 34,
                        height: 34,),
                      title: Text(Dashboard,
                        style: TextStyle(color: Colors.black, fontSize: 26),),
                      backgroundColor: Colors.grey[600]
                  ),

                  new BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shopping_basket, color: darkGreen, size: 34,),
                      title: Text(Portfolio,
                        style: TextStyle(color: Colors.black, fontSize: 26),),
                      backgroundColor: Colors.grey[600]
                  ),
                  new BottomNavigationBarItem(
                      icon: Container(
                        child: Image.asset("assets/dash14.png"),
                        width: 34,
                        height: 34,),
                      title: Text("Profile", style: TextStyle(color: greenStart,
                          fontSize: 26, fontWeight:FontWeight.bold),),
                      backgroundColor: Colors.grey[600]),

                ]
            ),
          )
      );
    }
  }

}