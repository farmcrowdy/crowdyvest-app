import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import '../colors.dart';

class ContactDetails extends StatefulWidget {
  String address, city;
  ContactDetails(this.address, this.city);

  @override
  ContactDetailState createState() => new ContactDetailState(address, city);

}

var _addressController = new TextEditingController();
var _cityontroller = new TextEditingController();
ProgressDialog pr;
bool isMobile = true;


class ContactDetailState extends State<ContactDetails> {
  String address, city;
  ContactDetailState(this.address, this.city);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _addressController.text = address;
      _cityontroller.text = city;
    });
  }


  void updatedInfo(BuildContext context) async {
    pr = new ProgressDialog(context,);
    pr.style(message:'Updating...');
    if(!pr.isShowing()) {
      pr.show();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int userId = (prefs.getInt('userid') ?? 1);


      final response = await http.post(
        baseUrl+'/api/userdetails',

        body: {"id": userId.toString(), "address": _addressController.text, "city": _cityontroller.text},
        headers: {
          "x-authorization":
          "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
        },
      );

      pr.hide();
      if(response.statusCode == 200) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Update successful...'), duration: Duration(seconds:4),));
      }
      else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Failed, please try again...'),
          duration: Duration(seconds: 4),));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    setState(() {
      if (!useMobileLayout) isMobile = false;
    });

    return Scaffold(
        appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
          Navigator.pop(context);
        }),

          title: Text('View/Edit Contact Details', style: TextStyle(color: Colors.white),),
          backgroundColor: darkGreen,
        ),

        body: Builder(builder: (contexter)=>

               SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            child:  Container(
              child: Column(
                children: <Widget>[
                  TextField(style: TextStyle(fontSize: isMobile ? 18 : 24, color: Colors.black),controller: _addressController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.location_on), labelText: 'Home/Office address',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18 : 24, color: Colors.black),controller: _cityontroller, decoration: InputDecoration(icon:new Icon(Icons.business), labelText: 'City',), obscureText: false),

                  SizedBox(height: 32,),

                  new SizedBox(
                      width: double.infinity,
                      height: 42,

                      child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                        onPressed: () {
                      updatedInfo(contexter);
                      }, child: Text('Update', style: TextStyle(fontSize: isMobile ? 18 : 24), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
                  )
                ],
              ),
            )
        )
        )

    );
  }

}