import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import '../colors.dart';
import '../models/bankspojo.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';

class BankDetails extends StatefulWidget {
  final String bankId; final String accountName, accountNumber;
  BankDetails(this.bankId, this.accountName, this.accountNumber);

  @override
  BankDetailState createState() => new BankDetailState( bankId, accountName, accountNumber);
}

var _accountnumberController = new TextEditingController();
var _acntNameController = new TextEditingController();
var _bankController = new TextEditingController();
ProgressDialog pr;
bool isMobile = true;

class BankDetailState extends State<BankDetails> {
  String bankId;String accountName, accountNumber;
  BankDetailState(this.bankId, this.accountName, this.accountNumber);

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String currentBank = "Select bank";
  List<String> _banks = new List();

  _getListOfBanks() async {
    _bankController.text = bankId;
    _acntNameController.text = accountName;
    final response = await http.get(

      baseUrl+'/api/banks/',
      headers: headerAuth,
    );

    var bankList = banksPojoFromJson(response.body);
    for(BankListDatum cityData in bankList.data) {
      _banks.add(cityData.bankName);
    }

    setState(() {
      _accountnumberController.text = accountNumber;
      List<DropdownMenuItem<String>> getDropDownMenuItems() {
        List<DropdownMenuItem<String>> items = new List();
        for (String bank in _banks) {
          // here we are creating the drop down menu items, you can customize the item right here
          // but I'll just use a simple text for this
          items.add(new DropdownMenuItem(
              value: bank,
              child: new Text(bank)
          ));
        }
        return items;
      }
      _dropDownMenuItems = getDropDownMenuItems();
       currentBank = _banks[0];
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getListOfBanks();
   
  }
  changedDropDownItem(String selectedCity) {
    setState(() {
      currentBank = selectedCity;
    });
  }

  void updatedInfo(BuildContext context) async {
    pr = new ProgressDialog(context, );
    pr.style(message:'Updating...');
    if(!pr.isShowing()) {
      pr.show();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int userId = (prefs.getInt('userid') ?? 0);

      final response = await http.post(
        baseUrl+'/api/userdetails',

        body: {"id": userId.toString(), "acct_name": _acntNameController.text, "acct_number": _accountnumberController.text, "bank_id": currentBank},
        headers: headerAuth,
      );

      pr.hide();
      if(response.statusCode == 200) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Update successful...'), duration: Duration(seconds:4),));
      }
      else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Failed, please try again...'),
          duration: Duration(seconds: 4),));
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    setState(() {
      if (!useMobileLayout) isMobile = false;
    });

    return Scaffold(
        appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
          Navigator.pop(context);
        }),

          title: Text('View/Edit Bank Details', style: TextStyle(color: Colors.white),),
          backgroundColor: darkGreen,
        ),

        body: Builder(builder: (contexter)=>
        SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            child:  Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("Choose bank name to update"),
                  new DropdownButton(
                    hint: Text('Loading...', style: TextStyle(fontSize: 20),),
                    value: currentBank,
                    iconSize: 48,
                    isDense: true,
                    style: TextStyle(fontSize: 20, color: Colors.grey[800], ),
                    items: _dropDownMenuItems,
                    onChanged: changedDropDownItem,
                  ),

                  TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.grey),controller: _bankController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.account_circle), labelText: 'Your bank name',), obscureText: false, enabled: false,),
                  TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.black),controller: _accountnumberController, decoration: InputDecoration(icon:new Icon(Icons.account_balance_wallet), labelText: 'Account number', hintText: "Loading..."), obscureText: false),
                  TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.black),controller: _acntNameController, decoration: InputDecoration(icon:new Icon(Icons.account_circle), labelText: 'Account name', hintText: "Loading..."), obscureText: false),
                  SizedBox(height: 32,),
                  new SizedBox(
                      width: double.infinity,
                      height: 42,

                      child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                        onPressed: () {
                      updatedInfo(contexter);
                      }, child: Text('Update', style: TextStyle(fontSize:  isMobile ? 18 : 24), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
                  )

                ],
              ),
            )
        )
        )
    );
  }

}