import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import '../colors.dart';

class SocialMediaDetails extends StatefulWidget {
  String facebook, twitter, linkedin, instagram;
  SocialMediaDetails(this.facebook, this.twitter, this.linkedin, this.instagram);

  @override
  SocialMediaDetailState createState() => new SocialMediaDetailState(facebook, twitter, linkedin, instagram);

}

var _facebooklinkController = new TextEditingController();
var _twitterController = new TextEditingController();
var _linkedinController = new TextEditingController();
var _instagramController = new TextEditingController();
ProgressDialog pr;

bool isMobile = true;
class SocialMediaDetailState extends State<SocialMediaDetails> {

  String facebook, twitter, linkedin, instagram;
  SocialMediaDetailState(this.facebook, this.twitter, this.linkedin, this.instagram);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _facebooklinkController.text = facebook;
      _twitterController.text = twitter;
      _linkedinController.text = linkedin;
      _instagramController.text = instagram;
    });
  }


  void updatedInfo(BuildContext context) async {
    pr = new ProgressDialog(context,);
    pr.style(message:'Updating...');
    if(!pr.isShowing()) {
      pr.show();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int userId = (prefs.getInt('userid') ?? 1);


      final response = await http.post(
        baseUrl+'/api/userdetails',

        body: {"id": userId.toString(), "facebook": _facebooklinkController.text,
          "twitter": _twitterController.text, "linkedin": _linkedinController.text,
        "instagram": _instagramController.text},
        headers: {
          "x-authorization":
          "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
        },
      );

      pr.hide();
      if(response.statusCode == 200) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Update successful...'), duration: Duration(seconds:4),));
      }
      else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Failed, please try again...'),
          duration: Duration(seconds: 4),));
      }
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    setState(() {
      if (!useMobileLayout) isMobile = false;
    });


    return Scaffold(
        appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
          Navigator.pop(context);
        }),
          centerTitle: false,
          title: Text('View/Edit NextOfKin Details', style: TextStyle(color: Colors.white),),
          backgroundColor: darkGreen,
        ),

        body: Builder(builder: (contexter)=>
    SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            child:  Container(
              child: Column(
                children: <Widget>[
                  TextField(style: TextStyle( fontSize: isMobile ? 18 : 24  , color: Colors.black),controller: _facebooklinkController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.account_circle), labelText: 'Facebook Profile Link',), obscureText: false),
                  TextField(style: TextStyle( fontSize: isMobile ? 18 : 24, color: Colors.black),controller: _twitterController, decoration: InputDecoration(icon:new Icon(Icons.account_box), labelText: 'Twitter handle(e.g @farmcrowdy)',), obscureText: false),

                  TextField(style: TextStyle(fontSize: isMobile ? 18 : 24, color: Colors.black),controller: _linkedinController, decoration: InputDecoration(icon:new Icon(Icons.all_inclusive), labelText: 'LinkedIn username (e.g farmcrowdy)',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18 : 24, color: Colors.black),controller: _instagramController, decoration: InputDecoration(icon:new Icon(Icons.mail), labelText: 'Instagram handle(e.g @farmcrowdyng)',), obscureText: false),

                  SizedBox(height: 32,),

                  new SizedBox(
                      width: double.infinity,
                      height: 42,

                      child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                        onPressed: () {
                        updatedInfo(contexter);
                      }, child: Text('Update', style: TextStyle(fontSize: isMobile ?18 : 24), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
                  )
                ],
              ),
            )
        )
        )

    );
  }

}