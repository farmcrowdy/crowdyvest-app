import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import '../colors.dart';
import 'package:http/http.dart' as http;

class NextOfKinDetails extends StatefulWidget {
  String firstname, lastname, email, phone, relationshiptype, address;
  NextOfKinDetails(this.firstname, this.lastname, this.email, this.phone, this.relationshiptype, this.address);
  @override
  NextOfKinDetailState createState() => new NextOfKinDetailState(firstname, lastname, email, phone, relationshiptype, address);

}

ProgressDialog pr;
var _firstnameController = new TextEditingController();
var _lastnameController = new TextEditingController();
var _emailController = new TextEditingController();
var _phoneController = new TextEditingController();
var _relationshipController = new TextEditingController();
var _addressController = new TextEditingController();

bool isMobile = true;
class NextOfKinDetailState extends State<NextOfKinDetails> {

  String firstname, lastname, email, phone, relationshiptype, address;
  NextOfKinDetailState(this.firstname, this.lastname, this.email, this.phone, this.relationshiptype, this.address);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    setState(() {
      _firstnameController.text = firstname;
      _lastnameController.text = lastname;
      _emailController.text = email;
      _phoneController.text = phone;
      _relationshipController.text = relationshiptype;
      _addressController.text = address;
    });

  }


  void updatedInfo(BuildContext context) async {
    pr = new ProgressDialog(context, );
    pr.style(message:'Updating...');
    if(!pr.isShowing()) {
      pr.show();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int userId = (prefs.getInt('userid') ?? 1);


      final response = await http.post(
        baseUrl+'/api/userdetails',

        body: {"id": userId.toString(), "nextkin_fname": _firstnameController.text, "nextkin_sname": _lastnameController.text ,"nextkin_email": _emailController.text, "nextkin_relationship": _relationshipController.text,
          "nextkin_mobile":_phoneController.text, "nextkin_address": _addressController.text},
        headers: {
          "x-authorization":
          "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
        },
      );

      pr.hide();
      if(response.statusCode == 200) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Update successful...'), duration: Duration(seconds:4),));
      }
      else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Failed, please try again...'),
          duration: Duration(seconds: 4),));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    setState(() {
      if (!useMobileLayout) isMobile = false;
    });


    return Scaffold(
        appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
          Navigator.pop(context);
        }),

          title: Text('View/Edit NextOfKin Details', style: TextStyle(color: Colors.white),),
          backgroundColor: darkGreen,
        ),

        body: Builder(builder: (contexter)=>
    SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
            child:  Container(
              child: Column(
                children: <Widget>[
                  TextField(style: TextStyle(fontSize: isMobile ? 18: 24, color: Colors.black),controller: _firstnameController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.account_circle), labelText: 'Firstname',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18: 24, color: Colors.black),controller: _lastnameController, decoration: InputDecoration(icon:new Icon(Icons.account_box), labelText: 'Lastname',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18: 24, color: Colors.black),controller: _relationshipController, decoration: InputDecoration(icon:new Icon(Icons.all_inclusive), labelText: 'Relationship type',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18: 24, color: Colors.black),controller: _emailController, decoration: InputDecoration(icon:new Icon(Icons.mail), labelText: 'Email address',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18: 24, color: Colors.black),controller: _phoneController, decoration: InputDecoration(icon:new Icon(Icons.call), labelText: 'Phone number',), obscureText: false),
                  TextField(style: TextStyle(fontSize: isMobile ? 18: 24, color: Colors.black),controller: _addressController, decoration: InputDecoration(icon:new Icon(Icons.location_on), labelText: 'Address',), obscureText: false),

                  SizedBox(height: 32,),

                  new SizedBox(
                      width: double.infinity,
                      height: 42,

                      child: RaisedButton(
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                        onPressed: () {
                        updatedInfo(contexter);
                      }, child: Text('Update', style: TextStyle(fontSize: isMobile ? 18: 24), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
                  )
                ],
              ),
            )
        )
        )
    );
  }

}