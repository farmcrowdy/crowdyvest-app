import 'package:flutter/material.dart';
import '../colors.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class ProfileDetails extends StatefulWidget {
  String firstname,  email, phone, dob, gender, nationaility;
  ProfileDetails(this.firstname,this.email, this.phone, this.dob, this.gender, this.nationaility);

  @override
  ProfileDetailState createState() => new ProfileDetailState(firstname, email, phone, dob, gender, nationaility);

}

var _firstnameController = new TextEditingController();
var _emailController = new TextEditingController();
var _phoneController = new TextEditingController();
var _dobController = new TextEditingController();
var _genderController = new TextEditingController();
var _nationalityController = new TextEditingController();
ProgressDialog pr;

bool isMobile = true;

class ProfileDetailState extends State<ProfileDetails> {

  String firstname, email, phone, dob, gender, nationaility;
  ProfileDetailState(this.firstname, this.email, this.phone, this.dob, this.gender, this.nationaility);


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _firstnameController.text = firstname;
      _emailController.text = email;
      _phoneController.text = phone;
      _dobController.text = dob;
      _genderController.text = gender;
      _nationalityController.text = nationaility;
    });
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    setState(() {
      if (!useMobileLayout) isMobile = false;
    });

    void updatedInfo(BuildContext context) async {
      pr = new ProgressDialog(context,);
      pr.style(message:'Updating...');
      if(!pr.isShowing()) {
        pr.show();
        SharedPreferences prefs = await SharedPreferences.getInstance();
        int userId = (prefs.getInt('userid') ?? 1);


        final response = await http.post(
          baseUrl+'/api/userdetails',

          body: {"id": userId.toString(), "phone_number": _phoneController.text, "date_of_birth": _dobController.text, "gender": _genderController.text,
          "nationality":_nationalityController.text},
          headers: {
            "x-authorization":
            "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
          },
        );

        pr.hide();
        if(response.statusCode == 200) {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Update successful...'), duration: Duration(seconds:4),));
        }
        else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Failed, please try again...'),
            duration: Duration(seconds: 4),));
        }
      }
    }

    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
        Navigator.pop(context);
      }),

      title: Text('View/Edit Profile Details', style: TextStyle(color: Colors.white),),
      backgroundColor: darkGreen,
          ),

      body: Builder(builder: (contexter)=>
      SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        child:  Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextField(style: TextStyle(fontSize: isMobile ? 18 : 24, color: Colors.grey),controller: _firstnameController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.account_circle), labelText: 'Firstname',), obscureText: false, enabled: false,),

              TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.grey),controller: _emailController, decoration: InputDecoration(icon:new Icon(Icons.mail), labelText: 'Email address',), obscureText: false, enabled: false,),
              TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.black),controller: _phoneController, decoration: InputDecoration(icon:new Icon(Icons.call), labelText: 'Phone number',), obscureText: false),
              TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.black),controller: _dobController, decoration: InputDecoration(icon:new Icon(Icons.date_range), labelText: 'Date of birth',), obscureText: false, enabled: true, onTap: (){
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    minTime: DateTime(1940, 3, 5),
                    maxTime: DateTime(2003, 6, 7), onChanged: (date) {
                      print('change $date');
                    }, onConfirm: (date) {
                      String formattedDate = date.year.toString() +"/"+ date.month.toString()+"/"+date.day.toString();
                      setState(() {
                        _dobController.text = formattedDate;
                      });

                      print('confirm $formattedDate');
                    }, currentTime: DateTime.now(), locale: LocaleType.en);
              },),



              TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.black),controller: _genderController, decoration: InputDecoration(icon:new Icon(Icons.group), labelText: 'Gender',), obscureText: false),
              TextField(style: TextStyle(fontSize:  isMobile ? 18 : 24, color: Colors.black),controller: _nationalityController, decoration: InputDecoration(icon:new Icon(Icons.nature_people), labelText: 'Nationality',), obscureText: false),
              SizedBox(height: 32,),

        new SizedBox(
          width: double.infinity,
          height: 42,

          child: RaisedButton(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
            onPressed: () {
              updatedInfo(contexter);
              }, child: Text('Update', style: TextStyle(fontSize:  isMobile ? 18 : 24), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
        )
            ],
          ),
        )
      )
      )

    );
  }

}