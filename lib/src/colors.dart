import 'package:flutter/material.dart';

const kShrinePink50 = const Color(0xFFF1F8E9);
const kShrinePink100 = const Color(0xFFDCEDC8);
const kShrinePink300 = const Color(0xFFAED581);
const kShrinePink400 = const Color(0xFF8BC34A);

const kShrineBrown900 = const Color(0xFF46c634);

const kShrineErrorRed = const Color(0xFFC5032B);

const purpleStart = const Color(0xcF4568dc);
const purpleEnd = const Color(0xcFb06ab3);

const redStart = const Color(0xcfff512f);
const redEnd = const Color(0xcFdd2476);

const greenStart = const Color(0xff53ADBF);
const greenStartTwo = const Color(0xFF7CB8A6);
const darkGreen = const Color(0xFF41747A);
const greenEnd = const Color(0xaa99CC99);
const sharpGreen = const Color(0xffC0D271);
const fadeCV = const Color(0xFFC1E4EE);
const finalTitleColor = const Color(0xff53ADBF);

const kShrineSurfaceWhite = const Color(0xFFFFFBFA);
const kShrineBackgroundWhite = Colors.white;

const String baseUrl = "https://crowdyvest.com";
const String baseUrlFarm = "https://crowdyvest.com";
const String baseUrlCrowdy = "https://crowdyvest.com";
const String consetContent = "This is all consent";

const int TOAST_LENGTH = 7;

BoxDecoration myGradientDecor(Color color1, color2) {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: FractionalOffset.topLeft,
      end: FractionalOffset.bottomRight,
      colors: [
        color1,
        color2,
      ],
    ),
  );
}
const int appCurrentVersion = 4;
