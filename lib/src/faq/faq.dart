import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../models/faqpojo.dart';
import '../colors.dart';
import '../engine/readmore_text.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

class FAQ extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;
  FAQ(this.observer);

  @override
  FAQState createState()=> new FAQState(observer);

}
List<FaqPojo> myPojoList;
bool loaded = false;

class FAQState extends State<FAQ> with RouteAware {

  final FirebaseAnalyticsObserver observer;
  FAQState(this.observer);




  void _sendCurrentTabToAnalytics() {
    lyticsFAQ(observer);
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getFaqContent();
  }
  void getFaqContent() async {
    var response = await http.get("https://api.sheety.co/856bb482-c4a6-48d5-b61f-ee727230c83e");
    if(response.statusCode == 200) {
      setState(() {
        loaded = true;
        myPojoList = faqPojoFromJson(response.body);
      });
    }
    else {
      setState(() {
        loaded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,),
          onPressed: ()=>Navigator.pop(context)),
      backgroundColor: greenStart,
      title: Text("Frequently Asked Questions",style: TextStyle(color: Colors.white),),

      elevation: 0,),
      backgroundColor: greenStart,
      body:loaded ? ListView.builder(
        itemBuilder: _buildAudioItem,
        itemCount: myPojoList.length,
      ) : Center(child: CircularProgressIndicator(backgroundColor: Colors.white,),),
    );
  }

  Widget _buildAudioItem(BuildContext context, int pos) {
    return Card(
      elevation: 4,
      margin: EdgeInsets.all(16),
      child: ListTile(
      contentPadding: EdgeInsets.all(16),
      isThreeLine: true,
      title: Text(myPojoList[pos].question, style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),),
      subtitle: ReadMoreText(
        myPojoList[pos].answer,
        trimLines: 2,
        colorClickableText: darkGreen,
        trimMode: TrimMode.Line,
        trimCollapsedText: '...Show more',
        trimExpandedText: ' show less',
        style: TextStyle(color: Colors.black, height: 1.2, fontSize: 16),
      )),
    );
  }

}

