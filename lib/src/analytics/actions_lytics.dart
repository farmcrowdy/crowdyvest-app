import 'package:date_format/date_format.dart';
import 'package:firebase_analytics/observer.dart';

void loginLytics(FirebaseAnalyticsObserver observer) {
  observer.analytics.logLogin();
}

void signUpLytics(FirebaseAnalyticsObserver observer, String method) {
  observer.analytics.logSignUp(signUpMethod: method);
}

void signUpClickIntention(FirebaseAnalyticsObserver observer) {
  observer.analytics.logEvent(name: "Clicked signup button");
}

void viewingOnboardingScreens(FirebaseAnalyticsObserver observer) {
  observer.analytics.logTutorialBegin();
}

void completedOnboardingScreens(FirebaseAnalyticsObserver observer) {
  observer.analytics.logTutorialComplete();
}

void appOpens(FirebaseAnalyticsObserver observer) {
  observer.analytics.logAppOpen();
}

void followFarmIntention(FirebaseAnalyticsObserver observer) {
  observer.analytics.logEvent(name: "follow farm");
}

void unFollowFarmIntention(FirebaseAnalyticsObserver observer) {
  observer.analytics.logEvent(name: "Unfollow the farm");
}

void intendToSponsorAddedToCart(FirebaseAnalyticsObserver observer, int itemId, String itemName, category, int quantity, double price) {
  observer.analytics.logAddToCart(
      itemId: itemId.toString(),
      itemName: itemName,
      itemCategory:category,
      quantity: quantity,
      price: price);
}

void proceedToCheckout(FirebaseAnalyticsObserver observer, String transId, amount) {

  logPaymentRelated(observer: observer,
      transactionId: transId,
      amount: amount,
      type: 1);

}

void sentMessageToAccountManager(FirebaseAnalyticsObserver observer) {
  observer.analytics.logEvent(name: "sent_message_to_accountmanager");
}

void userLogsOut(FirebaseAnalyticsObserver observer) {
  observer.analytics.logEvent(name: "logout");
}

void paymentOnPaystack(FirebaseAnalyticsObserver observer, String transId, int amount) {

  logPaymentRelated(observer: observer,
  transactionId: transId,
  amount: amount,
  type: 2);

}

void paymentOnMonnify(FirebaseAnalyticsObserver observer, String transId,   amount) {

  logPaymentRelated(observer: observer,
      transactionId: transId,
      amount: amount,
      type: 3);

}

void logPaymentRelated({FirebaseAnalyticsObserver observer, String transactionId, int amount, int  type}) {
  String logtype = "";
  switch(type) {
    case 1: logtype = "begin_checkout";
      break;
    case 2: logtype = "payment_with_paystack_mobile";
      break;
    case 3: logtype = "payment_with_monnify_tentative_mobile";
      break;
  }
  observer.analytics.logEvent(
    name: logtype,
    parameters: (<String, dynamic>{ "_TRANSACTION_ID": transactionId, "_AMOUNT": amount }),
  );
}



