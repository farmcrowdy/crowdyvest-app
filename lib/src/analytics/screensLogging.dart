import 'package:firebase_analytics/observer.dart';

void lyticsPushNotificationOpen( FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "notification_has_opened_page");
  observer.analytics.logEvent(name: "user_opens_notication");
}
void lyticsUpdateApp(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "force_update_app_page");
}
void lyticsLogin(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "login_page");
}

void lyticsSignUp(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "signup_page");
}

void lyticsDashboard(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "dashboard_page");
}

void lyticsPortfolio(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "portfolio_page");
}

void lyticsPortfolioDetails(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "portfolio_details_page");
}

void lyticsCartPage(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "cart_page");
}

void lyticsFollowedProject(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "followed_projects_page");
}

void lyticsMainProfile(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "mainprofile_page");
}

void lyticsProjectUpdates(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "project_updates_page");
}

void lyticsSponsorshipHistory(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "sponsorship_history_page");
}

void lyticsSponsoredProjects(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "sponsored_projects_page");
}

void lyticsAccountManager(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "account_manager_page");
}

void lyticsFAQ(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "faq_page");
}

void lyticsCheckoutMobilePage(FirebaseAnalyticsObserver observer) {
  observer.analytics.setCurrentScreen(screenName: "checkout_mobile_page");
}