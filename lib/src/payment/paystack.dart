import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import '../analytics/actions_lytics.dart';


Future<bool> payWithPaystack(BuildContext context, int amountInNaira, String email, paymentRef) async {

  String _getMobileType() {
    String platform;
    if (Platform.isIOS) {
      platform = 'iOS app';
    } else {
      platform = 'Android app';
    }
    return platform;
  }

  String myMainRef = paymentRef.toString().split("&&")[2].toString();
  String mainSubAccount = paymentRef.toString().split("&&")[1];
  String companyId = paymentRef.toString().split("&&")[0];

  print("my main subAccount is: "+mainSubAccount);

  Charge charge = Charge()
   // ..accessCode = "testing"
    ..reference = myMainRef
    ..amount = amountInNaira * 100
    //..subAccount = mainSubAccount
    //..putCustomField("pay_source", "mobile")
    ..email = email;

  if(companyId != "1") charge.subAccount = mainSubAccount;

  CheckoutResponse response = await PaystackPlugin.checkout(context, charge: charge,method: CheckoutMethod.card, fullscreen: true);

  return response.status;

}