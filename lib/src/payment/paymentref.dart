// To parse this JSON data, do
//
//     final paymentRefModel = paymentRefModelFromJson(jsonString);

import 'dart:convert';

PaymentRefModel paymentRefModelFromJson(String str) => PaymentRefModel.fromJson(json.decode(str));

String paymentRefModelToJson(PaymentRefModel data) => json.encode(data.toJson());

class PaymentRefModel {
  int payid;
  Company company;

  PaymentRefModel({
    this.payid,
    this.company,
  });

  factory PaymentRefModel.fromJson(Map<String, dynamic> json) => PaymentRefModel(
    payid: json["payid"],
    company: Company.fromJson(json["company"]),
  );

  Map<String, dynamic> toJson() => {
    "payid": payid,
    "company": company.toJson(),
  };
}

class Company {
  int id;
  String paystack_code;

  Company({
    this.id,
    this.paystack_code
  });

  factory Company.fromJson(Map<String, dynamic> json) => Company(
    id: json["id"],
    paystack_code: json["paystack_code"]
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "paystack_code": paystack_code
  };
}
