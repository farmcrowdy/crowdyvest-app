import 'package:crowdyvest/src/engine/auth.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'paymentref.dart';
import '../colors.dart';

Future<String> doCheckout(int userId)  async {
  //First PrepareTheCheckout to move to holding.
  String myRefWithSubAccount = "0";
  var response = await http.get(baseUrlCrowdy+"/api/preparecheckout/"+userId.toString(), headers: headerAuth);

  print("Prepare response is: "+ response.statusCode.toString());

  var _data = await http.post(baseUrlCrowdy+"/api/checkout",
      headers: headerAuth,
      body: {"gateway": "Paystack", "user_id":userId.toString()});

  print("Main checkout response is: "+ response.body);


    if(_data.statusCode == 200) {
      final paymentRefModel = paymentRefModelFromJson(_data.body);
      String tempSubAcnt = "";
      if(paymentRefModel.company.paystack_code == null || paymentRefModel.company.paystack_code.isEmpty) tempSubAcnt="empty";
      else tempSubAcnt = paymentRefModel.company.paystack_code;

      myRefWithSubAccount = paymentRefModel.company.id.toString()+"&&"+ tempSubAcnt+"&&"+"crdy-"+paymentRefModel.payid.toString();
      //return myRefWithSubAccount;
    }

  
return myRefWithSubAccount;



}