import 'package:flutter/material.dart';
import '../colors.dart';
import '../dashboard/dashboard.dart';
import 'dart:async';
import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';
import '../sponsorship_history/sponsorshiphistory.dart';
import 'package:firebase_analytics/observer.dart';

class MyWebPage extends StatefulWidget {
  final String url;
  final FirebaseAnalyticsObserver observer;

  MyWebPage(this.url, this.observer);

  @override
  MyWebPageState createState()=> new MyWebPageState(url, observer);

}


class MyWebPageState extends State<MyWebPage> with RouteAware{
  String url;
  final FirebaseAnalyticsObserver observer;
  MyWebPageState(this.url, this.observer);

  bool gotoDashboard=false;


  InAppWebViewController webView;
  double progress = 0;

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    observer.analytics.logEvent(name: "on_mobile_payment_monnify");
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
        Navigator.pop(context);

      }), title: Text("Payment", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),), elevation: 0,
        centerTitle: false, backgroundColor: darkGreen,),
      backgroundColor: darkGreen,
      body:  Container(
        child: Column(
          children: <Widget>[
            (progress != 1.0) ? LinearProgressIndicator(value: progress, backgroundColor: Colors.grey[300],) : null,
            Expanded(
              child: Card(
                margin: const EdgeInsets.all(10.0),
                child: InAppWebView(
                  initialUrl: url,
                  initialHeaders: {

                  },
                  initialOptions: {
                    "clearCache": true
                  },
                  onWebViewCreated: (InAppWebViewController controller) {
                    webView = controller;
                  },
                  onLoadStart: (InAppWebViewController controller, String url) {
                    print("started $url");
                    print("started $url");
                    setState(() {
                      this.url = url;
                    });

                  },
                  onProgressChanged: (InAppWebViewController controller, int progress) {
                    setState(() {
                      this.progress = progress/100;
                    });
                  },
                ),
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton.icon(
                icon: Icon(Icons.check),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  label: Text("Done With Payment"),
                  onPressed: () {
                    if (webView != null) {
                     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>DashboardPageState(observer)));
                    }
                  },
                ),

                SizedBox(width: 8,),

                IconButton(
                  icon: Icon(Icons.refresh, color: Colors.white,),
                  //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  onPressed: () {
                    if (webView != null) {
                      webView.reload();
                    }
                  },
                ),
              ],
            ),
            SizedBox(height: 32,),
          ].where((Object o) => o != null).toList(),
        ),
      ),

    );
  }
}

// amount, email, name.

//checkout - gateway (string), user_id: (int)