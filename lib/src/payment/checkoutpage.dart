import 'dart:ui';
import 'package:crowdyvest/src/payment/paystack.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:shared_preferences/shared_preferences.dart';
import 'payment_webpage.dart';
import '../colors.dart';
import '../models/cartdetails.dart';
import 'package:encrypt/encrypt.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';
import '../analytics/actions_lytics.dart';
import 'package:flutter_paystack/flutter_paystack.dart';
import 'package:intl/intl.dart';
import 'api_payments.dart';
import '../dashboard/dashboard.dart';
import 'package:async_loader/async_loader.dart';


var publicKey = 'pk_live_5016efaa21685368a0bffdc0ac72fea8cca4e8e9';
//var publicKey = 'pk_test_74f3eb6362173647ca0c399baf5efda5ba7dc1c1 ';

class CheckOutPage extends StatefulWidget {
  final int totalAmount; final List<Datum> cartDetails;
  final FirebaseAnalyticsObserver observer;
  final String paymentRefWithSubAccount;
  CheckOutPage(this.totalAmount, this.cartDetails, this.observer, this.paymentRefWithSubAccount);

  @override
  CheckOutPageState createState() => new CheckOutPageState(totalAmount, cartDetails, observer, paymentRefWithSubAccount);
}
final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

int  userId = 1;
String emailSt;
String token;

final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 2);



class CheckOutPageState extends State<CheckOutPage> with RouteAware {
  final int totalAmount;
  final List<Datum> cartDetails;
  final FirebaseAnalyticsObserver observer;
  final String paymentRefWithSubAccount;

  CheckOutPageState(this.totalAmount, this.cartDetails, this.observer,
      this.paymentRefWithSubAccount);


  _checkForSponsorId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = (prefs.getInt('userid') ?? 1);
    emailSt = prefs.getString("email");
    token = prefs.getString("token");
  }


  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsCheckoutMobilePage(observer);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checkForSponsorId();
    PaystackPlugin.initialize(publicKey: publicKey);
    proceedToCheckout(observer, paymentRefWithSubAccount, totalAmount);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.keyboard_backspace, color: Colors.white,),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('Checkout', style: TextStyle(color: Colors.white),),
        centerTitle: false,
        elevation: 0,
        backgroundColor: darkGreen,),
      backgroundColor: darkGreen,

      body: Center(
          child: (paymentRefWithSubAccount == "0")
              ? _refActionError(context)
              : _completedBody(context, totalAmount, observer, cartDetails, paymentRefWithSubAccount)
      ),
    );
  }



}



  Widget _refActionError(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Something went wrong while preparing your cart. Either your cart is empty or a project has been sold out",
            style: prefix0.TextStyle(color: Colors.white, fontSize: 18), textAlign: TextAlign.center,),

            SizedBox(height: 16,),
            RaisedButton.icon(onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.refresh),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                label: Text("Try again"))
          ],
        ));
  }


  Widget _completedBody(BuildContext context, int totalAmount, FirebaseAnalyticsObserver observer, List<Datum> cartDetails, String paymentRef) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,

        children: <Widget>[
          SizedBox(height: 36,),
          Text('TOTAL PAYMENT',
            style: TextStyle(fontSize: 18, color: Colors.white),),
          SizedBox(height: 8,),
          Text('NGN ${formatCurrency.format(totalAmount)}', style: TextStyle(
              fontSize: 28, color: Colors.white, fontWeight: FontWeight.bold),),

          SizedBox(height: 22,),

          Card(
              elevation: 5,
              color: Colors.white,
              margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                child: ListTile(
                  title: Container(margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                      child: Text('Pay With NairaCard', style: TextStyle(
                          fontSize: 20,
                          color: darkGreen,
                          fontWeight: FontWeight.bold),)),
                  subtitle: Text('(Powered by Paystack)',
                    style: TextStyle(fontSize: 16, color: Colors.black),),
                  trailing: Icon(Icons.chevron_right, color: greenStart,),
                  onTap: () async{
                    String myMainRef = paymentRef;

                    print("my main ref is: "+ myMainRef);
                    if(await payWithPaystack(context, totalAmount, emailSt, paymentRef)) {
                       logPaymentRelated(observer: observer, amount: totalAmount, transactionId: paymentRef,type: 2);
                       Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>DashboardPageState(observer)));
                    };
                  },
                ),
              )

          ),


          SizedBox(height: 22,),



        ////////SUPPORTING MONNIFY, TO OPEN WEBPAGE.

        /*   Card(
              elevation: 5,
              color: Colors.white,
              margin: EdgeInsets.fromLTRB(16, 8, 16, 8),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 16, horizontal: 8),
                child: ListTile(
                  title: Container(margin: EdgeInsets.fromLTRB(0, 0, 0, 8),
                      child: Text('Instant Bank Transfer', style: TextStyle(
                          fontSize: 20,
                          color: darkGreen,
                          fontWeight: FontWeight.bold),)),
                  subtitle: Text('(Powered by Monnify)',
                    style: TextStyle(fontSize: 16, color: Colors.black),),
                  trailing: Icon(Icons.chevron_right, color: greenStart,),
                  onTap: () {
                    paymentOnMonnify(observer, paymentRef, totalAmount);

                    final encrypter = new Encrypter(new Salsa20(
                        'vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
                    String decryptedText = encrypter.decrypt(token);
                    String totalLink = baseUrlCrowdy + "/payment/mobile/" +
                        Uri.encodeComponent(
                            "email=$emailSt&pass=$decryptedText");
                    print('ECNAPS: ' + totalLink);

                    for (Datum mydata in cartDetails) {
                      _firebaseMessaging.subscribeToTopic(
                          "farmupdate_" + mydata.farmId.toString());
                    }
                    _firebaseMessaging.subscribeToTopic(
                        "sponsor_" + userId.toString());

                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) => MyWebPage(totalLink, observer)));
                  },
                ),
              )

          ), */


        ],
      ),
    );
  }


