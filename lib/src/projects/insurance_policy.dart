import '../colors.dart';
import 'package:flutter/material.dart';
import '../engine/values.dart';

class InsuranePolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(centerTitle: false,
        leading: IconButton(onPressed: () {
          Navigator.pop(context);
        }, icon: Icon(Icons.keyboard_backspace), color: Colors.white,),
        backgroundColor: darkGreen,
        title: Text(
          'Insurance Policy', style: TextStyle(color: Colors.white),),
      ),


      body: SingleChildScrollView(
        child: Container(
        padding: EdgeInsets.all(16),
        child: Text(insurancePolicy, style: TextStyle(color: Colors.black, fontSize: 18),),
      ),),
      backgroundColor: Colors.white,
    );
  }

}