import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:async';
import '../colors.dart';
import '../dashboard/dashboard.dart';
import 'projectInfoDetails.dart';
import '../profile/mainprofile.dart';
import '../models/farmshoplistpojo.dart';
import '../models/farmstatuspojo.dart';
import '../engine/noAnimationPageRouter.dart';
import '../engine/values.dart';
import '../analytics/screensLogging.dart';
import 'package:firebase_analytics/observer.dart';


class FarmShopPageState extends StatefulWidget {
  FirebaseAnalyticsObserver observer;


  FarmShopPageState(this.observer);

  @override
  FarmShopPage createState() => new FarmShopPage(observer);

}
final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 2);
class FarmShopPage extends State<FarmShopPageState> with RouteAware {


  FirebaseAnalyticsObserver observer;

  FarmShopPage(this.observer);

  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String currentCity = "Testing";
  String currentStatusId = "1";
  bool changed1 = false;
  bool changed2 = false;
  bool changed3 = false;
  bool changed4 = false;
  bool changed5 = false;
  bool changed6 = false;
  bool changed7 = false;
  bool changed8 = false;
  bool changed9 = false;
  int ccd = 0;
  String initString = "";
  List _cities =
  ["Active farms", "Opening soon", "Sold out"];

  List _status_id = ["1","2","3"];

  _getListOfStatus() async {
    final response = await http.get(
      baseUrlCrowdy+'/api/status/', headers: headerAuth,
    );
    if (response.statusCode == 200) {
      _cities.clear();
      _status_id.clear();
      final farmStatus = farmStatusFromJson(response.body);

        for (DatumStatusStatus datum in farmStatus.data) {
          _cities.add(datum.name);
          _status_id.add(datum.id);

        }
      // here we are creating the list needed for the DropDownButton
      setState(() {
        print(_cities);
        List<DropdownMenuItem<String>> getDropDownMenuItems() {
          List<DropdownMenuItem<String>> items = new List();
          for (String city in _cities) {
            // here we are creating the drop down menu items, you can customize the item right here
            // but I'll just use a simple text for this
            items.add(new DropdownMenuItem(
                value: city,
                child: new Text(city)
            ));
          }
          return items;
        }
        _dropDownMenuItems = getDropDownMenuItems();
        currentCity = _dropDownMenuItems[0].value;
         changed1 = true;
         changed2 = false;
         changed3 = false;
         changed4 = false;
         changed5 = false;
         changed6 = false;
         changed7 = false;
         changed8 = false;
         changed9 = false;
      });

    }
    else {
      print('Something went worng');
    }
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsPortfolio(observer);
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    observer.unsubscribe(this);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getListOfStatus();
  }
  void upderm(String pos) {
    switch(currentStatusId) {
      case "1" : changed1 = true;
      changed2 = false;
      changed3 = false;
      changed4 = false;
      changed5 = false;
      changed6 = false;
      changed7 = false;
      changed8 = false;
      changed9 = false;
      break;

      case "2":  changed1 = false;
    changed2 = true;
    changed3 = false;
    changed4 = false;
    changed5 = false;
    changed6 = false;
    changed7 = false;
    changed8 = false;
    changed9 = false;
    break;

      case "3":  changed1 = false;
      changed2 = false;
      changed3 = true;
      changed4 = false;
      changed5 = false;
      changed6 = false;
      changed7 = false;
      changed8 = false;
      changed9 = false;
      break;

      case "4":  changed1 = false;
      changed2 = false;
      changed3 = false;
      changed4 = true;
      changed5 = false;
      changed6 = false;
      changed7 = false;
      changed8 = false;
      changed9 = false;
      break;

      case "5":  changed1 = false;
      changed2 = false;
      changed3 = false;
      changed4 = false;
      changed5 = true;
      changed6 = false;
      changed7 = false;
      changed8 = false;
      changed9 = false;
      break;

      case "6":  changed1 = false;
      changed2 = false;
      changed3 = false;
      changed4 = false;
      changed5 = false;
      changed6 = true;
      changed7 = false;
      changed8 = false;
      changed9 = false;
      break;

      case "7":  changed1 = false;
      changed2 = false;
      changed3 = false;
      changed4 = false;
      changed5 = false;
      changed6 = false;
      changed7 = true;
      changed8 = false;
      changed9 = false;
      break;

      case "8":  changed1 = false;
      changed2 = false;
      changed3 = false;
      changed4 = false;
      changed5 = false;
      changed6 = false;
      changed7 = false;
      changed8 = true;
      changed9 = false;
      break;

      case "9":  changed1 = false;
      changed2 = false;
      changed3 = false;
      changed4 = false;
      changed5 = false;
      changed6 = false;
      changed7 = false;
      changed8 = false;
      changed9 = true;
      break;

    }
  }


  @override
  Widget build(BuildContext context) {
    void changedDropDownItem(String selectedCity) {
      int statusIndex = _cities.indexOf(selectedCity);

      setState(() {
        currentStatusId = _status_id[statusIndex].toString();
        currentCity = selectedCity;
        upderm(currentStatusId);
      });

    }
    void onTabTapped(int index) {
      switch(index) {
        case 0: Navigator.push(context, NoAnimationPageRouter(builder: (context)=>DashboardPageState(observer)));
          break;
        case 2: Navigator.push(context, NoAnimationPageRouter(builder: (context)=>MainProfile(observer)));
          break;
      }
    }

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    if (useMobileLayout) {
      return Scaffold(
          appBar: AppBar(title: Text(Portfolio, style: TextStyle(color: Colors.white, fontSize: 18), textAlign: TextAlign.start,), backgroundColor: darkGreen,leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
            Navigator.pop(context);
          }),centerTitle: false,
          ),

          body: Stack(
            children: <Widget>[


              Container(
                decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.grey[200]

                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(width: 16,),
                    Text(Filter_Offerings_by, style: TextStyle(color: Colors.grey[800]),),
                    SizedBox(width: 16,),
                    new DropdownButton(
                      hint: Text('Loading...'),
                      value: '$currentCity',
                      iconSize: 48,
                      isDense: true,
                      style: TextStyle(fontSize: 14, color: Colors.grey[800], ),
                      items: _dropDownMenuItems,
                      onChanged: changedDropDownItem,
                    ),
                  ],
                ),
              ),
              changed1 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("1", currentCity),)) : Container (width: 0, height: 0,),

              changed2 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("2", currentCity),)) : Container (width: 0, height: 0,),

              changed3 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("3", currentCity),)) : Container (width: 0, height: 0,),
              changed4 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("4", currentCity),)) : Container (width: 0, height: 0,),
              changed5 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("5", currentCity),)) : Container (width: 0, height: 0,),
              changed6 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("6", currentCity),)) : Container (width: 0, height: 0,),
              changed7 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("7", currentCity),)) : Container (width: 0, height: 0,),
              changed8 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("8", currentCity),)) : Container (width: 0, height: 0,),
              changed9 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("9", currentCity),)) : Container (width: 0, height: 0,),

            ],

          ),
          bottomNavigationBar: Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[500],
                  offset: Offset(0.01, 0.02),
                  blurRadius: 5.0,
                ),
              ],


            ),
            child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                onTap: onTabTapped, // new
                // new
                items: [
                  new BottomNavigationBarItem(
                    icon: Container(child: Image.asset("assets/dash13.png"), width: 24, height: 24,),
                    title: Text(Dashboard, style: TextStyle(color: Colors.black),),
                  ),
                  new BottomNavigationBarItem(
                    icon: Icon(Icons.shopping_basket, color: darkGreen, size: 24,),
                    title: Text(Portfolio, style: TextStyle(color: greenStart, fontWeight: FontWeight.bold),),
                  ),
                  new BottomNavigationBarItem(
                      icon: Container(child: Image.asset("assets/dash14.png"), width: 24, height: 24,),
                      title: Text(Profile, style: TextStyle(color: Colors.black),)),
                ]
            ),
          )
      );
    }

    else {
      return Scaffold(
          appBar: AppBar(title: Text(Portfolio, style: TextStyle(color: Colors.white, fontSize: 22), textAlign: TextAlign.start,), backgroundColor: darkGreen,leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
            Navigator.pop(context);
          }),centerTitle: false,
          ),

          body: Stack(
            children: <Widget>[


              Container(
                decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.grey[200]

                ),
                child:  Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(width: 16,),
                    Text(Filter_Offerings_by, style: TextStyle(color: Colors.grey[800], fontSize: 22),),
                    SizedBox(width: 16,),
                    new DropdownButton(
                      hint: Text('Loading...', style: TextStyle(fontSize: 22),),
                      value: '$currentCity',
                      iconSize: 48,
                      isDense: true,
                      style: TextStyle(fontSize: 22, color: Colors.grey[800], ),
                      items: _dropDownMenuItems,
                      onChanged: changedDropDownItem,
                    ),
                  ],
                ),
              ),
              changed1 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("1", currentCity),)) : Container (width: 0, height: 0,),

              changed2 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("2", currentCity),)) : Container (width: 0, height: 0,),

              changed3 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("3", currentCity),)) : Container (width: 0, height: 0,),
              changed4 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("4", currentCity),)) : Container (width: 0, height: 0,),
              changed5 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("5", currentCity),)) : Container (width: 0, height: 0,),
              changed6 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("6", currentCity),)) : Container (width: 0, height: 0,),
              changed7 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("7", currentCity),)) : Container (width: 0, height: 0,),
              changed8 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("8", currentCity),)) : Container (width: 0, height: 0,),
              changed9 ?  Container(
                  margin: EdgeInsets.fromLTRB(0, 48, 0, 0),
                  child: Center(child:  _asyncLoaderProjects("9", currentCity),)) : Container (width: 0, height: 0,),

            ],

          ),
          bottomNavigationBar: Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[500],
                  offset: Offset(0.01, 0.02),
                  blurRadius: 5.0,
                ),
              ],


            ),
            child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                onTap: onTabTapped, // new
                // new
                items: [
                  new BottomNavigationBarItem(
                    icon: Container(child: Image.asset("assets/dash13.png"), width: 34, height: 34,),
                    title: Text(Dashboard, style: TextStyle(color: Colors.black, fontSize: 26),),
                  ),
                  new BottomNavigationBarItem(
                    icon: Icon(Icons.shopping_basket, color: darkGreen, size: 34,),
                    title: Text(Portfolio, style: TextStyle(color: greenStart, fontSize: 26, fontWeight: FontWeight.bold),),
                  ),
                  new BottomNavigationBarItem(
                      icon: Container(child: Image.asset("assets/dash14.png"), width: 34, height: 34,),
                      title: Text(Profile, style: TextStyle(color: Colors.black, fontSize: 26),)),
                ]
            ),
          )
      );
    }
  }


_getListOfProjects(String statusId) async {
  initString = statusId;
  final response = await http.get(
    baseUrlCrowdy+'/api/farmbystatus/'+statusId,
    headers: headerAuth,
  );
  return response.body;
}


_asyncLoaderProjects(String au, String showTitle)=> new AsyncLoader(

    initState: () async => await _getListOfProjects(au),
    renderLoad: () =>new CircularProgressIndicator(),
    renderError: ([error]) =>
    new Text('Sorry, there was an error loading. Please check internet connection', style: TextStyle(color: Colors.black),),
    renderSuccess: ({data}) {
      try {
        var res = shoplistFromJson(data);
        if(res.data.length == 0) {
          return Container(padding: EdgeInsets.all(16), child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('No projects available currently in $showTitle.', style: TextStyle(color: Colors.grey, fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
              Text('Please check back later. You will also be notified of new sponsorship opportunities.', style:TextStyle(color: Colors.grey, fontSize: 18), textAlign: TextAlign.center,)
            ],
          ));
        }
        else return _buildProjects(res, showTitle, au);
      }
      catch(e) {
        return Text('There is no portfolio that are: '+showTitle, style: TextStyle(color: greenStart, fontSize: 18), textAlign: TextAlign.center,);
      }
    }

);


Widget _buildProjects(Shoplist listOfFarmShop, String showTitle, String authorId) {
    initString = authorId;
  return new GridView.builder(
      itemCount: listOfFarmShop.data.length,
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 250.0,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 1.0,
        childAspectRatio: 0.75,
      ),
      semanticChildCount: 2,
      itemBuilder: (BuildContext _context, int i) {

        return _buildRow(listOfFarmShop.data[i]);
      }
  );
}

Widget _buildRow(Datum farmshopData) {
  //print('we are now');
  return new GestureDetector(onTap: () {
    Navigator.push(context, MaterialPageRoute(builder: (context) =>
        FarmInfoPage(farmshopData, observer)));
  },
      child: Card(
        elevation: 2,
        margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              height: 100,
              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
              decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  color:  Colors.grey,
                  image: DecorationImage(image: NetworkImage(baseUrlCrowdy+'/storage/farmimages/'+farmshopData.farmImage), fit: BoxFit.cover)
              ),
            ),

            //Image.asset('assets/farmcrowdy_top_bg_login.jpg'),

            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(farmshopData.farmtypename[0].toUpperCase()+farmshopData.farmtypename.substring(1),
                    style: TextStyle(color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),),
                  Text(farmshopData.statename,
                    style: TextStyle(color: Colors.grey[500], fontSize: 14,),),
                  SizedBox(height: 4,),
                  Text('N${formatCurrency.format(farmshopData.pricePerunit)}',
                    style: TextStyle(fontSize: 16, color: greenStart),),
                  SizedBox(height: 4,),
                  Text('Returns ' + farmshopData.roi.toString() + '% in ' +
                      farmshopData.farmCycle.toString() + ' months',
                    style: TextStyle(color: Colors.grey[700]),)
                ],
              ),
            ),


          ],

        ),
      ));
}             // ... to here.

}

