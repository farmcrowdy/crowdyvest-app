import 'package:crowdyvest/src/analytics/actions_lytics.dart';
import 'package:crowdyvest/src/analytics/screensLogging.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'project_desc.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
//import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:share/share.dart';
import '../cart/cart.dart';
import 'insurance_policy.dart';
import '../colors.dart';
import '../models/farmfollowing.dart';
import '../models/farmshoplistpojo.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import '../engine/values.dart';
import 'package:firebase_analytics/observer.dart';

class FarmInfoPage extends StatefulWidget {
  final Datum farmdatum;
  final FirebaseAnalyticsObserver observer;
  FarmInfoPage(this.farmdatum, this.observer);

  @override
  FarmInfoPageState createState()=> new FarmInfoPageState(farmdatum, observer);
}
bool isMobile = true;
final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 2);
TextEditingController unitController = new TextEditingController();


class FarmInfoPageState extends State<FarmInfoPage> with RouteAware{
  final Datum farmdatum;
  final FirebaseAnalyticsObserver observer;
  FarmInfoPageState(this.farmdatum, this.observer);


  int unitPrice = 0;
  double returnOnHarvest = 0;
  double totalpayback = 0;
  int farmunits = 1;
  int isFarmFollowed = 0;
  double totalprice = 0;


  _checkForFarmFollowed() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = (prefs.getInt('userid') ?? 1);

    final response = await http.get(
      baseUrl+'/api/isfollowingfarm/'+userId.toString()+'/'+farmdatum.id.toString(),
      headers: {
        "x-authorization": "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
      },
    );

    print('add carting response is: '+response.body);
    int doTempresponse = 0;
    if(response.statusCode == 200) {
      var resultFollowing = farmFollowingFromJson(response.body);
      if(resultFollowing.data) doTempresponse = 2;
      else doTempresponse = 1;
    }
    else {
      doTempresponse = 0;
    }
    setState(() {
      isFarmFollowed = doTempresponse;
    });
  }


  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsPortfolioDetails(observer);
  }





  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      farmunits = 1;
      unitController.text = '1';
      unitPrice = farmdatum.pricePerunit * 1;
      returnOnHarvest = double.parse(((farmdatum.roi/100) * farmdatum.pricePerunit).toStringAsFixed(3));
      totalpayback = unitPrice +returnOnHarvest;
      totalprice = (unitPrice *0.02) + unitPrice;

      if(farmdatum.unitLeft < 0) farmdatum.unitLeft = 0;


    });
    _checkForFarmFollowed();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;
    if (!useMobileLayout) {
      setState(() {
        isMobile = false;
      });
    }

    double percentLeft = farmdatum.unitLeft/farmdatum.totalUnit;
    int percentLeftInInteger = int.parse((percentLeft * 100).toStringAsFixed(0));
    String percentLeftText = (percentLeft * 100).toStringAsFixed(0) + "% of units left";

    if(percentLeftInInteger <11) {
      percentLeftText = farmdatum.unitLeft.toString() + " units left";
      if(farmdatum.unitLeft == 0) {
        percentLeftText = "0 unit left - Sold out";
      }

    }
    return Scaffold(
        appBar: AppBar(centerTitle:false, leading: IconButton(onPressed: () {
          Navigator.pop(context);
        }, icon: Icon(Icons.keyboard_backspace), color: Colors.white, ),
          backgroundColor: darkGreen, title: Text(Offering_details, style: TextStyle(color: Colors.white, fontSize: isMobile ? 18:22),),

          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.share,
                  color: Colors.white,
                ),
                onPressed: () {
                  Share.share(sharingContent);
                })
          ],
         ),

        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                  height: isMobile ? 200 : 300,
                  margin: EdgeInsets.fromLTRB(0,16, 0, 16),
                  decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color:  Colors.grey,
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(image: NetworkImage(baseUrl+'/storage/farmimages/'+farmdatum.farmImage), fit: BoxFit.cover)
                  ),
              ),
              Text(farmdatum.farmtypename[0].toUpperCase()+farmdatum.farmtypename.substring(1), style: TextStyle(color:Colors.black, fontSize: isMobile ? 20: 28, fontWeight: FontWeight.bold),),

              SizedBox(height: 4,),
              Text(farmdatum.statename, style: TextStyle(color: Colors.grey[700], fontSize: isMobile ?14 :22),),
              SizedBox(height: 8,),
              //LinearProgressIndicator(value: percentLeft, backgroundColor: greenEnd,  valueColor: new AlwaysStoppedAnimation<Color>(greenStart),),
              Text('Returns '+farmdatum.roi.toString()+'% in '+farmdatum.farmCycle.toString()+' months', style: TextStyle(color: Colors.black, fontSize: isMobile ? 20: 26),),
             SizedBox(height: 16,),
              Padding(padding: EdgeInsets.only(left: 1), child: Text("$percentLeftText")),
              LinearPercentIndicator(
                width: 300,
                lineHeight: 8.0,
                percent: percentLeft ,
                animation: true,
                animateFromLastPercent: true,
                animationDuration: 2000,
                linearStrokeCap: LinearStrokeCap.roundAll,
                backgroundColor: greenEnd,
                progressColor: greenStart,
              ),
              SizedBox(height: 16,),

        Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton.icon(onPressed: () {
                   Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmPoli(farmdatum.farmDescription)));
                  }, icon: Icon(Icons.error_outline, color: greenStart,), label: Text(Offering_Details_as_a_button, style: TextStyle(fontSize: isMobile ? 16 : 22, color: greenStart),)),
                  
                  IconButton(icon: Container(child: Image.network(baseUrl+"/storage/images/"+farmdatum.insuranceDetails.logo, width: isMobile ? 40 : 90, height: isMobile ? 40 : 90,),), onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>InsuranePolicy()))),
                ],
              ),
              SizedBox(height: isMobile ? 0: 30,),
              Card(
                  elevation: 4,
                  color: Colors.white,
                  margin: EdgeInsets.fromLTRB(0, 0,0,16),

                  child:
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('Price', style: TextStyle(fontSize: isMobile ? 14 : 20, color: Colors.black),),
                                SizedBox(height: 8,),
                                Text('N '+ '${formatCurrency.format(unitPrice)} ', style: TextStyle(fontSize: isMobile ? 20: 28, color: Colors.black, fontWeight: FontWeight.bold),),

                              ],
                            ),


                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(height: 12,),
                                Text(Offering_units, style: TextStyle(fontSize: isMobile ? 14:20, color: Colors.black),),
                                SizedBox(height: 8,),
                                Container(
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: Colors.white,
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[500],
                                        offset: Offset(0.01, 0.01),
                                        blurRadius: 1.0,
                                      ),
                                    ],


                                  ),
                                  child: Row(
                                  children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        unitController.text = (farmunits-1).toString();
                                        farmunits = farmunits-1;
                                        if(farmunits<1) {
                                          farmunits = 1;
                                          unitController.text = '1';
                                        }
                                        unitPrice = farmdatum.pricePerunit * farmunits;
                                        returnOnHarvest = double.parse(((farmdatum.roi/100) * unitPrice).toStringAsFixed(3));
                                        totalpayback = unitPrice + returnOnHarvest;
                                        totalprice = (unitPrice *0.02) + unitPrice;
                                      });
                                    },
                                    child: Container(
                                      width: 50,
                                      height: 40,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Colors.grey[200],
                                      ),
                                      child: Center(
                                        child: Text('-', style: TextStyle(fontSize: isMobile? 20:28, color: Colors.black), textAlign: TextAlign.start,) ,
                                      )

                                    ),
                                  ),

                    Container(
                        width: 50,
                        height: 40,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                        ),
                        child: Center(
                          child: TextField(controller: unitController,
                            decoration: new InputDecoration(
                              border: InputBorder.none,
                            ),
                            style: TextStyle(fontSize: 20, color: Colors.black), textAlign: TextAlign.center, onChanged: (text) {
                            setState(() {
                              farmunits = int.parse(text);
                              if(farmunits <= farmdatum.unitLeft) {
                                if (farmunits < 1) farmunits = 1;
                                unitPrice = farmdatum.pricePerunit * farmunits;
                                returnOnHarvest = double.parse(((farmdatum.roi / 100) * unitPrice).toStringAsFixed(3));
                                totalpayback = unitPrice + returnOnHarvest;
                                totalprice = (unitPrice * 0.02) + unitPrice;
                              }
                              else {
                                unitController.text = farmdatum.unitLeft.toString();
                              }
                            });

                          },) ,
                        )

                    ),

                                  GestureDetector(
                                    onTap: () {
                                setState(() {
                                  if(farmunits <= farmdatum.unitLeft-1) {
                                    unitController.text = (farmunits + 1).toString();
                                    farmunits = farmunits + 1;
                                    unitPrice = farmdatum.pricePerunit * farmunits;
                                    returnOnHarvest = double.parse(((farmdatum.roi/100) * unitPrice).toStringAsFixed(3));
                                    totalpayback = unitPrice + returnOnHarvest;
                                    totalprice = (unitPrice *0.02) + unitPrice;
                                  }
                                });
                                    },
                                    child: Container(
                                      width: 50,
                                      height: 40,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Colors.grey[200],
                                      ),
                                      child:  Center(
                                      child: Text('+', style: TextStyle(fontSize: isMobile? 20:28, color: Colors.black), textAlign: TextAlign.start,) ,
                                  )

                                    ),
                                  ),


                                  ],
                                  ),

                                )
                              ],
                            )


                          ],
                        ),

                        SizedBox(height: 16,),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.grey[200],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(Return_on_harvest +'('+farmdatum.roi.toString()+'%) - NGN ${formatCurrency.format(returnOnHarvest)}', style: TextStyle(color: Colors.grey[700], fontSize: isMobile ? 14: 22),),
                              SizedBox(height: isMobile ? 8: 16),
                              Divider(color: Colors.grey[700],height: 1,),
                              SizedBox(height: isMobile ? 8: 16),
                              Text(Total_payback_after +" "+farmdatum.farmCycle.toString()+' months', style: TextStyle(color: greenStart,fontSize: isMobile ? 14: 22 ),),

                              SizedBox(height: isMobile ? 8: 16),
                              Text('NGN ${formatCurrency.format(totalpayback)}', style: TextStyle(fontSize: isMobile ? 18:24, color: greenStart),)
                            ],
                          ),
                        )
                      ],
                    ),
                  )
              )



            ],
          ),
        ),
        bottomNavigationBar: Builder(builder: (context)=>  BottomNavigationBar(

              items: [
                new BottomNavigationBarItem(
                  icon: Container(width: 0, height: 0,),
                 title: Container(
                   height: 52,
                   margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
                   child: doFollowChecks(context),
                 )
                ),

                BottomNavigationBarItem(
                  icon: Container(width: 0, height: 0,),
                  title: farmdatum.statusId == 1 ? new SizedBox(
                      width: double.infinity,
                      height: 60,
                      child: RaisedButton.icon(onPressed: () {
                        intendToSponsorAddedToCart(observer, farmdatum.id, farmdatum.farmtype.farmtype, farmdatum.farmtypename, farmunits,double.parse( (farmunits * farmdatum.pricePerunit).toString()));
                          _doAddToCart(context);
                      }, icon: Container(width: 0, height: 0,), label: Text(Sponsor_as_a_button, style: TextStyle(fontSize: isMobile ? 20: 28),),color:greenStart,  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),)):

                      SizedBox(width: 0, height: 0,)

                )

              ]

        )
        )
    );
  }

  Widget doFollowChecks(BuildContext context) {
    if(isFarmFollowed == 0) {
      return  FlatButton.icon(onPressed: () {

      }, icon: Icon(Icons.cached), label: Text('Loading', style: TextStyle(fontSize: isMobile ?16: 24),));
    }
    else if(isFarmFollowed ==1) {
     return FlatButton.icon(onPressed: () {
       Scaffold.of(context).showSnackBar(SnackBar(
         content: Text('Processing, please wait...'), duration: Duration(seconds:2),));

       _startFollowingFarm(context);
      },
         color: Colors.grey[200],
         icon: Icon(Icons.add), label: Text(Follow_Offering, style: TextStyle(fontSize: isMobile ?16: 24),), shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)));
    }
    else {
      return FlatButton.icon( onPressed: () {


        showDialog(context: context, builder: (context) {
          return AlertDialog(
            title: Text(Unfollow_Offering),
            content: Text(Unfollow_Offering_desc),
            actions: <Widget>[
              FlatButton(child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
                onPressed: () =>Navigator.pop(context),),

              FlatButton( child: Text(
                "Yes", style: TextStyle(color: Colors.white, fontSize: 16),),
                onPressed: () {
                  Navigator.pop(context);
                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text('Processing, please wait...'), duration: Duration(seconds:2),));
                  _unFollowingFarm(context);
                },
                )

            ],
          );
        });

       /* Alert(
          context: context,
          title: Unfollow_Offering,
          desc: Unfollow_Offering_desc,
          buttons: [
            DialogButton(
              child: Text(
                "Cancel",
                style: TextStyle(color: Colors.white, fontSize: 16),
              ),
              onPressed: () =>Navigator.pop(context),
              width: 120,
            ),

            DialogButton(
              child: Text(
                "Yes", style: TextStyle(color: Colors.white, fontSize: 16),),
                onPressed: () {
                Navigator.pop(context);
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Text('Processing, please wait...'), duration: Duration(seconds:2),));
                _unFollowingFarm(context);
                },
              width: 120,
            ),
          ],
        ).show();*/
      }, icon: Icon(Icons.check, color: greenStart,), label: Text(Following_Offering, style: TextStyle(fontSize: 16, color: greenStart),),);
    }
  }

  _doAddToCart(BuildContext context) async {
    if (farmdatum.statusId == 1) {
      if(farmdatum.unitLeft > 0) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Preparing your order...'), duration: Duration(seconds:4),));
        SharedPreferences prefs = await SharedPreferences.getInstance();
        int userId = (prefs.getInt('userid') ?? 1);

        print('$userId');
        final response = await http.post(
          baseUrlFarm + '/api/cart_cv', body: {
          "uid": userId.toString(),
          "fid": farmdatum.id.toString(),
          "sponsorunit": '$farmunits'
        },
          headers: {
            "x-authorization": "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
          },
        );

        print('add carting response is: ' + response.statusCode.toString());
        if (response.statusCode == 200) {
          Navigator.push(context, MaterialPageRoute(builder: (context) => CartPage(observer)));
        }
        else {
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Something went wrong, please try again...'),
            duration: Duration(seconds: 4),));
        }
      }
      else {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(Offering_sold_out_text),
          duration: Duration(seconds: 4),));
      }


  }
  else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(Offering_not_available_for_sponsorship),
        duration: Duration(seconds: 4),));
    }
  }

  _startFollowingFarm(BuildContext context) async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = (prefs.getInt('userid') ?? 1);

    print('$userId');
    final response = await http.post(
      baseUrl+'/api/follow', body: {"uid": userId.toString(), "fid": farmdatum.id.toString()},
      headers: {
        "x-authorization": "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
      },
    );

    print('Follow response is: '+response.statusCode.toString());

    if(response.statusCode == 200) {
      followFarmIntention(observer);

      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(Offering_followed_successfully), duration: Duration(seconds:2),));
      setState(() {
        isFarmFollowed = 2;
        final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
        _firebaseMessaging.subscribeToTopic("sponsor_"+userId.toString());
      });

    }

    else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Something went wrong, please try again...'), duration: Duration(seconds:4),));
    }

  }

  _unFollowingFarm(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int userId = (prefs.getInt('userid') ?? 1);

    print('$userId');
    final response = await http.post(
      baseUrl+'/api/unfollow', body: {"uid": userId.toString(), "fid": farmdatum.id.toString()},
      headers: {
        "x-authorization": "lPjWNGBIbLRDSji2j9JcL4aGnTV4Cdp2x3yPTYKRndAfE9mcCf4aCWy0BfgDZXDD"
      },
    );

    print('Follow response is: '+response.statusCode.toString());
    if(response.statusCode == 200) {
      unFollowFarmIntention(observer);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(Offering_unfollowed), duration: Duration(seconds:2),));
      setState(() {
        isFarmFollowed = 1;
      });
    }
    else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Something went wrong, please try again...'), duration: Duration(seconds:4),));
    }
  }

}

