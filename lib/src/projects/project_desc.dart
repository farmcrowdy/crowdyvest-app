import '../colors.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter/material.dart';
import '../webpage.dart';
import '../engine/values.dart';

class FarmPoli extends StatelessWidget {
 final  String content;
  FarmPoli(this.content);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(centerTitle: false,
        leading: IconButton(onPressed: () {
          Navigator.pop(context);
        }, icon: Icon(Icons.keyboard_backspace), color: Colors.white,),
        backgroundColor: darkGreen,
        title: Text(Offering_Description, style: TextStyle(color: Colors.white),),
      ),


      body: SingleChildScrollView(child:Container(
        padding: EdgeInsets.all(16),
        child: Html(data: content, useRichText: true,
          defaultTextStyle: TextStyle( color: Colors.grey[700],height: 1.2, fontSize: 18, wordSpacing: 4, ),
          onLinkTap: (url) {
            Navigator.push(context, MaterialPageRoute(builder: (context)=>MyWebPage(url, "Learn more")
            ));
          },)
      ),
      ),
      backgroundColor: Colors.white,
    );
  }

}