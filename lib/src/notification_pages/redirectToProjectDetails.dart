import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:async_loader/async_loader.dart';
import '../models/farmshoplistpojo.dart';
import '../colors.dart';
import 'package:http/http.dart' as http;
import '../projects/projectInfoDetails.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

_getListOfEpisodes() async {

  final response = await http.get(
    baseUrlCrowdy+'/api/farmbystatus/1',
    headers: headerAuth,
  );
  print('dd: '+response.body);
  return response.body;

}
class RedirectToProjectDetails extends StatelessWidget with RouteAware{
final int notifyFarmId;
final FirebaseAnalyticsObserver observer;


RedirectToProjectDetails(this.notifyFarmId, this.observer);

_asyncLoaderEpisodes()=> new AsyncLoader(

      initState: () async => await _getListOfEpisodes(),
      renderLoad: () => new Center(child:  CircularProgressIndicator()),
      renderError: ([error]) =>
      new Text('Sorry, there was an error loading. Please check internet connection', style: TextStyle(color: Colors.black),),
      renderSuccess: ({data}) {
        try {
          var res = shoplistFromJson(data);
          if(res.data.length == 0) {
            return Center(child: Container(padding: EdgeInsets.all(16), child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text('Proejct Sponsorship Sold Out', style: TextStyle(color: Colors.grey, fontSize: 20, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                Text('Please check back later. You will also be notified of new sponsorship opportunities.', style:TextStyle(color: Colors.grey, fontSize: 18), textAlign: TextAlign.center,)
              ],
            )));
          }

          else {
            Datum myDatum;
            for(Datum datum in res.data) {
              if(datum.id == notifyFarmId) {
                myDatum = datum;
              // return  _buildDetails(myDatum);
              }
            }
            lyticsPushNotificationOpen(observer);
            return _buildDetails(myDatum);
          }
        }
        catch(e) {
          return Text('Project Sold out', style: TextStyle(color: greenStart, fontSize: 18), textAlign: TextAlign.center,);
        }
      }

  );

  Widget _buildDetails(Datum datum) {
  return FarmInfoPage(datum, observer);
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      body: _asyncLoaderEpisodes(),
    );
  }

}