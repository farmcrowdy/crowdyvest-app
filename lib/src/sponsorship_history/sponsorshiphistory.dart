import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../projects/portfolio.dart';
import '../cart/cart.dart';
import '../colors.dart';
import '../models/fullsponsorpojo.dart';
import 'sponsorfarm_details.dart';
import '../engine/values.dart';
import 'dart:async';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

class SponsorshipHistoryPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;
  SponsorshipHistoryPage(this.observer);

  @override
  SponsorshipHistoryPageState createState()=> new SponsorshipHistoryPageState(observer);

}


final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 0);
final formatCurrency2 = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 1);
final GlobalKey<AnimatedCircularChartState> _chartKey = new GlobalKey<AnimatedCircularChartState>();
bool isMobile = true;


int userId;
bool emptyHistory = false;
FullSponsorPojo fullSponsorPojo = new FullSponsorPojo();
bool loaded = false;
double totalExpectedReturns = 0; int  numOfFarmSponsored = 0; double collectedReturns = 0;
String nextEndOfCycle = '';
int farmType1id = 0, farmType1Unit = 0; String farmType1Name, type1Percent;
int farmType2id = 0, farmType2Unit = 0; String farmType2Name, type2Percent;
int farmType3id = 0, farmType3Unit = 0; String farmType3Name, type3Percent;
int farmType4id = 0, farmType4Unit = 0; String type4Percent;

bool dataisUpto2 = false;
bool dataisUpto3 = false;
bool dataisUpto4 = false;

List<CircularStackEntry> data = new List();

class SponsorshipHistoryPageState extends State<SponsorshipHistoryPage> with RouteAware {

  final FirebaseAnalyticsObserver observer;
  SponsorshipHistoryPageState(this.observer);



  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsSponsorshipHistory(observer);
  }





  loadDefaults() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    userId = (prefs.getInt('userid') ?? 0);
    _getListOfHistoryInfinity(userId.toString()).then((value) {
      double tempTotalExpected = 0; double tempTotalCollected = 0;

      fullSponsorPojo = fullSponsorPojoFromJson(value);
      for(FullSponsorDatum datum in fullSponsorPojo.data) {
        if(datum.repaymentStatus ==null) {
          int costPayed = datum.sponsorUnit * datum.farmDetails.pricePerunit;
          double presentExpectedReturns = costPayed + ( (datum.farmDetails.roi /100) * costPayed);
          tempTotalExpected = tempTotalExpected + presentExpectedReturns;

        }
        else {
          int costPayed = datum.sponsorUnit * datum.farmDetails.pricePerunit;
          double presentCollectedReturns = costPayed + ( (datum.farmDetails.roi /100) * costPayed);
          tempTotalCollected = tempTotalCollected + presentCollectedReturns;
        }
      }
      setState(() {
        fullSponsorPojo = fullSponsorPojoFromJson(value);

        totalExpectedReturns = tempTotalExpected;
        collectedReturns = tempTotalCollected;
        numOfFarmSponsored = fullSponsorPojo.data.length;
        nextEndOfCycle = fullSponsorPojo.data.first.endDate;
        var dsa= DateTime.parse(nextEndOfCycle).difference(new DateTime.now());
        //var dsa = DateTime.parse(sponsorById.data.first.endDate).difference(new DateTime.now());
        String finalDate = "";

        if (dsa.inDays == 1 && !dsa.isNegative) {
          finalDate = 'Yesterday';
        }

        else if (dsa.inDays == 0 && !dsa.isNegative) {
          finalDate = 'in ' + dsa.inHours.toString() + ' hours';

          if (dsa.inHours == 0 && !dsa.isNegative) {
            finalDate = 'in ' + dsa.inMinutes.toString() + ' mins';
          }
        }

        else if (!dsa.isNegative) {
          finalDate = 'in ' + dsa.inDays.toString() + ' days';
          if (dsa.inDays > 60) {
            finalDate = 'in ' + (dsa.inDays / 30).round().toString() + 'months';
          }
        }

        else {
          finalDate = 'has passed';
        }


        for(FullSponsorDatum datum in fullSponsorPojo.data) {
          //ALLOCATE ALL RESPONSE INTO 3 CATEGORIES
          if (farmType1id == 0) {
            farmType1id = datum.farmDetails.farmtypeId;
            farmType1Name = datum.farmDetails.farmtype.farmtype[0].toUpperCase()+datum.farmDetails.farmtype.farmtype.substring(1).toLowerCase();
          }
          else if (farmType2id == 0) {
            farmType2id = datum.farmDetails.farmtypeId;
            farmType2Name = datum.farmDetails.farmtype.farmtype[0].toUpperCase()+datum.farmDetails.farmtype.farmtype.substring(1).toLowerCase();
          }
          else if (farmType3id == 0) {
            farmType3id = datum.farmDetails.farmtypeId;
            farmType3Name = datum.farmDetails.farmtype.farmtype[0].toUpperCase()+datum.farmDetails.farmtype.farmtype.substring(1).toLowerCase();
          }

          //CUMMULATE ALL FARM UNITS TO A FARMTYPE
          if (datum.farmDetails.farmtypeId == farmType1id) {
            farmType1Unit = farmType1Unit + datum.sponsorUnit;
          }
          else if (datum.farmDetails.farmtypeId == farmType2id) {
            farmType2Unit = farmType2Unit + datum.sponsorUnit;
            dataisUpto2 = true;
          }
          else if (datum.farmDetails.farmtypeId == farmType3id) {
            farmType3Unit = farmType3Unit + datum.sponsorUnit;
            dataisUpto3 = true;
          }
          else {
            farmType4Unit = farmType4Unit + datum.sponsorUnit;
            dataisUpto4 = true;
          }
        }

        type1Percent = (farmType1Unit / (farmType1Unit + farmType2Unit + farmType3Unit + farmType4Unit) * 100).round().toString();
        type2Percent = (farmType2Unit / (farmType1Unit + farmType2Unit + farmType3Unit + farmType4Unit) * 100).round().toString();
        type3Percent = (farmType3Unit / (farmType1Unit + farmType2Unit + farmType3Unit + farmType4Unit) * 100).round().toString();
        type4Percent = (farmType4Unit / (farmType1Unit + farmType2Unit + farmType3Unit + farmType4Unit) * 100).round().toString();

        data = <CircularStackEntry>[
          new CircularStackEntry(
            <CircularSegmentEntry>[
              new CircularSegmentEntry(dataisUpto4? farmType4Unit.toDouble(): 0, Colors.green[100], rankKey: 'Portfolio 4'),
              new CircularSegmentEntry(dataisUpto3? farmType3Unit.toDouble(): 0, Colors.blue, rankKey: 'Portfolio 3'),
              new CircularSegmentEntry(dataisUpto2? farmType2Unit.toDouble(): 0, Colors.orange, rankKey: 'Portfolio 2'),
              new CircularSegmentEntry(farmType1Unit.toDouble(), darkGreen, rankKey: 'Portfolio 1'),
            ],
            rankKey: Your_sponsorships,
          ),
        ];



          loaded = true;
      });
    }).catchError((onError) {
      print("blablabla: :");
      setState(() {
        emptyHistory = true;
      });

    });



  }
  Future<String> _getListOfHistoryInfinity(String userId) async  {
    final response = await http.get(
      baseUrl+'/api/sponsor/'+userId,
      headers: headerAuth,
    );
    //print(response.body);
    return response.body;
  }




  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadDefaults();
  }

  @override
  Widget build(BuildContext context) {
    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    setState(() {
      if (!useMobileLayout) isMobile = false;
    });

    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
        Navigator.pop(context);
      }), title: Text(Sponsorship_History, style: TextStyle(color: Colors.white, fontSize: isMobile? 18: 24),), actions: <Widget>[
        Container(margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16), child: IconButton(icon: Icon(Icons.shopping_cart, color: greenStart,), onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=> CartPage(observer)));
        }),
        )

      ], backgroundColor: darkGreen, centerTitle: false, elevation: 0,),
      body: loaded ? ListView(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(color: darkGreen),
            child: Card(
              elevation: 5,
              margin: EdgeInsets.fromLTRB(12, 20, 12, 16),
              // padding:EdgeInsets.all(16)
              //child:
              child:Container(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.grey[50],
                            ),
                            padding: EdgeInsets.all(isMobile ? 12:16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(My_Expected_Returns, style: TextStyle(fontSize: isMobile ? 14: 18, color: Colors.grey,  height: 1.1 ),),
                                SizedBox(height: 1,),
                                Text('N${formatCurrency.format(totalExpectedReturns)}', style: TextStyle(fontSize: isMobile ?16:20, color: Colors.black, fontWeight: FontWeight.bold),),
                                SizedBox(height: 4,),
                              ],
                            ),
                          ),
                          ),
                          SizedBox(width: isMobile ? 12:16,),

                          Expanded(
                            child: Container(
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.grey[50],
                              ),
                              padding: EdgeInsets.all(isMobile ? 12:16),
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                  Text(Next_End_Of_Cycle_Date, style: TextStyle(fontSize: isMobile ? 14:18, color: Colors.grey,  height: 1.1 ),),
                                  Text('$nextEndOfCycle', style: TextStyle(fontSize: isMobile ? 16:20, color: Colors.black, fontWeight: FontWeight.bold),),
                                  SizedBox(height: 4,),
                                ],
                              )
                          ))
                        ],
                      ),

                      SizedBox(height: isMobile ? 12:16,),

                      Row(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[

                          Expanded(
                            child: Container(
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.grey[50],
                            ),
                            padding: EdgeInsets.all(isMobile ? 12:16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(Offering_Sponsored, style: TextStyle(fontSize: isMobile ? 14 : 18, color: Colors.grey,  height: 1.1 ),),

                                SizedBox(height: 1,),
                                Text('$numOfFarmSponsored', style: TextStyle(fontSize: isMobile ? 16: 20, color: Colors.black, fontWeight: FontWeight.bold),),
                                SizedBox(height: 4,),

                              ],
                            ),
                          ),),

                          SizedBox(width: isMobile ? 12:16,),
                          Expanded(
                            child: Container(
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.grey[50],
                              ),
                              padding: EdgeInsets.all(isMobile ? 12:16),
                              child:
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(My_Collected_Returns, style: TextStyle(fontSize: isMobile ? 14: 18, color: Colors.grey, height: 1.1 ),),
                                  Text('N${formatCurrency.format(collectedReturns)}', style: TextStyle(fontSize: isMobile ? 16:20, color: Colors.black, fontWeight: FontWeight.bold),),
                                  SizedBox(height: 4,),

                                ],
                              )
                          )),
                        ],
                      ),
                    ],

                  )
              )
          ),

      ),
          SizedBox(height: 16,),
          Container(
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
            child: Text(Sponsorship_History_By_Offerings, style: TextStyle(color: Colors.black, fontSize: isMobile ? 14: 18)),
          ),

          Container(
            decoration: BoxDecoration(color: Colors.grey[50]),
            margin: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(flex: 0 , child:
                AnimatedCircularChart(
                  key: _chartKey,
                  duration: Duration(milliseconds: 1000),
                  size: isMobile ? const Size(150.0, 150.0) : const Size(200.0, 200.0) ,
                  initialChartData: data,
                  chartType: CircularChartType.Pie,
                )),

                Expanded(flex: 1, child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    FlatButton.icon(onPressed: null, icon: Icon(Icons.brightness_1, color: darkGreen, size: 14,), label: Text('$farmType1Name - $farmType1Unit units ( $type1Percent%)', style: TextStyle(fontSize: isMobile ? 12:16, color: Colors.black),), ),
                   dataisUpto2 ? FlatButton.icon(onPressed: null, icon: Icon(Icons.brightness_1, color: Colors.orange,size: 14,), label: Text('$farmType2Name - $farmType2Unit units ( $type2Percent%)', style: TextStyle(fontSize: isMobile ? 12:16, color: Colors.black))): Container(width: 0, height: 0,),
                    dataisUpto3 ? FlatButton.icon(onPressed: null, icon: Icon(Icons.brightness_1, color: Colors.blue,size: 14,), label: Text('$farmType3Name - $farmType3Unit units ( $type3Percent%)', style: TextStyle(fontSize: isMobile ? 12:16, color: Colors.black))): Container(width: 0, height: 0,),
                    dataisUpto4 ? FlatButton.icon(onPressed: null, icon: Icon(Icons.brightness_1, color: Colors.green[100],size: 14,), label: Text('Other Farms - $farmType4Unit units ( $type4Percent%)', style: TextStyle(fontSize: isMobile ? 12:16, color: Colors.black))): Container(width: 0, height: 0,),
                  ],
                )),
              ],
            )
          ),

          _asyncLoaderHistory('1', userId.toString(), context),



        ],
      ) :

      Center(child: doInitials()),
    );
  }

  Widget doInitials() {
   return emptyHistory ?  Center(child: Column(
   crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
    Text(Youve_not_sponsored_an_Offering_yet, style: TextStyle(fontSize: 20, color: Colors.black),),
    SizedBox(height: 8,),
    RaisedButton.icon(onPressed: () {
    Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmShopPageState(observer)));
    }, icon: Icon(Icons.shopping_basket, color: Colors.white,), label: Text(Sponsor_available_Offerings, style: TextStyle(color: Colors.white, fontSize: 18),), color: greenStart, )
    ],
    ))
       :

   Text('Loading...', style: TextStyle(fontSize: 22, color: Colors.grey));

  }
}



_getListOfHistory(String offset, userId) async {
  final response = await http.get(
    baseUrl+'/api/sponsor/'+userId,
    headers: headerAuth,
  );
  print('sponsor response is: '+response.body.toString());
  return response.body;
  //return responseBody;

}

_asyncLoaderHistory(String au, userId, BuildContext context)=> new AsyncLoader(

    initState: () async => await _getListOfHistory(au, userId),
    renderLoad: () =>new LinearProgressIndicator(),
    renderError: ([error]) =>
    new Text('Sorry, there was an error loading. Please check internet connection', style: TextStyle(color: Colors.black),),
    renderSuccess: ({data}) => new BuildEpisodeView(data, context)

);


Widget _buildHistory(FullSponsorPojo spons, BuildContext context) {
  return new ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: spons.data.length,

      itemBuilder: (BuildContext _context, int i) {

        return _buildRow(spons.data[i], context);
      }
  );
}


Widget _buildRow(FullSponsorDatum datum, BuildContext context) {
  //print('we are now');
  return
    GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context)=> SponsorFarmDetailsPage(datum)));
  },
    child:Container(
      margin: EdgeInsets.fromLTRB(8, 4, 8, 4),
      padding: EdgeInsets.all(16),
      decoration: new BoxDecoration(
        shape: BoxShape.rectangle,
        color:  Colors.grey[50],
        borderRadius: BorderRadius.circular(10),
      ),
      //decoration: BoxDecoration(color: Colors.white),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(datum.farmDetails.farmtype.farmtype[0].toUpperCase()+datum.farmDetails.farmtype.farmtype.substring(1).toLowerCase(), style: TextStyle(fontSize: isMobile ? 18 : 22, fontWeight: FontWeight.bold),),
          SizedBox(height: 1,),
          Text(datum.farmDetails.slug[0].toUpperCase()+datum.farmDetails.slug.substring(1).toLowerCase(), style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: isMobile ? 14:18), ),
          SizedBox(height: 8,),
          Row(
            children:
            <Widget>[

            Expanded(child:

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
              Text(Offering_Sponsored, style: TextStyle(color: Colors.grey, fontSize: isMobile ? 13 : 17, height: 1.15),),
              SizedBox(height: 4,),
                Text(datum.sponsorUnit.toString(), style: TextStyle(color:Colors.black, fontSize: isMobile ? 16:20, fontWeight: FontWeight.bold),)
              ],
            )),

            Container(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
              color: Colors.grey[400],
              width: 1,
              height: 50,
            ),

            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(Sponsorship_Amount_NGN, style: TextStyle(color: Colors.grey, fontSize: isMobile ? 13:17, height: 1.15),),
                SizedBox(height: 4,),
                Text(formatCurrency.format((datum.sponsorUnit * datum.farmDetails.pricePerunit)), style: TextStyle(color:Colors.black, fontSize: isMobile ? 16: 20, fontWeight: FontWeight.bold),)
              ],
            )),
            Container(
              margin: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
              color: Colors.grey[400],
              width: 1,
              height: 50,
            ),

            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(Expected_Earning_NGN, style: TextStyle(color: Colors.grey, fontSize: isMobile ? 13:17, height: 1.15),),
                SizedBox(height: 4,),
                Text(formatCurrency2.format((datum.sponsorUnit * datum.farmDetails.pricePerunit) + ((datum.sponsorUnit * datum.farmDetails.pricePerunit) * (datum.farmDetails.roi/100))), style: TextStyle(color:Colors.black, fontSize: isMobile ? 16:20, fontWeight: FontWeight.bold),)
              ],
            )),

          ],)
        ],
      )
    ));
                   // ... to here.

    }

class BuildEpisodeViewState extends State<BuildEpisodeView> {
  String _response; BuildContext context;

  BuildEpisodeViewState(this._response, this.context);

  @override
  Widget build(BuildContext context) {
    var _listOfEpis = fullSponsorPojoFromJson(_response);

    return  _buildHistory(_listOfEpis, context);



  }
}
class BuildEpisodeView extends StatefulWidget {
  final String _response; final BuildContext context;
  BuildEpisodeView(this._response, this.context);

  @override
  BuildEpisodeViewState createState()=> new BuildEpisodeViewState(_response, context);

}


