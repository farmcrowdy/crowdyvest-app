import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';

import '../colors.dart';
import '../models/fullsponsorpojo.dart';
import '../engine/values.dart';

class SponsorFarmDetailsPage extends StatefulWidget {
  final FullSponsorDatum updateData;
  SponsorFarmDetailsPage(this.updateData);

  @override
  SponsorFarmDetailsPageState createState() => new  SponsorFarmDetailsPageState(updateData);

}
final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 2);
bool isMobile = true;

class SponsorFarmDetailsPageState extends State<SponsorFarmDetailsPage> {

  FullSponsorDatum updateData;
  SponsorFarmDetailsPageState(this.updateData);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;
    if (!useMobileLayout) {
      setState(() {
        isMobile = false;
      });
    }



    int totalAmountPaid =  updateData.sponsorUnit * updateData.farmDetails.pricePerunit;
    double returnsEarned = totalAmountPaid + (totalAmountPaid * (updateData.farmDetails.roi/100));
    bool repayed = true;
    if(updateData.repaymentStatus ==null) {
      repayed = false;
      print('payment status is null');
    }
      return Scaffold(

          appBar: AppBar(centerTitle:false, leading: IconButton(onPressed: () {
            Navigator.pop(context);
          }, icon: Icon(Icons.keyboard_backspace), color: Colors.white, ),
            backgroundColor: darkGreen, title: Text(Sponsored_Offering_details, style: TextStyle(color: Colors.white, fontSize: isMobile ?18: 22),),
          ),


          body: SingleChildScrollView(
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Container(
                  height: isMobile ? 200 : 300,
                  margin: EdgeInsets.fromLTRB(16, 16, 16, 16),
                  decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color:  Colors.grey,
                      borderRadius: BorderRadius.circular(10),
                      image: new DecorationImage(
                          fit: BoxFit.cover,
                          image: new NetworkImage(baseUrl+"/storage/farmimages/"+updateData.farmDetails.farmImage)
                      )
                  ),
              //child: Image.network(baseUrl+'storage/farmimages/'+updateData.farmDetails.),
              ),

              Container(
                  padding: EdgeInsets.all(isMobile ? 16 : 28),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children:
                    <Widget>[


                      Text(updateData.farmDetails.farmtype.farmtype[0].toUpperCase()+updateData.farmDetails.farmtype.farmtype.substring(1), style: TextStyle(color:Colors.black, fontSize: isMobile ? 22:36, fontWeight: FontWeight.bold),),
                      Text(updateData.farmDetails.slug, style: TextStyle(color: Colors.grey[700], fontSize: isMobile ?18:24),),
                      SizedBox(height: 16,),
                      Text('N${formatCurrency.format(updateData.farmDetails.pricePerunit)}/unit', style: TextStyle(color: Colors.grey[700], fontSize: isMobile ?16:22),),
                      SizedBox(height: 3,),
                      Text(updateData.farmDetails.roi.toString()+'% '+returns_on_sponsorship, style: TextStyle(color: Colors.grey[700], fontSize: isMobile ?16:22),),
                      SizedBox(height: 3,),
                      Text(You_sponsored+' '+updateData.sponsorUnit.toString()+' '+ units_of_this_Offering, style: TextStyle(color: greenStart, fontSize: isMobile ?16:22),),
                      SizedBox(height: 10,),
                      Divider(color: Colors.grey[300],),


                      Card(
                          elevation: 5,
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          // padding:EdgeInsets.all(16)
                          //child:
                          child:Container(
                              padding: EdgeInsets.all(isMobile ? 12:16),
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Expanded(
                                        child: Container(
                                            decoration: new BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              borderRadius: BorderRadius.circular(5),
                                              color: Colors.grey[50],
                                            ),
                                            padding: EdgeInsets.all(isMobile ? 12:16),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(Sponsorship_Amount, style: TextStyle(color: Colors.grey, fontSize: isMobile ?13:17, height: 0.8),),
                                                SizedBox(height: 8,),
                                                Text('N${formatCurrency.format(totalAmountPaid)}', style: TextStyle(color:Colors.black, fontSize: isMobile? 22: 28),),
                                              ],
                                            )
                                        ),),
                                      SizedBox(width: isMobile ? 12:16,),

                                      Expanded(

                                          child: Container(
                                              decoration: new BoxDecoration(
                                                shape: BoxShape.rectangle,
                                                borderRadius: BorderRadius.circular(5),
                                                color: Colors.grey[50],
                                              ),
                                              padding: EdgeInsets.all(isMobile ? 12:16),
                                              child:
                                              Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Text(Expected_Earning, style: TextStyle(color: Colors.grey, fontSize: isMobile ? 13:17, height: 0.8),),
                                                  SizedBox(height: 8,),
                                                  Text('N${formatCurrency.format(returnsEarned)}', style: TextStyle(color:Colors.black, fontSize: isMobile ? 22:28),)
                                                ],
                                              )

                                          )),
                                    ],
                                  ),

                                  SizedBox(height: 8,),
                                  repayed ?  FlatButton.icon(onPressed: null, icon:Icon(Icons.check_circle, color: greenStart,), label:Text(Sponsorhip_returns_payed, style: TextStyle(color: greenStart, fontSize: isMobile ? 17:24),)): Text(Offering_cycle_is_still_running, style: TextStyle(color: Colors.black, fontSize: isMobile ? 18:24), textAlign: TextAlign.center,)
                                ],
                              )
                          )
                      ),

                    ],)),

              Container(
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  width: double.infinity,
                  height: isMobile ? 48: 62,

                  child: new RaisedButton.icon(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)), onPressed: ()=>Share.share('Check out our website https://crowdyvest.com'),
                    icon: Icon(Icons.share, color: Colors.black),
                    label: Text('Recommend Crowdyvest to a friend', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: isMobile ? 14:24),),
                    color: greenStart,
                    elevation: 0,)

              )
            ],
          ))
      );
    }
  }

