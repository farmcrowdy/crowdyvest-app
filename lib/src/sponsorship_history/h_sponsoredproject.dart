import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../colors.dart';
import '../projects/portfolio.dart';
import '../models/fullsponsorpojo.dart';
import 'sponsorfarm_details.dart';
import '../engine/values.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

class SponsoredFarmsPage extends StatefulWidget {

  final FirebaseAnalyticsObserver observer;
  SponsoredFarmsPage(this.observer);

  @override
  SponsoredFarmPageState createState()=> new SponsoredFarmPageState(observer);

}

class SponsoredFarmPageState extends State<SponsoredFarmsPage> with RouteAware {

  final FirebaseAnalyticsObserver observer;
  SponsoredFarmPageState(this.observer);

  int userId;
  bool updated = false;

  loadDefaults() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      userId = (prefs.getInt('userid') ?? 0);
      updated = true;
    });
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsSponsoredProjects(observer);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadDefaults();
  }


  @override
  Widget build(BuildContext context) {

      // TODO: implement build
    return Scaffold(
      appBar: AppBar(backgroundColor: darkGreen,
        leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
          Navigator.pop(context);
        }),
        title: Text("Your Sponsorhips", style: TextStyle(color: Colors.white),), centerTitle: false,),

      body: Center(
        child: updated ? _asyncLoaderEpisodes(userId.toString()) : Text('Loading...'),
      )
    );
  }

  _getListOfEpisodes(String sponsorId) async  {
    final response = await http.get(
      baseUrl+'/api/sponsor/'+sponsorId,
      headers: headerAuth,
    );

    return response.body;
  }

  _asyncLoaderEpisodes(String sponsorId)=> new AsyncLoader(

      initState: () async => await _getListOfEpisodes(sponsorId),
      renderLoad: () =>new CircularProgressIndicator(),
      renderError: ([error]) =>
      new Text('Sorry, there was an error loading. Please check internet connection', style: TextStyle(color: Colors.black),),
      renderSuccess: ({data}) {
        try{
          return _buildEpisodes(fullSponsorPojoFromJson(data));
        }
        catch(e) {
          return Center(child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text(Youve_not_sponsored_an_Offering_yet, style: TextStyle(fontSize: 20, color: Colors.black),),
            SizedBox(height: 8,),
            RaisedButton.icon(onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmShopPageState(observer)));
            }, icon: Icon(Icons.shopping_basket, color: Colors.white,), label: Text(Sponsor_available_Offerings, style: TextStyle(color: Colors.white, fontSize: 18),), color: greenStart, )
            ],
          ));
        }
      }

  );


  Widget _buildEpisodes(FullSponsorPojo listOfEpisodes) {
    return new ListView.separated(
        itemCount: listOfEpisodes.data.length,
        separatorBuilder: (BuildContext context, int index) {
          return Container(
              margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              child: new Divider(color: Colors.grey[300], )
          );
        },
        itemBuilder: (BuildContext _context, int i) {

          var episodeInfo = listOfEpisodes.data[i];

          var dsa= new DateTime.now().difference(DateTime.parse(episodeInfo.createdAt));
          String finalDate = "";
          if(dsa.inDays ==1) {
            finalDate = 'Yesterday';
          }
          else if(dsa.inDays ==0) {
            finalDate = dsa.inHours.toString()+ ' hours ago';

            if(dsa.inHours ==0) {
              finalDate = dsa.inMinutes.toString()+' mins ago';
            }
          }

          else {
            finalDate = dsa.inDays.toString()+' days ago';
          }

          return _buildRow(episodeInfo, finalDate, _context);
        }
    );
  }


}


Widget _buildRow(FullSponsorDatum updateData, String date, context,) {
  //print('we are now');
  var shortestSide = MediaQuery
      .of(context)
      .size
      .shortestSide;
  var useMobileLayout = shortestSide < 600;

  if (useMobileLayout) {
    return new ListTile(

          onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>SponsorFarmDetailsPage(updateData))),
          contentPadding: EdgeInsets.symmetric(horizontal: 18, vertical: 11),

          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 14,),
              Text(updateData.farmDetails.farmtype.farmtype[0].toUpperCase()+updateData.farmDetails.farmtype.farmtype.substring(1), style: TextStyle(color: greenStart, fontSize: 18, ), ),
              SizedBox(height: 3,),
              Text('N'+updateData.farmDetails.pricePerunit.toString()+'/unit', style: TextStyle(fontSize: 16, color: Colors.grey),),
              SizedBox(height: 3,),
              Text(updateData.farmDetails.slug, style: TextStyle(color: Colors.grey, fontSize: 16, ),),
              SizedBox(height: 3,),
              Text(date, style: TextStyle(fontSize: 14, color: Colors.grey),),
              SizedBox(height: 8,),
              Text(updateData.sponsorUnit.toString()+' '+units_sponsored, style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),),


            ],
          ),

          leading:   new Container(

              width: 105.0,
              height: 105.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  color:  Colors.grey,
                  borderRadius: new BorderRadius.circular(10),
                  image: new DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(baseUrl+"/storage/farmimages/"+updateData.farmDetails.farmImage)
                  )
              )),
          //leading: ,
    );
  }

  else {
    return new GestureDetector(onTap: () {
      Navigator.push(context, MaterialPageRoute(builder: (context)=>SponsorFarmDetailsPage(updateData)));
    },
        child:  new ListTile(

          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 4,),
              Text(updateData.farmDetails.farmtype.farmtype[0].toUpperCase()+updateData.farmDetails.farmtype.farmtype.substring(1), style: TextStyle(color: greenStart, fontSize: 22, ), ),
              Text(updateData.farmDetails.slug, style: TextStyle(color: Colors.black, fontSize: 20, ),),
              Text('N'+updateData.farmDetails.pricePerunit.toString()+'/unit', style: TextStyle(fontSize: 20, color: Colors.grey),),
              SizedBox(height: 8,),
              Text(updateData.sponsorUnit.toString()+' '+units_sponsored, style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),),
              Text(date, style: TextStyle(fontSize: 18, color: Colors.grey),),
              SizedBox(height: 8,),


            ],
          ),

          leading:   new Container(

              width: 80.0,
              height: 80.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  color:  Colors.grey,
                  borderRadius: new BorderRadius.circular(10),
                  image: new DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(baseUrl+"/storage/farmimages/"+updateData.farmDetails.farmImage)
                  )
              )),
          //leading: ,

        )
    );
  }
                  // ... to here.

}