import 'package:flutter/material.dart';
import 'dart:math';
import '../colors.dart';
import '../onboard/login.dart';
import '../engine/values.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/actions_lytics.dart';

final String bg1 = 'assets/layer_bg.png';
final String bg2 = 'assets/layer_bg.png';
final String bg3 = 'assets/layer_bg.png';
final String bgDisplay1 = 'assets/layer1.png';
final String bgDisplay2 = 'assets/layer2.png';
final String bgDisplay3 = 'assets/layer3.png';



bool myMobile;
class IntroPageDesign extends StatelessWidget {
  final bool checkIfMobile;
  final FirebaseAnalyticsObserver observer;


  IntroPageDesign(this.checkIfMobile, this.observer) {
    myMobile = this.checkIfMobile;
  }



  @override
  Widget build(BuildContext context) {


    return new MaterialApp(
      title: appName,
      home: new MyIntroPage(this.checkIfMobile , observer),
      debugShowCheckedModeBanner: false,
    );
  }
}


String buttonText = "Continue";

/// An indicator showing the currently selected page of a PageController
class DotsIndicator extends AnimatedWidget {
  DotsIndicator({
    this.controller,
    this.itemCount,
    this.onPageSelected,
    this.color: Colors.grey,
  }) : super(listenable: controller);

  /// The PageController that this DotsIndicator is representing.
  final PageController controller;

  /// The number of items managed by the PageController
  final int itemCount;

  /// Called when a dot is tapped
  final ValueChanged<int> onPageSelected;

  /// The color of the dots.
  ///
  /// Defaults to `Colors.white`.
  final Color color;

  // The base size of the dots
  static const double _kDotSize = 8.0;

  // The increase in the size of the selected dot
  static const double _kMaxZoom = 2.0;

  // The distance between the center of each dot
  static const double _kDotSpacing = 25.0;

  Widget _buildDot(int index) {


    double selectedness = Curves.easeOut.transform(
      max(
        0.0,
        1.0 - ((controller.page ?? controller.initialPage) - index).abs(),
      ),
    );
    double zoom = 1.0 + (_kMaxZoom - 1.0) * selectedness;
    return new Container(
      width: _kDotSpacing,
      child: new Center(
        child: new Material(
          color: color,
          type: MaterialType.circle,
          child: new Container(
            width: _kDotSize * zoom,
            height: _kDotSize * zoom,
            child: new InkWell(
              onTap: () => onPageSelected(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: new List<Widget>.generate(itemCount, _buildDot),
    );
  }
}

int myNextPage =1;


class MyIntroPage extends StatefulWidget {
  final bool checkIfMobile;
  final FirebaseAnalyticsObserver observer;

  MyIntroPage(this.checkIfMobile, this.observer);

  @override
  State createState() => new MyIntroState(checkIfMobile, observer);
}




bool isMobile = true;
class MyIntroState extends State<MyIntroPage> with RouteAware{
  bool _locked;
  final FirebaseAnalyticsObserver observer;
  var _controller;
  final  bool checkMobile;



  MyIntroState(this.checkMobile, this.observer);

  void scrollListen() {
    double _currentOffset = _controller.offset;
    double _width = MediaQuery.of(context).size.width;
    if (_currentOffset > 2 * _width) {
      setState(() {
        _locked = true;
      });
    }
    else {
      setState(() {
        _locked = false;
      });
    }
  }

  void onPageChanged(int index) {
    setState(() {
      if(index == 0)  {
        buttonText = "Continue";
        myNextPage = 1;
      }
      else if(index == 1) {
        buttonText = "Continue";
        myNextPage = 2;
      }
      else if(index ==2) buttonText = "Get Started!";
    });

  }





  static const _kDuration = const Duration(milliseconds: 300);

  static const _kCurve = Curves.ease;

  final _kArrowColor = Colors.black.withOpacity(0.8);

  final List<Widget> _pages = <Widget>[
    new Stack(
      children: <Widget>[
        new Image.asset(
            bg1,
        ),

            Positioned(
              bottom: 60,
              right: 0,
              left: 0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                 Container(
                   margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                   child:  Image.asset(
                       bgDisplay1,
                       width: myMobile ? 210 : 320,
                       height: myMobile ? 210 : 320,
                   )),

                 Container(
                   padding: EdgeInsets.fromLTRB(32, 0, 32, 110),
                   child: Column(
                     crossAxisAlignment: CrossAxisAlignment.start,
                     mainAxisAlignment: MainAxisAlignment.start,
                     children: <Widget>[
                       Text(introTitle1, style: TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.bold),),

                       SizedBox(height: 16,),
                       Text(introContent1, style: TextStyle(height: 1.2, fontSize: 18, color: Colors.grey[700],), textAlign: TextAlign.start,),



                     ],
                   )
                 )

                ],
              ),
            )
      ],
    ),

    new Stack(
      children: <Widget>[
        new Image.asset(
            bg2,),

        Positioned(
          bottom: 60,
          right: 0,
          left: 0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(16, 16, 16, 0),

                  child:  new Image.asset(
                      bgDisplay2,
                      width: myMobile ? 210: 320,
                      height:  myMobile ? 210 : 320,
                  )),

              Container(
                  padding: EdgeInsets.fromLTRB(32, 0, 32, 110),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(introTitle2, style: TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.bold),),

                      SizedBox(height: 16,),
                      Text(introContent2, style: TextStyle(height: 1.2, fontSize: 18, color: Colors.grey[700],), textAlign: TextAlign.start,),

                    ],
                  )
              )

            ],
          ),
        )
      ],
    ),





    new Stack(
      children: <Widget>[
        new Image.asset(bg3,),

        Positioned(
          bottom: 60,
          right: 0,
          left: 0,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                  child:  new Image.asset(
                      bgDisplay3,
                      width: myMobile ? 210 :320,
                      height: myMobile ? 210 :320,
                  )),

              Container(
                  padding: EdgeInsets.fromLTRB(32, 0, 32, 110),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(introTitle3, style: TextStyle(color: Colors.black, fontSize: 24, fontWeight: FontWeight.bold),),

                      SizedBox(height: 16,),
                      Text(introContent3, style: TextStyle(height: 1.2, fontSize: 18, color: Colors.grey[700],), textAlign: TextAlign.start,),
                    ],
                  )
              )

            ],
          ),
        )
      ],
    ),

  ];


  @override
  void initState() {
    // TODO: implement initState
    _controller = PageController();
    _locked = false;
    super.initState();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    viewingOnboardingScreens(observer);
  }


  @override
  Widget build(BuildContext context) {

    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;


    return new Scaffold(
      body: new IconTheme(
        data: new IconThemeData(color: _kArrowColor),
        child: new Stack(
          children: <Widget>[


            new PageView.builder(
              physics: _locked ? const NeverScrollableScrollPhysics() : AlwaysScrollableScrollPhysics(),
              onPageChanged: onPageChanged,
              controller: _controller,
              itemCount: 3,

              itemBuilder: (BuildContext context, int index) {
                return _pages[index % _pages.length];
              },
            ),

            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 50, right: 16),
                  child: GestureDetector(
                    onTap: ()=>  Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginPage(observer))),
                    child: Text("Skip", textAlign: TextAlign.end, style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold), )),
                )
              ],
            ),

            new Positioned(
              bottom: 10.0,
              left: 0.0,
              right: 0.0,
              child: new Container(
                padding: const EdgeInsets.all(20.0),
                child: new Center(
                  child: new DotsIndicator(
                    controller: _controller,
                    itemCount: _pages.length,
                    onPageSelected: (int page) {
                    print("PAGE AT "+page.toString());
                    _controller.animateToPage(
                        page,
                        duration: _kDuration,
                        curve: _kCurve,
                      );
                    },
                  ),
                ),
              ),
            ),


            new Positioned(
              bottom: 50.0,
              left: 0.0,
              right: 0.0,
              child: new Container(
                padding: const EdgeInsets.fromLTRB(20, 0,20, 42),
                child: new Center(
                  child:      new SizedBox(
                      width: double.infinity,
                      height: 48,

                      child: new RaisedButton.icon(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                        onPressed: () {
                          if(buttonText.contains("Continue")) {
                            _controller.animateToPage(
                              myNextPage,
                              duration: _kDuration,
                              curve: _kCurve,
                            );
                          }
                          else {
                            completedOnboardingScreens(observer);

                            Navigator.pushReplacement(context,
                                MaterialPageRoute(
                                    builder: (context) => LoginPage(observer)));
                          }

                        },
                        icon: Container(width: 0, height: 0,),
                        label: Text(buttonText,
                          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold ),),
                        color: greenStart,
                        elevation: 7,)

                  )
                ),
              ),
            ),


          ],
        ),
      ),
    );
  }
}