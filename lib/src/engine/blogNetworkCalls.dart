import 'dart:async';

import '../models/bloggerpojo.dart';
import 'package:http/http.dart';
import 'dart:convert';

const  String baseURL = "https://blog.farmcrowdy.com/wp-json/wp/v2/posts?";
const String per_page = "10";
const String orderBy = "date";
const String order = "desc";

Future<List<BlogFeedPojo>> getFeedItems(int offset) async {

  var page = offset.toString();

  String joinedQuery = "page=$page&per_page=$per_page&orderby=$orderBy&order=$order";
  String totalUrl = baseURL+joinedQuery;
  Response r = await get(totalUrl);
  return blogFeedPojoFromJson(r.body);
}