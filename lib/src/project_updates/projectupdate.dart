import 'dart:async';

import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import '../cart/cart.dart';
import '../colors.dart';
import 'projectupdate_details.dart';
import '../models/farmupdatepojo.dart';
import '../engine/values.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

class FarmUpdatePage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  FarmUpdatePage(this.observer);

  @override
  FarmUpdagePageState createState()=> new FarmUpdagePageState(observer);

}

String sponsorId;
bool updated = false;

class FarmUpdagePageState extends State<FarmUpdatePage> with RouteAware {


  final FirebaseAnalyticsObserver observer;
  FarmUpdagePageState(this.observer);

  _abm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      sponsorId = (prefs.getInt('userid') ?? 0).toString();
      updated = true;
    });
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsProjectUpdates(observer);
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _abm();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
        Navigator.pop(context);
      }),
        title: Text("Project Updates", style: TextStyle(color: Colors.white),),
      actions: <Widget>[
        Container(margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
        child: IconButton(icon: Icon(Icons.shopping_cart, color: Colors.white,), onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context)=> CartPage(observer)));
        }),)
      ], centerTitle: false, backgroundColor: darkGreen,),

      body: Center(
        child: updated ?  _asyncLoaderProjectUpdates(sponsorId, "0"): Text("Loading...") ,
      ),
    );
  }

  _getListOfProjectUpdates(String sponsorId, String offset) async {

    final response = await http.get(
      baseUrl+'/api/farmupdates/'+sponsorId+'/20/'+offset,
      headers: headerAuth,
    );
    print('dd: '+response.body);
    return response.body;

  }

  Future<String> _getListOfProjectUpdatesInfinity(String sponsorId, String offset) async  {
    final response = await http.get(
      baseUrl+'/api/farmupdates/'+sponsorId+'/10/'+offset,
      headers: headerAuth,
    );

    return response.body;
  }

  _asyncLoaderProjectUpdates(String sponsorId, String offset)=> new AsyncLoader (

      initState: () async => await _getListOfProjectUpdates(sponsorId, offset),
      renderLoad: () =>new CircularProgressIndicator(),
      renderError: ([error]) =>
      new Text('Sorry, there was an error loading. Please check internet connection', style: TextStyle(color: Colors.black),),
      renderSuccess: ({data}) {
        try{

          return _buildProjectUpdates(farmUpdatePojoFromJson(data), sponsorId);
        }
        catch(e) {
          return Text(No_Offering_update_available_yet);
        }
      }

  );


  Widget _buildProjectUpdates(FarmUpdatePojo listOfProjectUpdates, String sponsorId) {
    List<UpdateDatum> daterer = new List();
    daterer.addAll(listOfProjectUpdates.data);
    return new ListView.separated(
        itemCount: daterer.length,
        separatorBuilder: (BuildContext context, int index) {
          return Container(
              margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              child: new Divider(color: Colors.grey[300], )
          );
        },
        itemBuilder: (BuildContext _context, int i) {
          var episodeInfo = daterer[i];

          var dsa= new DateTime.now().difference(DateTime.parse(episodeInfo.createdAt));
          String finalDate = "";
          if(dsa.inDays ==1) {
            finalDate = 'Yesterday';
          }
          else if(dsa.inDays ==0) {
            finalDate = dsa.inHours.toString()+ ' hours ago';

            if(dsa.inHours ==0) {
              finalDate = dsa.inMinutes.toString()+' mins ago';
            }
          }

          else {
            finalDate = dsa.inDays.toString()+' days ago';
          }

          return _buildRow(episodeInfo, finalDate, _context);
        }
    );
  }


}


Widget _buildRow(UpdateDatum updateData, String date, context,) {
  //print('we are now');
  var shortestSide = MediaQuery
      .of(context)
      .size
      .shortestSide;
  var useMobileLayout = shortestSide < 600;

  if (useMobileLayout) {
    return  new ListTile(
      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>FarmUpdateDetailsPage(updateData.update[0].id.toString(), updateData.farmDetails))),

      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 14,),
          Text(updateData.farmDetails.farmtype.farmtype[0].toUpperCase()+updateData.farmDetails.farmtype.farmtype.substring(1), style: TextStyle(color: Colors.black, fontSize: 16, ), ),
          SizedBox(height: 4,),
          Text(updateData.farmDetails.slug, style: TextStyle(color: Colors.grey, fontSize: 14, ),),
          SizedBox(height: 4,),
          Text("Update was made "+date, style: TextStyle(fontSize: 14, color: Colors.grey),),
          SizedBox(height: 8,),
          Text(updateData.update[0].title, style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold ),),
          SizedBox(height: 8,),
          Text('Read Details...',style: TextStyle(fontSize: 14, color: Colors.grey))
        ],
      ),

      leading:   new Container(
          width: 105.0,
          height: 105.0,
          decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color:  Colors.grey,
              borderRadius: new BorderRadius.circular(10),
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(baseUrl +"/storage/farmimages/"+updateData.farmDetails.farmImage)
              )
          )),
      //leading: ,


    );
  }

  else {
    return new ListTile(
      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>FarmUpdateDetailsPage(updateData.update[0].id.toString(), updateData.farmDetails))),

      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 14,),
          Text(updateData.farmDetails.farmtype.farmtype[0].toUpperCase()+updateData.farmDetails.farmtype.farmtype.substring(1), style: TextStyle(color: Colors.black, fontSize: 20, ), ),
          SizedBox(height: 4,),
          Text(updateData.farmDetails.slug, style: TextStyle(color: Colors.grey, fontSize: 18, ),),
          SizedBox(height: 4,),
          Text("Update was made "+date, style: TextStyle(fontSize: 18, color: Colors.grey),),
          SizedBox(height: 8,),
          Text(updateData.update[0].title, style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold ),),
          SizedBox(height: 8,),
          Text('Read Details...',style: TextStyle(fontSize: 18, color: Colors.grey))
        ],
      ),

      leading:   new Container(

          width: 105.0,
          height: 105.0,
          decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color:  Colors.grey,
              borderRadius: new BorderRadius.circular(10),
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(baseUrl +"/storage/farmimages/"+updateData.farmDetails.farmImage)
              )
          )),
      //leading: ,


    );
  }


                   // ... to here.

}


