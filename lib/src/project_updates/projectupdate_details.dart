import 'dart:async';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:share/share.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:http/http.dart' as http;
import 'package:percent_indicator/percent_indicator.dart';
import '../webpage.dart';
import '../colors.dart';
import '../models/farmupdate_detailspojo.dart';
import '../models/farmupdatepojo.dart';
import '../engine/values.dart';


class FarmUpdateDetailsPage extends StatefulWidget {
  String farmid, formattedDate;
  FarmDetails farmDetails;
  FarmUpdateDetailsPage(this.farmid, this.farmDetails);

  @override
  FarmUpdateDetailsPageState createState()=> new FarmUpdateDetailsPageState(farmid, farmDetails);

}

class FarmUpdateDetailsPageState extends State<FarmUpdateDetailsPage> {
  String updateid, formattedDate;
  FarmDetails farmDetails;
  FarmUpdateDetailsPageState(this.updateid, this.farmDetails);
  String farmName, farmSlug,roi,cycle, message;
  int totalLength = 0, clickedPosition = 0, currentPosition = 0;
  FarmUpdateDetailsPojo farmUpdateDetailsPojo = new FarmUpdateDetailsPojo();
  bool loaded = false;
  bool update2 = false;
  bool update3 = false;
  bool update4 = false;
  bool update5 = false;
  bool update6 = false;

  bool update7 = false;
  bool update8 = false;
  bool update9 = false;
  bool update10 = false;
  bool update11 = false;
  bool update12 = false;

  int percentCompleteStage1 = 0;
  int percentCompleteStage2 = 0;


 Future<String> _getListOfEpisodesInfinity() async  {
    final response = await http.get(
      baseUrl+'/api/userupdateingroup/'+updateid,
      headers: headerAuth,
    );
    return response.body;
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getListOfEpisodesInfinity().then((value) {
      farmUpdateDetailsPojo = farmUpdateDetailsPojoFromJson(value);
      setState(() {
        totalLength = farmUpdateDetailsPojo.data.length;
        clickedPosition = farmUpdateDetailsPojo.data.length -1;
        currentPosition = farmUpdateDetailsPojo.data.length;
        loaded = true;

        if(totalLength >1) {
          update2 = true;
        }
        if(totalLength >2) {
          update3 = true;
        }
        if(totalLength >3) {
          update4 = true;
        }
        if(totalLength >4) {
          update5 = true;
        }
        if(totalLength >5) {
          update6 = true;
        }
        if(totalLength >6) {
          update7 = true;
          percentCompleteStage2 = (totalLength~/2 * 10* 1.667).toInt();
        }
        if(totalLength >7) {
          update8 = true;
        }
        if(totalLength >8) {
          update9 = true;
        }
        if(totalLength >9) {
          update10 = true;
        }
        if(totalLength >10) {
          update11 = true;
        }
        if(totalLength >11) {
          update12 = true;
        }
        if(totalLength <=6) {
          percentCompleteStage1 = (totalLength * 10 * 1.667).toInt();
          print('sss: '+percentCompleteStage1.toString());
        }




      });
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
        Navigator.pop(context);
      }),
      title: Text(Offering_Update_Details, style: TextStyle(color: Colors.white, fontSize: 18),),
      actions: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
          child: IconButton(icon: Icon(Icons.share, color: Colors.white,), onPressed: () {
            Share.share(sharingContent);
          }),
        )
      ], backgroundColor: darkGreen, centerTitle: false,),

      body: loaded ? SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(color: Colors.grey[50]),
              padding: EdgeInsets.all(16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[

                  new Container(
                      width: 80.0,
                      height: 80.0,
                      decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color:  Colors.grey,
                          borderRadius: new BorderRadius.circular(10),
                          image: new DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(baseUrl+"/storage/farmimages/"+farmDetails.farmImage)
                          )
                      )),

                  SizedBox(width: 16,),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 0,),
                      Text(farmDetails.farmtype.farmtype.substring(0,1).toUpperCase()+farmDetails.farmtype.farmtype.substring(1), style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),),
                      SizedBox(height: 4,),
                      Text(farmDetails.slug, style: TextStyle(color: Colors.black, fontSize: 14, ),),

                    ],
                  )
                ],
              )
            ),

            Container(
                decoration: BoxDecoration(color: Colors.grey[50]),
                padding: EdgeInsets.fromLTRB(16, 0, 16,16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Divider(height: 1, color: Colors.grey[300],),
                  SizedBox(height: 16,),
                  Text('Tap the numbers below to view the corresponding update', style: TextStyle(fontSize: 14, color: Colors.black),),

                  SizedBox(height: 16,),
                  Stack(
                    children: <Widget>[
                      LinearPercentIndicator(
                        width: 300,
                        lineHeight: 8.0,
                        percent: percentCompleteStage1/100,
                        animation: true,
                        animateFromLastPercent: true,
                        animationDuration: 1500,
                        linearStrokeCap: LinearStrokeCap.roundAll,
                        backgroundColor: Colors.grey[300],
                        progressColor: greenStart,
                      ),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 1;
                                  clickedPosition = 0;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black, border: Border.all(color: Colors.white, width: 2)),

                                padding: EdgeInsets.all(2),
                                height: 25,
                                child: Text('1', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),
                              ),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 2;
                                  clickedPosition = 1;
                                });
                              },
                              child: update2? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                padding: EdgeInsets.all(2),
                                height: 25,
                                child: Text('2', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white), textAlign: TextAlign.center,),

                              ) : Container(width: 0, height: 0,),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 3;
                                  clickedPosition = 2;
                                });
                              },
                              child: update3 ? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                height: 25,
                                padding: EdgeInsets.all(2),
                                child: Text('3', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),

                              ) : Container(width: 0, height: 0,),
                            ),


                          ),


                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 4;
                                  clickedPosition = 3;

                                });
                              },
                              child: update4 ? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                padding: EdgeInsets.all(2),
                                height: 25,
                                child: Text('4', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white), textAlign: TextAlign.center,),

                              ): Container(width: 0, height: 0,),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 5;
                                  clickedPosition = 4;
                                });
                              },
                              child: update5? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                height: 25,
                                padding: EdgeInsets.all(2),
                                child: Text('5', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),

                              ): Container(width: 0, height: 0,),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 6;
                                  clickedPosition = 5;
                                });
                              },
                              child:update6 ? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                height: 25,
                                padding: EdgeInsets.all(2),
                                child: Text('6', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),

                              ): Container(width: 0, height: 0,),
                            ),


                          ),
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 16,),
                  update7 ? Stack(
                    children: <Widget>[
                      LinearPercentIndicator(
                        width: 300,
                        lineHeight: 8.0,
                        percent: percentCompleteStage1/100,
                        animation: true,
                        animateFromLastPercent: true,
                        animationDuration: 1500,
                        linearStrokeCap: LinearStrokeCap.roundAll,
                        backgroundColor: Colors.grey[300],
                        progressColor: greenStart,
                      ),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 7;
                                  clickedPosition = 6;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black, border: Border.all(color: Colors.white, width: 2)),

                                padding: EdgeInsets.all(2),
                                height: 25,
                                child: Text('7', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),
                              ),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 8;
                                  clickedPosition = 7;
                                });
                              },
                              child: update8? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                padding: EdgeInsets.all(2),
                                height: 25,
                                child: Text('8', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white), textAlign: TextAlign.center,),

                              ) : Container(width: 0, height: 0,),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 9;
                                  clickedPosition = 8;
                                });
                              },
                              child: update9 ? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                height: 25,
                                padding: EdgeInsets.all(2),
                                child: Text('9', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),

                              ) : Container(width: 0, height: 0,),
                            ),


                          ),


                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 10;
                                  clickedPosition = 9;
                                });
                              },
                              child: update10 ? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                padding: EdgeInsets.all(2),
                                height: 25,
                                child: Text('10', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white), textAlign: TextAlign.center,),

                              ): Container(width: 0, height: 0,),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 11;
                                  clickedPosition = 10;
                                });
                              },
                              child: update11? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                height: 25,
                                padding: EdgeInsets.all(2),
                                child: Text('11', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),

                              ): Container(width: 0, height: 0,),
                            ),


                          ),

                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  currentPosition = 12;
                                  clickedPosition = 11;
                                });
                              },
                              child:update12 ? Container(
                                decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black,  border: Border.all(color: Colors.white, width: 2)),
                                height: 25,
                                padding: EdgeInsets.all(2),
                                child: Text('12', style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: Colors.white),textAlign: TextAlign.center,),

                              ): Container(width: 0, height: 0,),
                            ),

                          ),
                        ],
                      )
                    ],
                  ) : Container(width: 0, height: 0,)

                ],
              ),
            ),

            Column(
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,

                    children: <Widget>[
                      Expanded(
                        flex: 1,
                     child: Container(
                        decoration: BoxDecoration(color: darkGreen),
                        padding: EdgeInsets.fromLTRB(26,12, 26, 12),
                        margin: EdgeInsets.all(16),
                        child: Text('$currentPosition', style: TextStyle(fontSize: 42, color: Colors.white, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                      ),
                      ),

                      Expanded(
                        flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 16,),
                          Container(
                            padding: EdgeInsets.fromLTRB(0,0,16,0),
                            child: Text(farmUpdateDetailsPojo.data[clickedPosition].title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),)),
                          SizedBox(height: 8,),
                          Text(farmUpdateDetailsPojo.data[clickedPosition].createdAt, style: TextStyle(fontSize: 14, color: Colors.grey),)
                        ],
                      )
                      )
                    ],
                  ),

                  Container(
                    padding: EdgeInsets.all(16),
                    child: Html(data: farmUpdateDetailsPojo.data[clickedPosition].message,
                      defaultTextStyle: TextStyle(color: Colors.grey[700],height: 1.2, fontSize: 18, wordSpacing: 4),
                      onLinkTap: (url) {
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>MyWebPage(url, "Learn more")));
                      },),
                  )

                ],
              ),




          ],
        )): Center(child: Text('Loading...', style: TextStyle(fontSize: 22, color: Colors.grey),)),
    );
  }

}