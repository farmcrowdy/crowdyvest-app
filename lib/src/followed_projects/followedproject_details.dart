import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';

import '../colors.dart';
import '../projects/insurance_policy.dart';
import '../models/full_followedfarmspojo.dart';
import '../engine/values.dart';

class FollowedFarmPage extends StatefulWidget {
  final FollowedDatum followedPojo;
  FollowedFarmPage(this.followedPojo);
  @override
  FollowedFarmPageState createState()=> new FollowedFarmPageState(followedPojo);

}
final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 2);

class FollowedFarmPageState extends State<FollowedFarmPage> {
  FollowedDatum followedPojo;
  FollowedFarmPageState(this.followedPojo);
  bool farmDetails = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(centerTitle: false,
          leading: IconButton(onPressed: () {
            Navigator.pop(context);
          }, icon: Icon(Icons.keyboard_backspace), color: Colors.white,),
          backgroundColor: darkGreen,
          title: Text(
            Followed_Offering_details, style: TextStyle(color: Colors.white),),
        ),

        body: SingleChildScrollView(
          child: Column(
              children: <Widget>[
                new Container(
                    height: 200,
                    margin: EdgeInsets.fromLTRB(16, 16, 16, 16),
                    decoration: new BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(10),
                        image: new DecorationImage(
                            fit: BoxFit.cover,
                            image: new NetworkImage(
                                baseUrl+"/storage/farmimages/"+followedPojo.farmImage)
                        )
                    )),

                Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children:
                    <Widget>[


                      Text(followedPojo.farmtype.farmtype[0].toUpperCase() +
                          followedPojo.farmtype.farmtype.substring(1),
                        style: TextStyle(color: Colors.black,
                            fontSize: 22,
                            fontWeight: FontWeight.bold),),
                      Text(followedPojo.slug, style: TextStyle(
                          color: Colors.grey[700], fontSize: 18),),
                      SizedBox(height: 8,),
                      Text('N${formatCurrency.format(
                          followedPojo.pricePerunit)}/unit', style: TextStyle(
                          color: Colors.grey[700], fontSize: 16),),
                      Text(followedPojo.roi.toString() +
                          '% '+returns_on_sponsorship, style: TextStyle(
                          color: Colors.grey[700], fontSize: 16),),
                      SizedBox(height: 8,),
                      Divider(color: Colors.grey[300],),
                      SizedBox(height: 8,),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: greenStart, width: 1.0),

                            ),child: Text(Offering_Description),

                          ),

                        GestureDetector(
                          onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=> InsuranePolicy()));
                          },
                          child:Container(
                            padding: EdgeInsets.fromLTRB(16, 8, 16, 8),
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: Colors.grey[400], width: 1.0),

                            ),child: Text('Insurance Policy', style: TextStyle(color: Colors.grey[400]),),

                          ),
                        ),


                      ],),

                      SizedBox(height: 8,),
            Html(data: followedPojo.farmDescription,
            defaultTextStyle: TextStyle(color: Colors.grey[700],height: 1.2, fontSize: 18, wordSpacing: 4),)


                    ],
                  ),
                ),
              ]),
        )
    );
  }

}