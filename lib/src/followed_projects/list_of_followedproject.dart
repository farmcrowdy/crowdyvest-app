import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../colors.dart';
import '../projects/portfolio.dart';
import 'followedproject_details.dart';
import '../models/full_followedfarmspojo.dart';
import '../engine/values.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

class FollowedFarmsPage extends StatefulWidget {

  final FirebaseAnalyticsObserver observer;
  FollowedFarmsPage(this.observer);

  @override
  FollowedFarmPageState createState()=> new FollowedFarmPageState(observer);

}

class FollowedFarmPageState extends State<FollowedFarmsPage> with RouteAware {
  int userId;
  bool updated = false;

  final FirebaseAnalyticsObserver observer;


  FollowedFarmPageState(this.observer);

  loadDefaults() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      userId = (prefs.getInt('userid') ?? 0);
      updated = true;
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadDefaults();
  }


  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsFollowedProject(observer);
  }


  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(backgroundColor: darkGreen,
        leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
          Navigator.pop(context);
        }),
        title: Text("Followed Projects", style: TextStyle(color: Colors.white),), centerTitle: false,),

      body: Center(
        child: updated ? _asyncLoaderFollowedProjects(userId.toString()) : Text('Loading...'),
      )
    );
  }

  _getListOfFollowedProjects(String sponsorId) async  {
    final response = await http.get(
      baseUrl+'/api/follow/'+sponsorId,
      headers: headerAuth,
    );

    return response.body;
  }

  _asyncLoaderFollowedProjects(String sponsorId)=> new AsyncLoader(

      initState: () async => await _getListOfFollowedProjects(sponsorId),
      renderLoad: () =>new CircularProgressIndicator(),
      renderError: ([error]) =>
      new Text('Sorry, there was an error loading. Please check internet connection', style: TextStyle(color: Colors.black),),
      renderSuccess: ({data}) {
        try{
          return _buildFollowedProjects(fullFollowedPojoFromJson(data));
        }
        catch(e) {
          return Center(child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(OfferingNotYetFollowed, style: TextStyle(fontSize: 20, color: Colors.black),),
              RaisedButton.icon(onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmShopPageState(observer)));
              }, icon: Icon(Icons.shopping_basket, color: Colors.white,), label: Text(See_Offerings_you_can_follow, style: TextStyle(color: Colors.white),), color: greenStart, )
            ],
          ));
        }
      }

  );


  Widget _buildFollowedProjects(FullFollowedPojo listOfFollowedProjects) {
    return new ListView.separated(
        itemCount: listOfFollowedProjects.data.length,
        separatorBuilder: (BuildContext context, int index) {
          return Container(
              margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 16.0),
              child: new Divider(color: Colors.grey[300], )
          );
        },
        itemBuilder: (BuildContext _context, int i) {

          var tempProjectInfo = listOfFollowedProjects.data[i];

          var dsa= new DateTime.now().difference(DateTime.parse(tempProjectInfo.createdAt));
          String finalDate = "";
          if(dsa.inDays ==1) {
            finalDate = 'Yesterday';
          }
          else if(dsa.inDays ==0) {
            finalDate = dsa.inHours.toString()+ ' hours ago';

            if(dsa.inHours ==0) {
              finalDate = dsa.inMinutes.toString()+' mins ago';
            }
          }

          else {
            finalDate = dsa.inDays.toString()+' days ago';
          }

          return _buildRow(tempProjectInfo, finalDate, _context);
        }
    );
  }


}


Widget _buildRow(FollowedDatum updateData, String date, context,) {
  //print('we are now');
  var shortestSide = MediaQuery
      .of(context)
      .size
      .shortestSide;
  var useMobileLayout = shortestSide < 600;

  if (useMobileLayout) {
    return  new ListTile(
          onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>FollowedFarmPage(updateData))),
          contentPadding: EdgeInsets.symmetric(horizontal: 18, vertical: 11),
          title: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 14,),
              Text(updateData.farmtypename[0].toUpperCase()+updateData.farmtypename.substring(1), style: TextStyle(color: greenStart, fontSize: 18, ), ),
              SizedBox(height: 3,),
              Text('N'+updateData.pricePerunit.toString()+'/unit', style: TextStyle(fontSize: 16, color: Colors.grey),),

              SizedBox(height: 3,),
              Text(Offering_opened +" "+date, style: TextStyle(fontSize: 14, color: Colors.grey),),

              SizedBox(height: 3,),
              Text(updateData.slug, style: TextStyle(color: Colors.grey, fontSize: 14, ),),

              SizedBox(height: 8,),

              Text(updateData.roi.toString()+'% returns in '+updateData.farmCycle.toString()+' months', style: TextStyle(fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),),




            ],
          ),

          leading:   new Container(
              width: 105.0,
              height: 105.0,
              decoration: new BoxDecoration(
                  shape: BoxShape.rectangle,
                  color:  Colors.grey,
                  borderRadius: new BorderRadius.circular(10),
                  image: new DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(baseUrl+"/storage/farmimages/"+updateData.farmImage)
                  )
              )),
          //leading: ,


    );
  }

  else {
    return  new ListTile(
      onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>FollowedFarmPage(updateData))),
      contentPadding: EdgeInsets.symmetric(horizontal: 18, vertical: 11),
      title: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 14,),
          Text(updateData.farmtypename[0].toUpperCase()+updateData.farmtypename.substring(1), style: TextStyle(color: greenStart, fontSize: 22, ), ),
          SizedBox(height: 3,),
          Text('N'+updateData.pricePerunit.toString()+'/unit', style: TextStyle(fontSize: 20, color: Colors.grey),),

          SizedBox(height: 3,),
          Text(Offering_opened+' '+date, style: TextStyle(fontSize: 18, color: Colors.grey),),

          SizedBox(height: 3,),
          Text(updateData.slug, style: TextStyle(color: Colors.grey, fontSize: 18, ),),

          SizedBox(height: 8,),

          Text(updateData.roi.toString()+'% returns in '+updateData.farmCycle.toString()+' months', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold),),




        ],
      ),

      leading:   new Container(
          width: 105.0,
          height: 105.0,
          decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              color:  Colors.grey,
              borderRadius: new BorderRadius.circular(10),
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(baseUrl+"/storage/farmimages/"+updateData.farmImage)
              )
          )),
      //leading: ,


    );
  }
                    // ... to here.

}