import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'colors.dart';


class MyWebPage extends StatelessWidget {
  final String url, title;

  MyWebPage(this.url, this.title);



  bool gotoDashboard=false;


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    new Timer(new Duration(seconds: 15), () {
      gotoDashboard = true;
    });

    return  WebviewScaffold(
      url: url,
        initialChild: Container(
          color: Colors.white,
          child: const Center(
            child: Text('Loading.....', style: TextStyle(color: Colors.grey, fontSize: 20),),
          ),
        ),
        appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: () {
           Navigator.pop(context);
        }), title: Text(title, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),), elevation: 0,
          centerTitle: false, backgroundColor: darkGreen,),
    );

  }
}