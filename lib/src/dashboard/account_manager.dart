import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import '../colors.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'message_success.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';
import '../analytics/actions_lytics.dart';

var _subjectController = new TextEditingController();
var _contentController = new TextEditingController();
class AccountManager extends StatelessWidget with RouteAware {
  final FirebaseAnalyticsObserver observer;
  AccountManager(this.observer);


  void _sendCurrentTabToAnalytics() {
    lyticsAccountManager(observer);
  }

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build




    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    if (useMobileLayout) {
      return Scaffold(
          appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: ()=> Navigator.pop(context)),
            title: Text('Contact Account Manager', style: TextStyle(color: Colors.white),), centerTitle: false, backgroundColor: darkGreen,) ,

          body: Builder(
              builder: (contexter)=>
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                    child: Column(
                      children: <Widget>[
                        TextField(style: TextStyle(fontSize: 18, color: Colors.black),controller: _subjectController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.subject), labelText: 'Subject topic',), obscureText: false),
                        TextField(style: TextStyle(fontSize: 18, color: Colors.black),controller: _contentController, decoration: InputDecoration(icon:new Icon(Icons.text_fields), labelText: 'Message content', alignLabelWithHint: true ), obscureText: false, maxLines: 7, textAlign: TextAlign.start, ),

                        SizedBox(height: 32,),

                        new SizedBox(
                            width: double.infinity,
                            height: 42,

                            child: RaisedButton(onPressed: () {
                              sendMessage(contexter, observer);
                            }, child: Text('Send message', style: TextStyle(fontSize: 18, color: Colors.white), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
                        )

                      ],
                    ),
                  )
          )
      );
    }

    else {
      return Scaffold(
          appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.white,), onPressed: ()=> Navigator.pop(context)),
            title: Text('Contact Account Manager', style: TextStyle(color: Colors.white),), centerTitle:false, backgroundColor: darkGreen,) ,

          body: Builder(
              builder: (contexter)=>
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                    child: Column(
                      children: <Widget>[
                        TextField(style: TextStyle(fontSize: 24, color: Colors.black),controller: _subjectController, maxLines: 1, decoration: InputDecoration(icon:new Icon(Icons.subject), labelText: 'Subject topic',), obscureText: false),
                        TextField(style: TextStyle(fontSize: 24, color: Colors.black),controller: _contentController, decoration: InputDecoration(icon:new Icon(Icons.text_fields), labelText: 'Message content', alignLabelWithHint: true ), obscureText: false, maxLines: 7, textAlign: TextAlign.start, ),

                        SizedBox(height: 32,),

                        new SizedBox(
                            width: double.infinity,
                            height: 42,

                            child: RaisedButton(onPressed: () {
                              sendMessage(contexter, observer);
                            }, child: Text('Send message', style: TextStyle(fontSize: 24, color: Colors.white), textAlign: TextAlign.center,), color: greenStart, padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16), )
                        )

                      ],
                    ),
                  )
          )
      );

    }

  }
}
void sendMessage(BuildContext contexter, FirebaseAnalyticsObserver observer)  async {
  if(_subjectController.text.length < 10) {
    Scaffold.of(contexter).showSnackBar(SnackBar(
      content: Text('Suject topic too short. Minimum of 10 chars...'),
      duration: Duration(seconds: 3),));
  }
  else if(_contentController.text.length < 20) {
    Scaffold.of(contexter).showSnackBar(SnackBar(
      content: Text('Message content too short, minimum of 20 chars...'),
      duration: Duration(seconds: 3),));
  }
  else {
   ProgressDialog pr = new ProgressDialog(contexter);
   pr.style(message:'Sending...');
    if(!pr.isShowing()) {
      pr.show();
      SharedPreferences prefs = await SharedPreferences.getInstance();

      int userId = (prefs.getInt('userid') ?? 1);
      String email = prefs.get('email');

      //print("lavierier id is: "+ userId.toString());

      final response = await http.post(
        baseUrl+'/api/contactmanager',

        body: {"id": userId.toString(), "help_type": _subjectController.text,
          "message": _contentController.text, "email":email, "phone": ""},
        headers: headerAuth,
      );

      pr.hide();
      print('contact response: '+response.body);
      if(response.statusCode == 200) {
        sentMessageToAccountManager(observer);

        String email = prefs.get('email');
        Route route = MaterialPageRoute(builder: (context) => MessageSuccess(email));
        Navigator.pushReplacement(contexter, route);
      }
      else {
        Scaffold.of(contexter).showSnackBar(SnackBar(
          content: Text('Message not sent, please try again...'),
          duration: Duration(seconds: 4),));
      }
    }
  }
}
