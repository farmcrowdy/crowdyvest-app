import 'package:crowdyvest/src/analytics/screensLogging.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import '../colors.dart';
import '../projects/portfolio.dart';
import '../models/sponsorbyidpojo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../sponsorship_history/sponsorshiphistory.dart';
import '../project_updates/projectupdate.dart';
import '../profile/mainprofile.dart';
import '../sponsorship_history/h_sponsoredproject.dart';
import '../followed_projects/list_of_followedproject.dart';
import '../webpage.dart';
import '../cart/cart.dart';
import 'account_manager.dart';
import 'package:intl/intl.dart';
import '../engine/values.dart';
import '../engine/noAnimationPageRouter.dart';
import 'package:flutter/services.dart';
import '../faq/faq.dart';
import 'package:firebase_analytics/observer.dart';



class DashboardPageState extends StatefulWidget{
  final FirebaseAnalyticsObserver observer;

  DashboardPageState(this.observer);

  @override
  DashboardPage createState() =>new DashboardPage(observer);
}


final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 0);

class DashboardPage extends State<DashboardPageState> with RouteAware{

  int userId;
  String fname = "Loading...";
  double farmSponsoredAmount;
  String nextendofcycle = "0";
  String numberOfcyclesRunning = "0";
  String propix = "";

  final FirebaseAnalyticsObserver observer;


  DashboardPage(this.observer);


  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsDashboard(observer);
  }



  loadDefaults() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userId = (prefs.getInt('userid') ?? 0);
     fname = prefs.getString('fname')[0] + prefs.getString('fname').substring(1).toLowerCase();
      //fname = "Your first name";
      farmSponsoredAmount = (prefs.getDouble('farmSponsoredAmount') ?? 0);
      nextendofcycle = (prefs.getString('nextendofcycle') ?? "0");
      numberOfcyclesRunning = (prefs.getString('numberOfcyclesRunning') ?? "0");
      propix = prefs.get("propix");
      _getSponsorshipDetails();
    });
  }

  _getSponsorshipDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('$userId');
    final response = await http.get(
      baseUrl+'/api/sponsor/$userId',
      headers: headerAuth );
    if(response.statusCode == 200) {
      final sponsorById = sponsorByIdFromJson(response.body);

      // var dsa = new DateTime.now().difference(DateTime.parse(sponsorById.data.la))
      var dsa = DateTime.parse(sponsorById.data.first.endDate).difference(
          new DateTime.now());
      String finalDate = "";

      if (dsa.inDays == 1 && !dsa.isNegative) {
        finalDate = 'Yesterday';
      }

      else if (dsa.inDays == 0 && !dsa.isNegative) {
        finalDate = 'in ' + dsa.inHours.toString() + ' hours';

        if (dsa.inHours == 0 && !dsa.isNegative) {
          finalDate = 'in ' + dsa.inMinutes.toString() + ' mins';
        }
      }

      else if (!dsa.isNegative) {
        finalDate = 'in ' + dsa.inDays.toString() + ' days';
        if (dsa.inDays > 60) {
          finalDate = 'in ' + (dsa.inDays / 30).round().toString() + 'months';
        }
      }

      else {
        finalDate = 'has passed';
      }

      int numberOfActive = 0;
      double tempTotalExpected = 0;
      for (SponsorDatum datum in sponsorById.data) {
        var dsa = DateTime.parse(datum.endDate).difference(new DateTime.now());
        if (!dsa.isNegative) {
          numberOfActive = numberOfActive + 1;
        }
        if (datum.repaymentStatus == null) {
          int costPayed = datum.sponsorUnit * datum.farmDetails.pricePerunit;
          double presentExpectedReturns = costPayed +
              ((datum.farmDetails.roi / 100) * costPayed);
          tempTotalExpected = tempTotalExpected + presentExpectedReturns;
        }
      }

      await prefs.setString('nextendofcycle', finalDate);
      await prefs.setString('numberOfcyclesRunning', numberOfActive.toString());
      await prefs.setDouble("farmSponsoredAmount", tempTotalExpected);


      setState(() {
        nextendofcycle = finalDate;
        numberOfcyclesRunning = numberOfActive.toString();
        farmSponsoredAmount = tempTotalExpected;
      });
    }
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    observer.unsubscribe(this);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadDefaults();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    void onTabTapped(int index) {
      switch (index) {
        case 1:
          Navigator.push(context,
              NoAnimationPageRouter(builder: (context) => FarmShopPageState(observer)));
          break;
        case 2:
          Navigator.push(
              context, NoAnimationPageRouter(builder: (context) => MainProfile(observer)));
          break;
      }
    }


    var shortestSide = MediaQuery
        .of(context)
        .size
        .shortestSide;
    var useMobileLayout = shortestSide < 600;

    //CHECK FOR MOBILE SCREEN AND USES THIS CODE.

    //////////////// MOBILE SCREEN STARTS
    if (useMobileLayout) {

      return Scaffold(
          appBar: AppBar( automaticallyImplyLeading: false,
            leading: SizedBox(width: 0, height: 0,),
            elevation: 0,
            backgroundColor: Colors.white,
            brightness: Brightness.light,
            primary: true,
            title: Text("Hello $fname ...",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1, overflow: TextOverflow.ellipsis,),
            iconTheme: IconThemeData(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 2,
                          child: new

                          GestureDetector(onTap: () =>
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => MainProfile(observer))), child:

                          new Container(
                            width: 60.0,
                            height: 60.0,
                            margin: EdgeInsets.fromLTRB(16, 16, 16, 16),
                            decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(100),
                                color: Colors.grey[300],
                                image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(baseUrl + '/storage/propixs/' + propix),
                                )
                            ),
                          ),

                          )),

                      Expanded(
                        flex: 4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 22),

                            GestureDetector(
                              onTap: () =>
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          SponsorshipHistoryPage(observer))),
                              child: Text('$numberOfcyclesRunning' +
                                  ' project cycles running', style: TextStyle(
                                  fontSize: 14, color: Colors.black),),),

                            SizedBox(height: 4,),

                            GestureDetector(onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      SponsorshipHistoryPage(observer)));
                            },
                              child: Text(Sponsorship_History,
                                style: TextStyle(
                                    fontSize: 14, color: greenStart),),
                            ),
                            SizedBox(height: 140,)
                          ],
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Container(
                              margin: EdgeInsets.fromLTRB(16, 6, 6, 16),
                              child: IconButton(icon: Icon(
                                Icons.shopping_cart, color: Colors.black,),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => CartPage(observer)));
                                  }
                              )

                          ))

                    ],

                  ),
                ),

                Container(
                    //elevation: 5,
                    margin: EdgeInsets.fromLTRB(12, 110, 12, 16),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.grey[300]),
                    padding: EdgeInsets.all(12),
                    // padding:EdgeInsets.all(16)
                    //child:
                    child:
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SponsorshipHistoryPage(observer)));
                                      },
                                      child: Container(
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          borderRadius: BorderRadius.circular(
                                              5),
                                          color: Colors.grey[50],
                                        ),
                                        padding: EdgeInsets.all(12),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: <Widget>[
                                            Text(Total_Expected_returns,
                                              style: TextStyle(fontSize: 14,
                                                  color: Colors.grey),),
                                            SizedBox(height: 4,),
                                            (farmSponsoredAmount != null) ? Text('N${formatCurrency.format(farmSponsoredAmount)}',
                                              style: TextStyle(fontSize: 16,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight
                                                      .bold),) : Text('N 0.00',
                                              style: TextStyle(fontSize: 16,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight
                                                      .bold),),
                                            SizedBox(height: 8,),
                                            Text("View",
                                              style: TextStyle(fontSize: 14,
                                                  color: greenStart),),
                                          ],
                                        ),
                                      )
                                  ),),
                                SizedBox(width: 12,),

                                Expanded(
                                    child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SponsorshipHistoryPage(observer)));
                                        },
                                        child: Container(
                                            decoration: new BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              borderRadius: BorderRadius
                                                  .circular(5),
                                              color: Colors.grey[50],
                                            ),
                                            padding: EdgeInsets.all(12),
                                            child:
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .start,
                                              children: <Widget>[
                                                Text(Next_end_of_cycle_date,
                                                  style: TextStyle(fontSize: 14,
                                                      color: Colors.grey),),
                                                SizedBox(height: 4,),
                                                Text('$nextendofcycle',
                                                  style: TextStyle(fontSize: 16,
                                                      color: Colors.black,
                                                      fontWeight: FontWeight
                                                          .bold),),
                                                SizedBox(height: 8,),
                                                Text("View",
                                                  style: TextStyle(fontSize: 14,
                                                      color: greenStart),),
                                              ],
                                            )
                                        )
                                    )),
                              ],
                            ),
                ),

          new Container(
              width: double.infinity,
              height: 50,
              margin: EdgeInsets.fromLTRB(16, 242, 16, 0),

              child: new RaisedButton.icon(
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) =>
                          FarmShopPageState(observer)));
                },
                icon: Icon(Icons.shopping_basket, color: Colors.white,),
                label: Text(Sponsor_Available_Offerings,
                  style: TextStyle(fontSize: 16, color: Colors.white),),
                color: darkGreen,
                elevation: 7,)

          ),

                Container(
                  margin: EdgeInsets.fromLTRB(16, 320, 16, 16),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => SponsoredFarmsPage(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(18),
                                  width: 100.0,
                                  height: 100.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/sponsored_portfolios.png",),

                                ),
                                SizedBox(height: 8,),
                                Text(Sponsored_Offerings, style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 14,
                                    height: 1.2), textAlign: TextAlign.center)
                              ],
                            ),
                          ),


                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => FollowedFarmsPage(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(18),
                                  width: 100.0,
                                  height: 100.0,
                                  // margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/followed_porfolios.png"),
                                ),
                                SizedBox(height: 8,),
                                Text("Followed\nProjects", style: TextStyle(
                                    color: Colors.grey[800], height: 1.2),
                                  textAlign: TextAlign.center,)
                              ],
                            ),
                          ),

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => FarmUpdatePage(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 100.0,
                                  height: 100.0,
                                  //margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/portfolio_updates.png"),
                                ),
                                SizedBox(height: 8,),
                                Text("Project\nUpdates", style: TextStyle(
                                    color: Colors.grey[800], height: 1.2),
                                    textAlign: TextAlign.center)
                              ],
                            ),
                          )

                        ],
                      ),

                      SizedBox(height: 9,),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => FarmShopPageState(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 100.0,
                                  height: 100.0,
                                  margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/browse_portfolios.png"),
                                ),
                                SizedBox(height: 8,),
                                Text("Browse All\nProjects", style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 14,
                                    height: 1.2), textAlign: TextAlign.center)
                              ],
                            ),),

                          GestureDetector(
                            onTap: () =>
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => AccountManager(observer))),
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 100.0,
                                  height: 100.0,
                                  margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/acccount_manager.png"),
                                ),
                                SizedBox(height: 8,),
                                Text(Account_Manager, style: TextStyle(
                                    color: Colors.grey[800], height: 1.2),
                                    textAlign: TextAlign.center)
                              ],
                            ),
                          ),

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                    FAQ(observer)));
                            },

                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 100.0,
                                  height: 100.0,
                                  margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/faqs.png"),
                                ),
                                SizedBox(height: 8,),
                                Text('FAQ      \n',
                                  style: TextStyle(color: Colors.grey[800]),
                                  textAlign: TextAlign.center,)
                              ],
                            ),),

                        ],
                      )


                    ],
                  ),
                )


              ],
            ),
          ),
          bottomNavigationBar:
          Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[500],
                  offset: Offset(0.01, 0.02),
                  blurRadius: 5.0,
                ),
              ],


            ),
            child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,

                onTap: onTabTapped, // new
                items: [
                  new BottomNavigationBarItem(
                      icon: Container(child: Image.asset("assets/dash13.png"),
                        width: 24,
                        height: 24,),
                      title: Text(Dashboard, style: TextStyle(
                          color: greenStart, fontWeight: FontWeight.bold),),
                      backgroundColor: Colors.grey[600]
                  ),

                  new BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shopping_basket, color: darkGreen, size: 24,),
                      title: Text(
                        Portfolio, style: TextStyle(color: Colors.black),),
                      backgroundColor: Colors.grey[600]
                  ),

                  new BottomNavigationBarItem(
                      icon: Container(child: Image.asset("assets/dash14.png"),
                        width: 24,
                        height: 24,),
                      title: Text(
                        Profile, style: TextStyle(color: Colors.black),),
                      backgroundColor: Colors.grey[600]),

                ]
            ),
          )
      );
    }



    ////////MOBILE SCREEN ENDS

    /////TABLE SCREEN STARTS, expected with the same behavior but increased sizes of components


    else {
      return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar( automaticallyImplyLeading: false,
            leading: SizedBox(width: 0, height: 0,),
            elevation: 0,
            backgroundColor: Colors.white,
            brightness: Brightness.light,
            primary: true,
            title: Text("Hello $fname ...",style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold), maxLines: 1, overflow: TextOverflow.ellipsis,),
            iconTheme: IconThemeData(color: Colors.black),
          ),
          body: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(color: darkGreen),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 2,
                          child: new

                          GestureDetector(onTap: () =>
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => MainProfile(observer))), child:
                          new Container(
                            width: 100.0,
                            height: 100.0,
                            margin: EdgeInsets.fromLTRB(16, 75, 0, 16),
                            decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(100),
                                image: new DecorationImage(
                                  fit: BoxFit.cover,
                                  image: NetworkImage(
                                      baseUrl + '/storage/propixs/' + propix),
                                )
                            ),
                          ),

                          )),

                      SizedBox(width: 8,),
                      Expanded(
                        flex: 4,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 48),

                            GestureDetector(
                              onTap: () =>
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          SponsorshipHistoryPage(observer))),
                              child: Text('$numberOfcyclesRunning farm_cycles_running', style: TextStyle(
                                  fontSize: 23, color: Colors.white),),),

                            SizedBox(height: 4,),

                            GestureDetector(onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                      SponsorshipHistoryPage(observer)));
                            },
                              child: Text(Sponsorship_History +"->",
                                style: TextStyle(
                                    fontSize: 23, color: greenStart),),
                            ),
                            SizedBox(height: 140,)
                          ],
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Container(
                              margin: EdgeInsets.fromLTRB(16, 65, 6, 16),
                              child: IconButton(icon: Icon(
                                Icons.shopping_cart, color: greenStart, size: 40,),
                                  onPressed: () {
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => CartPage(observer)));
                                  }
                              )

                          ))

                    ],

                  ),
                ),

                Card(
                    elevation: 5,
                    margin: EdgeInsets.fromLTRB(16, 200, 16, 16),
                    // padding:EdgeInsets.all(16)
                    //child:
                    child: Container(
                        padding: EdgeInsets.all(16),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: GestureDetector(
                                      onTap: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SponsorshipHistoryPage(observer)));
                                      },
                                      child: Container(
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          borderRadius: BorderRadius.circular(
                                              5),
                                          color: Colors.grey[50],
                                        ),
                                        padding: EdgeInsets.all(16),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: <Widget>[
                                            Text(Total_Expected_returns,
                                              style: TextStyle(fontSize: 22,
                                                  color: Colors.grey),),
                                            SizedBox(height: 4,),
                                            Text('N${formatCurrency.format(
                                                farmSponsoredAmount)}',
                                              style: TextStyle(fontSize: 20,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight
                                                      .bold),),
                                            SizedBox(height: 8,),
                                            Text("View History ->",
                                              style: TextStyle(fontSize: 20,
                                                  color: greenStart),),
                                          ],
                                        ),
                                      )
                                  ),),
                                SizedBox(width: 16,),

                                Expanded(
                                    child: GestureDetector(
                                        onTap: () {
                                          Navigator.push(context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SponsorshipHistoryPage(observer)));
                                        },
                                        child: Container(
                                            decoration: new BoxDecoration(
                                              shape: BoxShape.rectangle,
                                              borderRadius: BorderRadius
                                                  .circular(5),
                                              color: Colors.grey[50],
                                            ),
                                            padding: EdgeInsets.all(16),
                                            child:
                                            Column(
                                              crossAxisAlignment: CrossAxisAlignment
                                                  .start,
                                              children: <Widget>[
                                                Text(Next_end_of_cycle_date,
                                                  style: TextStyle(fontSize: 22,
                                                      color: Colors.grey),),
                                                SizedBox(height: 4,),
                                                Text('$nextendofcycle',
                                                  style: TextStyle(fontSize: 20,
                                                      color: Colors.black,
                                                      fontWeight: FontWeight
                                                          .bold),),
                                                SizedBox(height: 8,),
                                                Text(View_Sponsorships +"->",
                                                  style: TextStyle(fontSize: 20,
                                                      color: greenStart),),
                                              ],
                                            )
                                        )
                                    )),
                              ],
                            ),

                            SizedBox(height: 16,),
                            new SizedBox(
                                width: double.infinity,
                                height: 62,

                                child: new RaisedButton.icon(
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),

                                  onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          FarmShopPageState(observer)));
                                },
                                  icon: Icon(Icons.shopping_basket, size: 40,),
                                  label: Text(Sponsor_Available_Offerings,
                                    style: TextStyle(fontSize: 20, ),),
                                  color: greenStart,
                                  elevation: 7,)

                            )


                          ],
                        )
                    )
                ),

                Container(
                  margin: EdgeInsets.fromLTRB(16, 500, 16, 16),
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => SponsoredFarmsPage(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(18),
                                  width: 150.0,
                                  height: 150.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/sponsored_portfolios.png",),

                                ),
                                SizedBox(height: 16,),
                                Text(Sponsored_Offerings, style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 22,
                                    height: 1.2), textAlign: TextAlign.center)
                              ],
                            ),
                          ),


                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => FollowedFarmsPage(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(18),
                                  width: 150.0,
                                  height: 150.0,
                                  // margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/followed_porfolios.png"),
                                ),
                                SizedBox(height: 16,),
                                Text(Followed_Offerings, style: TextStyle(
                                    color: Colors.grey[800], height: 1.2, fontSize: 22),
                                  textAlign: TextAlign.center,)
                              ],
                            ),
                          ),

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => FarmUpdatePage(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 150.0,
                                  height: 150.0,
                                  //margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/portfolio_updates.png"),
                                ),
                                SizedBox(height: 16,),
                                Text(Offering_Updates, style: TextStyle(
                                    color: Colors.grey[800], height: 1.2, fontSize: 22),
                                    textAlign: TextAlign.center)
                              ],
                            ),
                          )

                        ],
                      ),

                      SizedBox(height: 9,),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => FarmShopPageState(observer)));
                            },
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 150.0,
                                  height: 150.0,
                                  margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/browse_portfolios.png"),
                                ),
                                SizedBox(height: 16,),
                                Text(Browse_All_Offerings, style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 22,
                                    height: 1.2), textAlign: TextAlign.center)
                              ],
                            ),),

                          GestureDetector(
                            onTap: () =>
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => AccountManager(observer))),
                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 150.0,
                                  height: 150.0,
                                  margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/acccount_manager.png"),
                                ),
                                SizedBox(height: 16,),
                                Text(Account_Manager, style: TextStyle(
                                    color: Colors.grey[800], height: 1.2, fontSize: 22),
                                    textAlign: TextAlign.center)
                              ],
                            ),
                          ),

                          GestureDetector(
                            onTap: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (context) =>
                                     FAQ(observer)));
                            },

                            child: Column(
                              children: <Widget>[
                                new Container(
                                  padding: EdgeInsets.all(20),
                                  width: 150.0,
                                  height: 150.0,
                                  margin: EdgeInsets.fromLTRB(0, 24, 0, 0),
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                        color: Colors.grey[300],
                                        offset: Offset(0.01, 0.02),
                                        blurRadius: 10.0,
                                      ),
                                    ],
                                    color: Colors.white,

                                  ),
                                  child: Image.asset("assets/faqs.png"),
                                ),
                                SizedBox(height: 16,),
                                Text('FAQ      \n',
                                  style: TextStyle(color: Colors.grey[800], fontSize: 22),
                                  textAlign: TextAlign.center,)
                              ],
                            ),),

                        ],
                      )


                    ],
                  ),
                )


              ],
            ),
          ),
          bottomNavigationBar:
          Container(
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.grey[500],
                  offset: Offset(0.01, 0.02),
                  blurRadius: 5.0,
                ),
              ],


            ),
            child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,

                onTap: onTabTapped, // new
                items: [
                  new BottomNavigationBarItem(
                      icon: Container(child: Image.asset("assets/dash13.png"),
                        width: 34,
                        height: 34,),
                      title: Text(Dashboard, style: TextStyle(
                          color: greenStart, fontWeight: FontWeight.bold, fontSize: 26),),
                      backgroundColor: Colors.grey[600]
                  ),

                  new BottomNavigationBarItem(
                      icon: Icon(
                        Icons.shopping_basket, color: darkGreen, size: 34,),
                      title: Text(
                        Portfolio, style: TextStyle(color: Colors.black, fontSize: 26),),
                      backgroundColor: Colors.grey[600]
                  ),

                  new BottomNavigationBarItem(
                      icon: Container(child: Image.asset("assets/dash14.png"),
                        width: 34,
                        height: 34,),
                      title: Text(
                        Profile, style: TextStyle(color: Colors.black, fontSize: 26),),
                      backgroundColor: Colors.grey[600]),

                ]
            ),
          )
      );
    }

    /////TABLE SCREEN ENDS.



  }

}




