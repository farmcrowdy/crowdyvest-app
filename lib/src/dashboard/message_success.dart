import 'package:flutter/material.dart';
import '../colors.dart';

class MessageSuccess extends StatelessWidget {
  final String email;
  MessageSuccess(this.email);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            Icon(Icons.mail, color: Colors.grey[400], size: 100,),
            Text('Message sent to account manager', style: TextStyle(fontSize: 30, color: Colors.grey), textAlign: TextAlign.center,),
            SizedBox(height: 16,),

            RaisedButton.icon(onPressed: ()=>Navigator.pop(context), icon: Icon(Icons.keyboard_backspace, color: Colors.white,), label: Text('Go to Dashboard', style: TextStyle(color: Colors.white, fontSize: 18),), color: greenStart,)


          ],
        ),
      ),
    );
  }

}
