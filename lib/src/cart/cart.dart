import 'dart:async';
import 'dart:ui';

import 'package:async_loader/async_loader.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:progress_dialog/progress_dialog.dart';
//import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../payment/checkoutpage.dart';
import '../colors.dart';
import '../models/cartdetails.dart';
import '../analytics/actions_lytics.dart';
import '../analytics/screensLogging.dart';
import 'package:firebase_analytics/observer.dart';
import '../payment/api_payments.dart';


ProgressDialog pr;

final formatCurrency = new NumberFormat.currency(symbol: "", name: "", decimalDigits: 2);
TextEditingController unitController = new TextEditingController();







class CartPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;


  CartPage(this.observer);

  @override
  CartPageState createState() => new CartPageState(observer);
}


class CartPageState extends State<CartPage> with RouteAware{
  final FirebaseAnalyticsObserver observer;


  int userId;
  int subtotal = 0;
  int mgtfee = 0;
  int finalTotal = 0;
  bool _loading = true;
  CartDetails myCartDetails;


  CartPageState(this.observer);

  _checkForSponsorId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      userId = (prefs.getInt('userid') ?? 0);
      _loading = false;
    });
  }


  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsCartPage(observer);
  }

  @override
  void dispose() {
    observer.unsubscribe(this);
    super.dispose();
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _checkForSponsorId();
  }

  void callback(int addition, bool type, int action) {
    //Where Action is to check if its either addition or substration
    setState(() {
      if (action == 0) {
        if (type) subtotal = subtotal + addition;
        else if (subtotal > 0) subtotal = subtotal - addition;
        print('it entered here with: ' + subtotal.toString());
      } else {
        print('this is for delete');
      }
    });
  }

  void cartListCallback(CartDetails initialDetails, int position, int actionType) {
    //Action 1 = set initial details
    //Action 2 = Add more units to a particular cart id;
    //Action 3 = Remove unit from a particular cart id;
    //Action 4 = Remove an entire cart item
    setState(() {
      if (actionType == 1) {
        myCartDetails = initialDetails;
      }
    });
  }

  void adjustCartCallback(int position, int initial, int maintype) {
    //Remove item from cart list
    //Then remove from the sub total.
    if(maintype == 2) {
      setState(() {
        myCartDetails.data.removeAt(position);
        if (subtotal > 0) subtotal = subtotal - initial;
      });
    }
    else {
      setState(() {
        if (initial == 2) {
          // myCartDetails.data[position].sponsorUnit = myCartDetails.data[position].sponsorUnit + 1;
        } else if (initial == 3) {
          //myCartDetails.data[position].sponsorUnit = myCartDetails.data[position].sponsorUnit - 1;
        } else {
          myCartDetails.data.removeAt(position);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    pr = new ProgressDialog(context);
    pr.style(message: 'Please wait...');

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.keyboard_backspace,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
          title: Text(
            'My Cart',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: darkGreen,
          centerTitle: false,
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Icons.share,
                  color: Colors.white,
                ),
                onPressed: () {
                  Share.share(
                      'I\'ve you checked out the Crowdyvest mobile app yet? Check it out on https://crowdyvest.com');
                })
          ],
        ),
        backgroundColor: Colors.grey[300],
        body: Builder(
          builder: (context) => Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.fromLTRB(0, 0, 0, 200),
                    child: Center(
                        child: _loading
                            ? Text('Loading...')
                            : _asyncLoaderCartItem(
                                this.callback,
                                this.cartListCallback,
                                this.adjustCartCallback)),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 16, horizontal: 16),
                          decoration: BoxDecoration(
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                color: Colors.grey[500],
                                offset: Offset(0.01, 0.05),
                                blurRadius: 10.0,
                              ),
                            ],
                            color: Colors.grey[300],
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    '',
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black),
                                  ),
                                  Text(
                                    'Naira',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Sub total',
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black),
                                  ),
                                  Text(
                                    '${formatCurrency.format(subtotal)}',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),
                              /*Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Transaction costs: 2%',
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black),
                                  ),
                                  Text(
                                    '${formatCurrency.format(0.00 * subtotal)}',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              ),*/
                              SizedBox(
                                height: 8,
                              ),
                              Divider(
                                color: Colors.grey[700],
                                height: 1,
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    'Total payment',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    //'${formatCurrency.format(subtotal + (0.00 * subtotal))}',
                                      '${formatCurrency.format(subtotal + (0.00 * subtotal))}',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              SizedBox(height: 8),
                              SizedBox(
                                height: 20,
                              ),

                              //BUTTON TO CHECKOUT STARTS FROM HERE
                              new SizedBox(
                                  width: double.infinity,
                                  height: 42,
                                  child: new RaisedButton.icon(
                                    onPressed: () {
                                      if (myCartDetails != null && myCartDetails.data.length > 0) {
                                        if (!pr.isShowing()) {
                                          pr.show();
                                          for (int i = 0; i < myCartDetails.data.length; i++) {
                                            updateCart(myCartDetails.data[i].id, myCartDetails.data[i].sponsorUnit, myCartDetails.data.length, i, context);
                                          }
                                        } else Navigator.pop(context);
                                      } else {
                                        Scaffold.of(context).showSnackBar(SnackBar(
                                          content: Text('Your cart is empty, add new item before checkout...'),
                                          duration: Duration(seconds: 4),
                                        ));
                                      }
                                    },
                                    icon: Container(width: 0, height: 0,),
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                                    label: Text('Proceed to checkout', style: TextStyle(fontSize: 16), ),
                                    color: greenStart,
                                    elevation: 7,
                                  )),
                              SizedBox(height: 16),
                            ],
                          )),
                    ],
                  ),
                ],
              ),
        ));
  }

  updateCart(int cartid, int sponsorunit, int totalSize, currentPos, BuildContext context) async {
    print("carid: " + cartid.toString() + " sponsorUnit: " + sponsorunit.toString());
    final response = await http.post(
      baseUrlCrowdy + '/api/updatecart_cv',
      body: {"id": cartid.toString(), "sponsor_unit": sponsorunit.toString()},
      headers: headerAuth,
    );


    final _responseCheckout = await doCheckout(userId);

    pr.hide();
    if (response.statusCode == 200) {
     // logPaymentRelated(observer: observer, amount: subtotal, transactionId: _responseCheckout ,type: 1);

      if (currentPos == totalSize - 1) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => CheckOutPage(subtotal, myCartDetails.data, observer, _responseCheckout)));
        //MaterialPageRoute(builder: (context) => CheckOutPage(subtotal + (subtotal * 0.00), myCartDetails.data, observer)));
      }
    } else {
      print(response.body);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Failed, please try again...'),
        duration: Duration(seconds: 3),
      ));
      Navigator.pop(context);
    }
  }

  _asyncLoaderCartItem(Function callback, Function cartCallback,
          Function adjustCartCallback) =>
      new AsyncLoader(
          initState: () async => await _getListOfCart(),
          renderLoad: () => new CircularProgressIndicator(),
          renderError: ([error]) => new Text(
                'Sorry, there was an error loading. Please check internet connection',
                style: TextStyle(color: Colors.black),
              ),
          renderSuccess: ({data}) {
            try {
              return FirstLiving(
                cartDetailsFromJson(data),
                callback,
                cartCallback,
                adjustCartCallback,
              );
            } catch (e) {
              return Center(
                child: Text('Cart empty, add new item from sponsorshop', style: TextStyle(color: Colors.grey[600]),),
              );
            }
          });

  _getListOfCart() async {
    final response = await http.get(
      baseUrl + '/api/getcartwithdetails/' + userId.toString(),
      headers: headerAuth
    );
    return response.body;
  }
}

class FirstLiving extends StatefulWidget {
  final Function callback, cartCallback, adjustCartCallback;
  final CartDetails cartDetails;
  FirstLiving(this.cartDetails, this.callback, this.cartCallback,
      this.adjustCartCallback);

  @override
  FirstLivingState createState() => new FirstLivingState(
      cartDetails, callback, cartCallback, adjustCartCallback);
}

class FirstLivingState extends State<FirstLiving> {
  Function callback, cartCallback, adjustCartCallback;
  CartDetails cartDetails;
  FirstLivingState(this.cartDetails, this.callback, this.cartCallback,
      this.adjustCartCallback);
  int doSubTotal = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (cartDetails.data != null) {
      new Timer(new Duration(seconds: 1), () async {
        for (Datum datum in cartDetails.data) {
          doSubTotal =
              doSubTotal + (datum.sponsorUnit * datum.farmDetails.pricePerunit);
        }
        this.widget.callback(doSubTotal, true, 0);
        this.widget.cartCallback(cartDetails, 0, 1);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return _buildCartItem(
        cartDetails, callback, cartCallback, adjustCartCallback);
  }
}

Widget _buildCartItem(CartDetails listOfFarmCart, Function callback,
    Function cartCallback, Function adjustCartCallback) {
  if (listOfFarmCart.data != null && listOfFarmCart.data.length > 0) {
    return ListView.builder(
        itemCount: listOfFarmCart.data.length,
        semanticChildCount: 1,
        itemBuilder: (BuildContext _context, int i) {
          return BuildingRow(listOfFarmCart.data[i], callback, cartCallback,
              adjustCartCallback, i);
        });
  } else {
    return Center(
      child: Text(
        'Cart empty, add new item from sponsorshop',
        style: TextStyle(color: Colors.grey[600]),
      ),
    );
  }
}

class BuildingRow extends StatefulWidget {
  final Datum datum;
  final Function callback, cartCallback, adjustCartCallback;
  final int pos;
  BuildingRow(this.datum, this.callback, this.cartCallback,
      this.adjustCartCallback, this.pos);

  @override
  BuildingRowState createState() => new BuildingRowState(datum, pos);
}

class BuildingRowState extends State<BuildingRow> {
  Datum farmcart;
  int pos;
  BuildingRowState(this.farmcart, this.pos);
  int stateSponsoredunits = 0;
  int priceOfFarmToBuy = 0;
  int deleted = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stateSponsoredunits = farmcart.sponsorUnit;
    priceOfFarmToBuy = stateSponsoredunits * farmcart.farmDetails.pricePerunit;
    deleted = 0;
  }

  @override
  Widget build(BuildContext context) {
    if (deleted == 1) {
      return Container(
        width: 0,
        height: 0,
      );
    } else {
      return Builder(
          builder: (context) => GestureDetector(
                onTap: () {},
                child: Card(
                    margin: EdgeInsets.fromLTRB(0, 1, 0, 0),
                    elevation: 0,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          //Image.network(farmcart.farmDetails.farmImage, width: 70, height: 70,),
                          new Container(
                            width: 80.0,
                            height: 80.0,
                            margin: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 0),
                            // margin: EdgeInsets.fromLTRB(16, 450, 16, 8),
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                  color: Colors.grey[300],
                                  offset: Offset(0.01, 0.00),
                                  blurRadius: 1.0,
                                ),
                              ],
                              color: Colors.white,
                            ),
                            child: Image.network(
                              baseUrl +
                                  '/storage/farmimages/' +
                                  farmcart.farmDetails.farmImage,
                              fit: BoxFit.cover,
                            ),
                          ),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                height: 16,
                              ),
                              Text(
                                farmcart.farmtype[0].toUpperCase() +
                                    farmcart.farmtype.substring(1),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                farmcart.farmDetails.slug,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Text(
                                'NGN ${formatCurrency.format(farmcart.farmDetails.pricePerunit)}/unit',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                farmcart.farmDetails.roi.toString() +
                                    '% returns in ' +
                                    farmcart.farmDetails.farmCycle.toString() +
                                    ' months',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                'NGN ${formatCurrency.format(priceOfFarmToBuy)}',
                                style: TextStyle(fontSize: 16, color: darkGreen),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        if (farmcart.sponsorUnit > 0) {

                                          this.widget.callback(farmcart.farmDetails.pricePerunit, false, 0);
                                          this.widget.adjustCartCallback(pos, 3, 1);

                                          farmcart.sponsorUnit = farmcart.sponsorUnit - 1;
                                          stateSponsoredunits = farmcart.sponsorUnit;
                                          priceOfFarmToBuy = stateSponsoredunits * farmcart.farmDetails.pricePerunit;

                                        }
                                      });
                                    },
                                    child: Container(
                                        width: 50,
                                        height: 40,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Colors.grey[200],
                                        ),
                                        child: Center(
                                          child: Text(
                                            '-',
                                            style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.black),
                                            textAlign: TextAlign.start,
                                          ),
                                        )),
                                  ),
                                  Container(
                                      width: 50,
                                      height: 40,
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Colors.white,
                                      ),
                                      child: Center(
                                        child: Text('$stateSponsoredunits',
                                            style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.black),
                                            textAlign: TextAlign.center),
                                      )),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        if (farmcart.sponsorUnit <= farmcart.farmDetails.unitLeft - 1) {
                                          this.widget.callback(farmcart.farmDetails.pricePerunit, true, 0);
                                          this.widget.adjustCartCallback(pos, 2, 1);

                                          farmcart.sponsorUnit = farmcart.sponsorUnit + 1;
                                          stateSponsoredunits = farmcart.sponsorUnit;
                                          priceOfFarmToBuy = stateSponsoredunits * farmcart.farmDetails.pricePerunit;
                                        }
                                      });
                                    },
                                    child: Container(
                                        width: 50,
                                        height: 40,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Colors.grey[200],
                                        ),
                                        child: Center(
                                          child: Text(
                                            '+',
                                            style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.black),
                                            textAlign: TextAlign.start,
                                          ),
                                        )),
                                  ),
                                ],
                              ),
                              Divider(
                                color: Colors.grey[400],
                              )
                            ],
                          ),
                          IconButton(
                              icon: Icon(
                                Icons.cancel,
                                size: 24,
                              ),
                              onPressed: () {
                               showDialog(context: context, builder: (context){
                                 return AlertDialog(
                                   title: Text("Delete item from cart",),
                                   content: Text("Are you sure you want to delete this item from your cart?",),
                                   actions: <Widget>[
                                     FlatButton(child: Text(
                                       "Cancel",
                                       style: TextStyle(
                                           color: Colors.white, fontSize: 16),
                                     ),
                                       onPressed: () => Navigator.pop(context),
                                       color: Colors.grey,),

                                     FlatButton(
                                       child: Text(
                                         "Delete",
                                         style: TextStyle(
                                             color: Colors.white, fontSize: 16),
                                       ),
                                       onPressed: () {
                                         Navigator.pop(context);
                                         //this.widget.callback((farmcart.sponsorUnit * farmcart.farmDetails.pricePerunit), false, farmcart.farmDetails.id);
                                         pr.style(message:"Deleting...");
                                         pr.show();
                                         _doRemoveItem(context, farmcart.id, (farmcart.sponsorUnit * farmcart.farmDetails.pricePerunit));
                                       },

                                       color: Colors.red,
                                     )
                                   ],
                                 );
                               });

                              })
                        ],
                      ),
                    )),
              ));
    }
  }

  _doRemoveItem(BuildContext context, int cartid, totalPrice) async {
    final response = await http.post(
      baseUrl + '/api/removecartitem',
      body: {"id": cartid.toString()},
      headers: headerAuth
    );
    if (pr.isShowing()) {
      pr.hide();
    }
    if (response.statusCode == 200) {
      setState(() {
        deleted = 1;
        this.widget.adjustCartCallback(pos, totalPrice, 2);
      });
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Successful...'),
        duration: Duration(seconds: 2),
      ));
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Failed, please try again...'),
        duration: Duration(seconds: 4),
      ));
    }
  }

}
