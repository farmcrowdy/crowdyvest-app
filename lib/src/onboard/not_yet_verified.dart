import 'package:flutter/material.dart';

class NotYetVerified extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.black,),
        onPressed: ()=>Navigator.pop(context),),
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text("Verify your account", style: TextStyle(color: Colors.black),),
      ),
      body: SafeArea(
          minimum: EdgeInsets.all(16),
          child:  Center(child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.security, color: Colors.black, size: 100,),
          SizedBox(height: 16,),
          Text("You're yet to verify your account. Please check your email for the verify link and try logging in again afterwards",
          textAlign: TextAlign.center, style: TextStyle(fontSize: 16, color: Colors.black),)
        ],
      ))),
    );
  }

}