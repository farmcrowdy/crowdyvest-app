import 'package:crowdyvest/src/analytics/actions_lytics.dart';
import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import '../colors.dart';
import '../models/facebookpojo.dart';
import 'package:encrypt/encrypt.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import '../webpage.dart';
import 'package:http/http.dart' as http;
import 'registration_success.dart';
import 'package:progress_dialog/progress_dialog.dart';
import '../models/loginpojo.dart';
import '../models/loginpojo2.dart';
import '../dashboard/dashboard.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';


final _firstNameController = new TextEditingController();
final _lastNameController = new TextEditingController();
final _emailController = new TextEditingController();
final _passwordController = new TextEditingController();
final FacebookLogin facebookSignIn = new FacebookLogin();
ProgressDialog pr;

class RegisterPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;
  RegisterPage(this.observer);

  @override
  RegisterPageState createState()=> new RegisterPageState(observer);

}

class RegisterPageState extends State<RegisterPage> with RouteAware {

  final FirebaseAnalyticsObserver observer;

  RegisterPageState(this.observer);


  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    lyticsSignUp(observer);
  }

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: ['email'],
  );

  saveOffline(String body) async {

    try {
      final fcLogin = fcLoginFromJson(body);
      SharedPreferences prefs = await SharedPreferences.getInstance();

      final encrypter = new Encrypter(new Salsa20('vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
      String encryptedText = encrypter.encrypt(_passwordController.text);


      print('encrypted: '+encryptedText);
      await prefs.setString("token", encryptedText);
      await prefs.setInt('loginStatus', 1);
      await prefs.setInt('userid', fcLogin.data.id);
      await prefs.setString("fname", fcLogin.data.fullName.split(" ").first);
      await prefs.setString("sname", fcLogin.data.fullName.split(" ").last);
      await prefs.setString("email", fcLogin.data.email);

      if(fcLogin.data.profile != null) {
        if(fcLogin.data.profile.propix !=null) {
          await prefs.setString("propix", fcLogin.data.profile.propix);
        }
        else await prefs.setString("propix", "");
      }
      else await prefs.setString("propix", "");
      print("Login success");
    }

    catch(e) {

      final fcLogin = fcLogin2FromJson(body);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final encrypter = new Encrypter(new Salsa20('vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
      String encryptedText = encrypter.encrypt(_passwordController.text);
      print('encrypted: '+encryptedText);

      await prefs.setString("token", encryptedText);
      await prefs.setInt('loginStatus', 1);
      await prefs.setInt('userid', fcLogin.data.id);
      await prefs.setString("fname", fcLogin.data.fullName.split(" ").first);
      await prefs.setString("sname", fcLogin.data.fullName.split(" ").last);
      await prefs.setString("email", fcLogin.data.email);
      await prefs.setString("propix", "");
      print("Login failed");
    }
  }

  _login(BuildContext contexter) async {
    if(await facebookSignIn.isLoggedIn) facebookSignIn.logOut();
    facebookSignIn.loginBehavior = FacebookLoginBehavior.nativeWithFallback;
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        ProgressDialog pd = new ProgressDialog(context);
        if(!pd.isShowing()) {
          pd.style(message:"Authenticating...");
          pd.show();
          print("accesstoken:  "+accessToken.token);
          final graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token='+accessToken.token);
          final profile = facebookParseFromJson(graphResponse.body);

          pd.hide();
          if(profile.email == null || profile.email.isEmpty) {
            Scaffold.of(contexter).showSnackBar(SnackBar(
              content: Text('Email not provided. Please go to your Facebook settings and allow email for Farmcrowdy App'), duration: Duration(seconds:TOAST_LENGTH),));

          }
         else socialMediaLogin(profile.name, profile.email, contexter);
        }

        break;

      case FacebookLoginStatus.cancelledByUser:
        print("facebook cancelled ");
        break;
      case FacebookLoginStatus.error:
        print("facebook error: "+result.errorMessage.toString());
        break;
    }
  }


  Future<void> _handleSignIn(BuildContext context) async {
    try {
      if( await _googleSignIn.isSignedIn()) {
        await _googleSignIn.signOut();
      }
      var success = await _googleSignIn.signIn();
      String name = success.displayName;
      socialMediaLogin(name, success.email, context);

    } catch (error) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Something went wrong: '+error.toString()), duration: Duration(seconds:4),));

      print('Something went wrong for $error');
    }
  }


  socialMediaLogin(String name,String email, BuildContext context ) async {
    print("Social email: "+email);
    ProgressDialog dialog = new ProgressDialog(context);
    dialog.style(message:"Logging in...");
    if(!dialog.isShowing()) {
      dialog.show();
      final response = await http.post(
        baseUrl+'/api/login_social',
        body: {"email": email, "password": email},
        headers: headerAuth
      );
      dialog.hide();

      print("Login Message: " + response.body);

      if(response.statusCode == 200) {
        await saveOffline(response.body);
        Navigator.push(context, (MaterialPageRoute(builder: (context)=>DashboardPageState(observer))));
      }
      else if(response.statusCode == 404) {
        socialMediaSignUp(name, email, context);
      }
    }
  }

  socialMediaSignUp(String name,String email, BuildContext context) async{
    ProgressDialog dialog = new ProgressDialog(context,);
    dialog.style(message:"Securing...");
    if(!dialog.isShowing()) {
      dialog.show();
      final response = await http.post(
        baseUrl+'/api/signup_social',
        body: {"name":name, "email": email, "password": email},
        headers: headerAuth,
      );
      dialog.hide();

      print("Login Message: " + response.body);

      if(response.statusCode == 200) {
        await saveOffline(response.body);
        Navigator.push(context, (MaterialPageRoute(builder: (context)=>DashboardPageState(observer))));
      }
      else if(response.statusCode == 404) {
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Something went wrong. Please use the login form to login or reset password'), duration: Duration(seconds:4),));
      }
    }
  }



  bool _obscureText = true;
  String _password;
  // Toggles the password show status

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  doRegister(BuildContext contexter, AnimationController controller) async {
    String fullnameConcat = _firstNameController.text.replaceAll(" ", '') +" "+
        _lastNameController.text.replaceAll(" ", '');
    if (fullnameConcat.trim().length < 4) {
      Scaffold.of(contexter).showSnackBar(SnackBar(
        content: Text('Name too short, please review...'),
        duration: Duration(seconds: 3),));
    }
    else if (!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(
        _emailController.text.trim())) {
      Scaffold.of(contexter).showSnackBar(SnackBar(
        content: Text('Email incorrect, please review...'),
        duration: Duration(seconds: 3),));
    }
    else if (_passwordController.text.trim().length < 5) {
      Scaffold.of(contexter).showSnackBar(SnackBar(
        content: Text('Password too short, minimum of 6 chars...'),
        duration: Duration(seconds: 3),));
    }
    else{

      controller.forward();

      final response = await http.post(
        baseUrl + '/api/register',

        body: {
          "email": _emailController.text,
          "name": fullnameConcat.trim(),
          "password": _passwordController.text
        },

        headers: headerAuth,
      );

      controller.reverse();
      if (response.statusCode == 200) {
        Scaffold.of(contexter).showSnackBar(SnackBar(
          content: Text('Registration successful...'),
          duration: Duration(seconds: 3),));

        signUpLytics(observer, "email form");
        Route route = MaterialPageRoute(builder: (context) => RegistrationSuccess());
        Navigator.pushReplacement(contexter, route);
      }
      else {
        print("REG ERROR IS: "+response.body);

        Scaffold.of(contexter).showSnackBar(SnackBar(
        //  content: Text('Failed, please try again...'),
          content: Text(response.body),
          duration: Duration(seconds: 4),));
      }

  }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    observer.unsubscribe(this);
    if(pr !=null) {
      pr.hide();
      pr = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.black,), onPressed: ()=>Navigator.pop(context)),
        backgroundColor: Colors.white, brightness: Brightness.light, elevation: 0,),
        backgroundColor: Colors.white,
        body: Builder(builder: (contexter)=> SingleChildScrollView(
            child:  Container(padding: EdgeInsets.all(32), child:

            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text('Create a free account', style: TextStyle(color: darkGreen, fontWeight: FontWeight.bold, fontSize: 26)),
                              SizedBox(height: 16.0),
                              TextField(style: TextStyle(fontSize: 18, color: Colors.black),controller: _firstNameController, decoration: InputDecoration(prefixIcon:new Icon(Icons.account_circle, color: Colors.grey,), labelText: 'First name', labelStyle: TextStyle(color: Colors.grey, fontSize: 14)), obscureText: false),

                              SizedBox(height: 16.0),
                              TextField(style: TextStyle(fontSize: 18, color: Colors.black),controller: _lastNameController, decoration: InputDecoration(prefixIcon:new Icon(Icons.account_box, color: Colors.grey,), labelText: 'Last name',labelStyle: TextStyle(color: Colors.grey, fontSize: 14), errorMaxLines: 1), obscureText: false),
                              SizedBox(height: 16.0),
                              TextField(style: TextStyle(fontSize: 18, color: Colors.black),controller: _emailController, decoration: InputDecoration(prefixIcon:new Icon(Icons.mail, color: Colors.grey,), labelText: 'Email address',labelStyle: TextStyle(color: Colors.grey, fontSize: 14)), obscureText: false),

                              SizedBox(height: 16.0),
                TextFormField(
                  decoration:  InputDecoration(
                      labelText: 'Password',
                      labelStyle: TextStyle(color: Colors.grey, fontSize: 14),
                      suffixIcon: IconButton(icon: Icon(_obscureText ? Icons.visibility: Icons.visibility_off), color: Colors.grey, onPressed: _toggle,),
                      prefixIcon: Icon(Icons.lock, color: Colors.grey,)),
                  validator: (val) => val.length < 6 ? 'Password too short.' : null,
                  onSaved: (val) => _password = val,
                  controller: _passwordController,

                  style: TextStyle(fontSize: 18, color: Colors.black), textInputAction: TextInputAction.done,
                  obscureText: _obscureText,
                ),

                              SizedBox(height: 32.0),
                              GestureDetector(onTap: () {
                                //_launchURL("http://www.farmcrowdy.com/terms-of-use/");
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>MyWebPage("https://www.farmcrowdy.com/terms-of-sponsorship/", "Terms of sponsorhip")
                                ));
                              }, child:
                              Text('By signing up, you agree with our terms and conditions',textAlign: TextAlign.center, style: TextStyle(color: Colors.grey,),),),

                              SizedBox(height: 8,),

                          new SizedBox(
                            width: double.infinity,
                            height: 48,
                              child:
                              ProgressButton(
                                progressIndicatorSize: 26,
                                progressIndicatorColor: Colors.white,
                                color: darkGreen,
                                borderRadius: BorderRadius.circular(8),
                                child: Text(
                                  "Register",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16,
                                  ),
                                ),
                                onPressed: (AnimationController controller) {
                                  doRegister(contexter, controller);
                                },
                              ),


                          ),
                              SizedBox(height: 16,),

                              SizedBox(height: 4,),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:  MainAxisAlignment.start,
                                children: <Widget>[
                                  Text('Create an account with'),
                                  SizedBox(width: 16,),
                                  GestureDetector(onTap: () {
                                    //_login(contexter);
                                  }, child: Image.asset('assets/facebook_logo.png', width: 50, height: 50,),),

                                  SizedBox(width: 8,),
                                  GestureDetector(onTap: () {
                                    //_handleSignIn(contexter);

                                  }, child: Image.asset('assets/google_logo.png', width: 45, height: 45,),),

                                ],




                ),
                SizedBox(height: 16,),


                GestureDetector(onTap: () {
                  Navigator.pop(context);
                }, child:
                Align(alignment: Alignment.center, child: Text('Log into your account', style: TextStyle(fontSize: 16, decoration: TextDecoration.underline,   color: Colors.grey, ),),),),
                SizedBox(height: 100,)

              ],
            ))
        )
        )
    );

  }


}
