import 'package:flutter/material.dart';
import '../models/vscontrolpojo.dart';
import '../colors.dart';
import 'dart:io' show Platform;
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/screensLogging.dart';

class UpdateYourApp extends StatelessWidget {
  final VsControlPojo _vsControlPojo;
  final FirebaseAnalyticsObserver observer;

  UpdateYourApp(this._vsControlPojo, this.observer);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    //Send Analytics info to Firebase Analytis.
    observer.analytics.setCurrentScreen(screenName: "force_update_app");

    return Scaffold(
    backgroundColor: Colors.white,
      appBar: AppBar(brightness: Brightness.light, backgroundColor: Colors.white, elevation: 0,),
      body: SafeArea(minimum: EdgeInsets.all(32),
         child: Column(
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisAlignment: MainAxisAlignment.spaceAround,
           children: <Widget>[
             Column(
               crossAxisAlignment: CrossAxisAlignment.center,
             mainAxisAlignment: MainAxisAlignment.center,
             children: <Widget>[

               Image.asset("assets/update_app_illus.png", width: 300, height: 300,),
               Text(_vsControlPojo.topic, style: TextStyle(color:Colors.black, fontWeight: FontWeight.bold ,fontSize: 24), textAlign: TextAlign.center,),
               SizedBox(height: 16,),
               Text(_vsControlPojo.content, style: TextStyle(color: Colors.black, fontSize: 16), textAlign: TextAlign.center,),

             ],
         ),
             SizedBox(width: double.infinity,
                   height: 48, child: RaisedButton.icon(onPressed: () {
                     if (Platform.isAndroid) {_launchURL(_vsControlPojo.linkAndroid, observer);}
                     else if (Platform.isIOS) {_launchURL(_vsControlPojo.linkIos, observer);}
                 },
                   icon: Icon(Icons.update, color: Colors.white,),
                   label: Text("Update Now", style: TextStyle(color: Colors.white, fontSize: 16),),
                   color: darkGreen,))
           ],
         ),
      ),
    );
  }

}

_launchURL(String url, FirebaseAnalyticsObserver observer) async {
  lyticsUpdateApp(observer);
  await launch(url);
}