import 'package:crowdyvest/src/engine/auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';

var _passwordController = new TextEditingController();
var _confirmPasswordController = new TextEditingController();
class ResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(leading: IconButton(icon: Icon(Icons.keyboard_backspace, color: Colors.black,),
          onPressed: ()=>Navigator.pop(context)), backgroundColor: Colors.white, brightness: Brightness.light,
      title: Text("Reset Password", style: TextStyle(color: Colors.black), ), elevation: 0,),

      body: Builder(builder: (context)=>SafeArea(
        minimum: EdgeInsets.all(16),
        child:Column(children: <Widget>[
          TextField(style: TextStyle(fontSize: 18, color: Colors.black),
            controller: _passwordController, decoration: InputDecoration(icon:new Icon(Icons.lock, color: Colors.grey,),
                labelText: 'Enter New Password', alignLabelWithHint: true, labelStyle: TextStyle(color: Colors.grey, fontSize: 14) ), obscureText: true,
            textAlign: TextAlign.start, ),

          SizedBox(height: 16,),

          TextField(style: TextStyle(fontSize: 18, color: Colors.black),
            controller: _confirmPasswordController, decoration: InputDecoration(icon:new Icon(Icons.vpn_lock, color: Colors.grey,),
                labelText: 'Confirm Password', alignLabelWithHint: true, labelStyle: TextStyle(color: Colors.grey, fontSize: 14) ), obscureText: true, textAlign: TextAlign.start, ),

          SizedBox(height: 32,),

          RaisedButton.icon(onPressed: () =>doReset(context), icon: SizedBox(height: 0, width: 0,), label: Text("Reset Now"))

      ],),
      ) )
    );
  }

}

void doReset(BuildContext buildContext) async {
  if(_passwordController.text == _confirmPasswordController.text) {
    ProgressDialog _progressDialog = new ProgressDialog(buildContext, );
    _progressDialog.style(message:"Please wait...");
    _progressDialog.show();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String userid = prefs.get("userid").toString();
    http.post("https://crowdyvest.com/api/resetpassword/"+userid.toString(),
        body:  {"password": _confirmPasswordController.text},
        headers: headerAuth)

        .then((response){
      _progressDialog.hide();
      if(response.statusCode == 200) {
        Scaffold.of(buildContext).showSnackBar(SnackBar(content: Text('Password reset successful...'), duration: Duration(seconds: 2),));
        new Timer(new Duration(seconds: 3), () async { Navigator.pop(buildContext); });
      }
      else {
        Scaffold.of(buildContext).showSnackBar(SnackBar(content: Text('Something went wrong, Please try again later...'), duration: Duration(seconds: 2),));

      }

    }).catchError((myErr) {
      _progressDialog.hide();
      Scaffold.of(buildContext).showSnackBar(SnackBar(content: Text('No internet connection detected...'), duration: Duration(seconds: 2),));
    });


  }
  else {
    Scaffold.of(buildContext).showSnackBar(SnackBar(content: Text('Password does not match, please try again...'), duration: Duration(seconds: 2),));
  }
}



