import 'dart:async';

import 'package:crowdyvest/src/engine/auth.dart';
import 'package:crowdyvest/src/onboard/not_yet_verified.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:progress_dialog/progress_dialog.dart';
import '../colors.dart';
import '../dashboard/dashboard.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../models/loginpojo.dart';
import '../models/loginpojo2.dart';
import 'register.dart';
import '../webpage.dart';
import '../models/facebookpojo.dart';
import 'package:encrypt/encrypt.dart';
import 'dart:io';
import '../models/fcLoginNoProfilePojo.dart';
import '../models/consent_pojo.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_analytics/observer.dart';
import '../analytics/actions_lytics.dart';

final _usernameController = new TextEditingController();
final _passwordController = new TextEditingController();
final FacebookLogin faebookSignIn = new FacebookLogin();
final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

ProgressDialog dialog;

setupPushNotification() {
  _firebaseMessaging.subscribeToTopic("everyone_2");

  if (Platform.isIOS) iOS_Permission();

  _firebaseMessaging.getToken().then((token) {
    print('My token:  ' + token);
  });

  _firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
    },
    onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
    },
  );
}

void iOS_Permission() {
  _firebaseMessaging.requestNotificationPermissions(
      IosNotificationSettings(sound: true, badge: true, alert: true));
  _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings) {
    print("Settings registered: $settings");
  });
}

class MyConsentPage extends StatelessWidget {
  final String _fcLogin, _password, _consentContent;
  final FirebaseAnalyticsObserver observer;
  MyConsentPage(this._fcLogin, this._password, this._consentContent, this.observer);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_backspace,
              color: Colors.black,
            ),
            onPressed: () => Navigator.pop(context),
          ),
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(
            "Terms Of Service",
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Text(
          _consentContent,
          style: TextStyle(height: 1.2, fontSize: 15, color: Colors.black),
        )),
        bottomNavigationBar: Container(
          width: double.infinity,
          height: 48,
          color: Colors.white,
          child: ProgressButton(
            progressIndicatorSize: 26,
            progressIndicatorColor: Colors.white,
            color: darkGreen,
            child: Text(
              "I agree",
              style: TextStyle(
                color: Colors.white,
                fontSize: 24,
              ),
            ),
            onPressed: (AnimationController controller) {
              doAgreeToConsent(context, controller, observer);
            },
          ),
        ));
  }

  doAgreeToConsent(BuildContext context, AnimationController controller, FirebaseAnalyticsObserver observer) async {
    controller.forward();
    final myFcLogin = fcLogin2FromJson(_fcLogin);
    final response = await http.post(
      baseUrl + '/api/consent/' + myFcLogin.data.id.toString(),
      body: {
        "consent": "1",
      },
      headers: headerAuth,
    );

    print("From consent is to check if it got in here ");
    print("From consent is: " + response.statusCode.toString());

    if (response.statusCode == 200) {
      await otherType(myFcLogin, _password);
      controller.reverse();
      await Navigator.pushReplacement(context, (MaterialPageRoute(builder: (context) => DashboardPageState(observer))));
    } else {
      controller.reverse();
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Something went wrong. Please check your internet connection and try again.'), duration: Duration(seconds: 2),
      ));
    }
  }
}

Future<int> doCheckConsent(String body) async {
  final fcLogin = fcLoginFromJsonNoPojo(body);
  return fcLogin.data.consent;
}

Future<bool> saveOffline(String body, String password) async {
   bool isVerified = true;
  try {
    final fcLogin = fcLoginFromJson(body);
    //isVerified = fcLogin.data.verified == 1;
   // if(!isVerified) return isVerified;
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final encrypter = new Encrypter(new Salsa20('vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
    String encryptedText = encrypter.encrypt(password);

    print('encrypted: ' + encryptedText);
    await prefs.setString("token", encryptedText);
    await prefs.setInt('loginStatus', 1);
    await prefs.setInt('userid', fcLogin.data.id);

    if (fcLogin.data.fullName != null && fcLogin.data.fullName.isNotEmpty) {
      await prefs.setString("fname", fcLogin.data.fullName);
      await prefs.setString("sname", "");
    } else {
      await prefs.setString("fname", fcLogin.data.firstName);
      await prefs.setString("sname", "");
    }

    await prefs.setString("email", fcLogin.data.email);

    if (fcLogin.data.profile != null) {
      if (fcLogin.data.profile.propix != null) {
        await prefs.setString("propix", fcLogin.data.profile.propix);
      } else
        await prefs.setString("propix", "");
    } else {
      await prefs.setString("propix", "");
      print("Login success");
    }
    await setupPushNotification();
  } catch (e) {
    final fcLogin = fcLogin2FromJson(body);
    //isVerified = fcLogin.data.verified == 1;
    //if(!isVerified) return isVerified;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final encrypter =
        new Encrypter(new Salsa20('vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
    String encryptedText = encrypter.encrypt(password);
    print('encrypted: ' + encryptedText);

    await prefs.setString("token", encryptedText);
    await prefs.setInt('loginStatus', 1);
    await prefs.setInt('userid', fcLogin.data.id);
    if (fcLogin.data.fullName != null && fcLogin.data.fullName.isNotEmpty) {
      await prefs.setString("fname", fcLogin.data.fullName.split(" ").first);
      await prefs.setString("sname", "");
    } else {
      await prefs.setString("fname", fcLogin.data.first_name);
      await prefs.setString("sname", "");
    }
    await prefs.setString("email", fcLogin.data.email);
    await prefs.setString("propix", "");
    print("Login failed");
    await setupPushNotification();
  }
  return isVerified;
}

Future<String> getConsentContent() async {
  var response = await http
      .get("https://api.sheety.co/e4c2c062-92f9-4c9e-b825-f799fe4f5ca2");
  if (response.statusCode == 200) {
    final consentPojo = consentPojoFromJson(response.body);
    return consentPojo[0].consentContent;
  } else
    return "No internet connection. Please try again";
}

_asyncLoader(
    BuildContext context, int type, AnimationController controller, FirebaseAnalyticsObserver observer) async {
  controller.forward();
  await http.post(
    baseUrl + '/api/login',
    body: {
      "password": _passwordController.text,
      "email": _usernameController.text
    },
    headers: headerAuth,
  ).then((response) async {
    print(response.body);
    if (response.statusCode == 200) {
      if (await doCheckConsent(response.body) == 1) {
        bool verified = await saveOffline(response.body, _passwordController.text);
        controller.reverse();
        if(verified) Navigator.push(context, (MaterialPageRoute(builder: (context) => DashboardPageState(observer))));
        else Navigator.push(context, MaterialPageRoute(builder: (context)=>NotYetVerified()));
      } else {
        print("Yanakasa");
        String consentContent = await getConsentContent();
        controller.reverse();
        print(consentContent);
        Navigator.push(
            context,
            (MaterialPageRoute(
                builder: (context) => MyConsentPage(
                    response.body, _passwordController.text, consentContent, observer))));
      }
    } else if (response.statusCode == 404) {
      controller.reverse();
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Wrong password or email, please try again'),
        duration: Duration(seconds: 4),
      ));
    } else {}
  }).catchError((onError) {
    controller.reverse();
    Scaffold.of(context).showSnackBar(SnackBar(
      content:
          Text('Please verify your email or check your internet and try again'),
      duration: Duration(seconds: 4),
    ));
  });
}


class LoginPage extends StatefulWidget {
  final FirebaseAnalyticsObserver observer;

  LoginPage(this.observer);

  @override
  LoginPageState createState() => new LoginPageState(observer);
}

class LoginPageState extends State<LoginPage> with RouteAware {
  final FirebaseAnalyticsObserver observer;


  LoginPageState(this.observer);

  @override
  void didPush() {
    _sendCurrentTabToAnalytics();
  }

  @override
  void didPopNext() {
    _sendCurrentTabToAnalytics();
  }

  void _sendCurrentTabToAnalytics() {
    loginLytics(observer);
  }



  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      //'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  socialMediaLogin(String name, String email, BuildContext context) async {
     ProgressDialog dialog = new ProgressDialog(context);
     dialog.style(message:"Logging in...");

    if (!dialog.isShowing()) {
    //  dialog.show();
      http.post(
        baseUrl + '/api/login_social',
        body: {"email": email, "password": '@#\$89765430!!!98**'},
        headers: headerAuth,
      ).then((response) async {
        if (response.statusCode == 200) {
          loginLytics(observer);

          if (await doCheckConsent(response.body) == 1) {
            dialog.hide();

            bool verified = await saveOffline(response.body, '@#\$89765430!!!98**');

            verified = true;
            if(verified) Navigator.push(context, (MaterialPageRoute(builder: (context) => DashboardPageState(observer))));
            else Navigator.push(context, MaterialPageRoute(builder: (context)=>NotYetVerified()));
          } else {
            String consentContent = await getConsentContent();
            dialog.hide();
            Navigator.push(context, (MaterialPageRoute(builder: (context) => MyConsentPage(response.body, '@#\$89765430!!!98**', consentContent, observer))));
          }
        } else if (response.statusCode == 404) {
          dialog.hide();
          socialMediaSignUp(name, email, context);
        }
      }).catchError((onError) {
        dialog.hide();
        socialMediaSignUp(name, email, context);
      });
      dialog.hide();
    }
   // else
  }

  socialMediaSignUp(String name, String email, BuildContext context) async {
    // ProgressDialog dialog = new ProgressDialog(context, ProgressDialogType.Normal);
    dialog.style(message:"Securing...");
    if (!dialog.isShowing()) {
      dialog.show();

      http.post(
        baseUrl + '/api/signup_social',
        body: {"name": name, "email": email, "password": "@#\$89765430!!!98**"},
        headers: headerAuth,
      ).then((response) async {
        dialog.hide();

        print("Login Message: " + response.body);

        if (response.statusCode == 200) {
          signUpLytics(observer, "social media");

          setState(() {
            dialog.hide();
          });
          bool verified =  await saveOffline(response.body, '@#\$89765430!!!98**');
          verified = true;
          if(verified) Navigator.push(context, (MaterialPageRoute(builder: (context) => DashboardPageState(observer))));
          else Navigator.push(context, MaterialPageRoute(builder: (context)=>NotYetVerified()));
        } else {
          setState(() {
            dialog.hide();
          });
          Scaffold.of(context).showSnackBar(SnackBar(
            content: Text('Something went wrong. Please use the login form to login or reset password'),
            duration: Duration(seconds: TOAST_LENGTH),
          ));

          setState(() {
            dialog.hide();
          });
        }
      }).catchError((onError) {
        setState(() {
          dialog.hide();
        });
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text(
              'Something went wrong. Please use the login form to login or reset password'),
          duration: Duration(seconds: 4),
        ));
      });

      setState(() {
        dialog.hide();
      });
    }
  }

  Future<void> _handleSignIn(BuildContext context) async {
    try {
      if (await _googleSignIn.isSignedIn()) {
        await _googleSignIn.signOut();
      }

      var success = await _googleSignIn.signIn();
      String name = success.displayName;
      socialMediaLogin(name, success.email, context);
    } catch (error) {
      print('Something went wrong for $error');
    }
  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();
    _showMessage('Logged out.');
  }

  _showMessage(String message) {
    print(message);
  }

  bool _obscureText = true;

  String _password;

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  Widget progressing(int status) {
    if (status == 1) {
      return CircularProgressIndicator();
    } else {
      return Container(
        width: 0,
        height: 0,
      );
    }
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    observer.unsubscribe(this);
    dialog.hide();
    dialog = null;
  }

  @override
  Widget build(BuildContext context) {
    dialog = new ProgressDialog(context);

    _login(BuildContext context) async {
      print("name: oga");
      //if(await facebookSignIn.isLoggedIn) facebookSignIn.logOut();
      facebookSignIn.loginBehavior = FacebookLoginBehavior.nativeWithFallback;
      final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final FacebookAccessToken accessToken = result.accessToken;
          if (!dialog.isShowing()) {
            dialog.style(message:"Authenticating...");
            dialog.show();
            print("accesstoken:  " + accessToken.token);
            final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=' + accessToken.token);
            final profile = facebookParseFromJson(graphResponse.body);
            dialog.hide();
            if (profile.email == null || profile.email.isEmpty) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text('Email not provided. Please go to your Facebook settings and allow email for Farmcrowdy App'),
                duration: Duration(seconds: 4),
              ));
            } else socialMediaLogin(profile.name, profile.email, context);
          }
          break;
        case FacebookLoginStatus.cancelledByUser: print("facebook cancelled");
          break;
        case FacebookLoginStatus.error: print("facebook error: " + result.errorMessage.toString());
          break;
      }
    }

    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(
          width: 0,
          height: 0,
        ),
        brightness: Brightness.light,
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Colors.white,
      body: Builder(
          builder: (context) => SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.all(32),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 16,
                      ),
                      Image.asset(
                        "assets/just_cv_logo.png",
                        width: 100,
                        height: 100,
                      ),
                      SizedBox(height: 16.0),
                      Text(
                        "Welcome,",
                        style: TextStyle(
                            color: darkGreen,
                            fontWeight: FontWeight.bold,
                            fontSize: 26),
                      ),
                      SizedBox(height: 16.0),
                      Text(
                        'Log into your account to get started',
                        style: TextStyle(fontSize: 18, color: Colors.grey),
                      ),
                      SizedBox(height: 16.0),
                      TextField(
                          style: TextStyle(fontSize: 18, color: Colors.black),
                          controller: _usernameController,
                          decoration: InputDecoration(prefixIcon: new Icon(Icons.mail, color: Colors.grey,),
                              labelText: 'Email address',
                              labelStyle:
                                  TextStyle(color: Colors.grey, fontSize: 14)),
                          obscureText: false),
                      SizedBox(height: 16.0),
                      TextFormField(
                        decoration:  InputDecoration(
                            labelText: 'Password',
                            labelStyle:
                            TextStyle(color: Colors.grey, fontSize: 14),
                            suffixIcon: IconButton(
                              icon: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
                              color: Colors.grey,
                              onPressed: _toggle,
                            ),
                            prefixIcon: Icon(Icons.lock, color: Colors.grey,) ),
                        validator: (val) =>
                        val.length < 6 ? 'Password too short.' : null,
                        onSaved: (val) => _password = val,
                        controller: _passwordController,
                        style: TextStyle(fontSize: 18, color: Colors.black),
                        textInputAction: TextInputAction.done,
                        obscureText: _obscureText,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      GestureDetector(
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyWebPage(
                                    baseUrl + "/password/reset",
                                    "Password reset"))),
                        child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              'Forgot Password?',
                              style: TextStyle(fontSize: 15, color: greenStart),
                            )),
                      ),
                      SizedBox(height: 26.0),
                      new SizedBox(
                        width: double.infinity,
                        height: 48,
                        child: ProgressButton(
                          progressIndicatorSize: 26,
                          progressIndicatorColor: Colors.white,
                          color: darkGreen,
                          borderRadius: BorderRadius.circular(8),
                          child: Text(
                            "Login",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                          onPressed: (AnimationController controller) {
                            _asyncLoader(context, 1, controller, observer);
                          },
                        ),
                      ),
                      SizedBox(
                        height: 26,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {
                                //_login(context);
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1, color: greenStart),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: FlatButton.icon(
                                      onPressed: null,
                                      icon: Image.asset(
                                        'assets/facebook_logo.png',
                                        width: 28,
                                        height: 28,
                                      ),
                                      label: Text(
                                        "Facebook",
                                        style: TextStyle(color: Colors.black),
                                      ))),
                            ),
                          ),
                          SizedBox(
                            width: 16,
                          ),
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () {
                               // _handleSignIn(context);
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          width: 1, color: greenStart),
                                      borderRadius: BorderRadius.circular(5)),
                                  child: FlatButton.icon(
                                      onPressed: null,
                                      icon: Image.asset(
                                        'assets/google_logo.png',
                                        width: 28,
                                        height: 28,
                                      ),
                                      label: Text(
                                        "Google",
                                        style: TextStyle(color: Colors.black),
                                      ))),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 26,
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Or',
                          style: TextStyle(color: Colors.grey, fontSize: 20),
                        ),
                      ),
                      SizedBox(
                        height: 32,
                      ),
                      GestureDetector(
                        onTap: () {
                          signUpClickIntention(observer);

                          Navigator.push(
                              context,
                              (MaterialPageRoute(
                                  builder: (context) => RegisterPage(observer))));
                        },
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'Create a free account',
                            style: TextStyle(
                                fontSize: 18,
                                color: darkGreen,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  )))),
    );
  }
}


otherType(var fcLogin, String password) async {

  try {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    final encrypter = new Encrypter(new Salsa20('vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
    String encryptedText = encrypter.encrypt(password);

    print('encrypted: ' + encryptedText);
    await prefs.setString("token", encryptedText);
    await prefs.setInt('loginStatus', 1);
    await prefs.setInt('userid', fcLogin.data.id);
    await prefs.setString("fname", fcLogin.data.fullName.split(" ").first);
    await prefs.setString("sname", "");
    await prefs.setString("email", fcLogin.data.email);

    if (fcLogin.data.profile != null) {
      if (fcLogin.data.profile.propix != null &&
          fcLogin.data.fullName.isNotEmpty) {
        await prefs.setString("propix", fcLogin.data.profile.propix);
      } else
        await prefs.setString("propix", "");
    } else {
      await prefs.setString("propix", "");
      print("Login success");
    }
    await setupPushNotification();

  } catch (e) {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final encrypter =
        new Encrypter(new Salsa20('vtENob4nSBh99nhQ9yNWkTLTYgw2', 'f4rmcr0w'));
    String encryptedText = encrypter.encrypt(password);
    print('encrypted: ' + encryptedText);

    await prefs.setString("token", encryptedText);
    await prefs.setInt('loginStatus', 1);
    await prefs.setInt('userid', fcLogin.data.id);
    if (fcLogin.data.fullName != null && fcLogin.data.fullName.isNotEmpty) {
      await prefs.setString("fname", fcLogin.data.fullName.split(" ").first);
      await prefs.setString("sname", "");
    } else {
      await prefs.setString("fname", fcLogin.data.first_name);
      await prefs.setString("sname", "");
    }
    await prefs.setString("email", fcLogin.data.email);
    await prefs.setString("propix", "");
    print("Login failed");
    await setupPushNotification();
  }


}
