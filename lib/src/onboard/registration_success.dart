import 'package:flutter/material.dart';
import '../colors.dart';

class RegistrationSuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            Icon(Icons.mail, color: Colors.grey[400], size: 100,),
            Text('Signed Up Successfuly', style: TextStyle(fontSize: 22, color: Colors.grey), textAlign: TextAlign.center,),
            Text('Please check your email to activate your account. Thank you.', style: TextStyle(fontSize: 16, color: Colors.black), textAlign: TextAlign.center,),
            SizedBox(height: 16,),

            RaisedButton.icon(onPressed: ()=>Navigator.pop(context), icon: Icon(Icons.keyboard_backspace, color: Colors.white,), label: Text('Back to Login', style: TextStyle(color: Colors.white, fontSize: 18),), color: greenStart,)


          ],
        ),
      ),
    );
  }

}
