import 'dart:async';
import 'package:flutter/material.dart';
final Map<String, Item> _items = <String, Item>{};
Item itemForMessage(Map<String, dynamic> message) {
  final dynamic data = message['data'] ?? message;
  final String itemId = data['id'];
  final Item item = _items.putIfAbsent(itemId, () => Item(itemId: itemId))
    ..status = data['status'];
  return item;
}

class Item {
  Item({this.itemId});

  final String itemId;

  StreamController<Item> _controller = StreamController<Item>.broadcast();

  Stream<Item> get onChanged => _controller.stream;

  String _status;

  String get status => _status;

  set status(String value) {
    _status = value;
    _controller.add(this);
  }
}


Widget buildDialog(BuildContext context, dynamic message) {
  return AlertDialog(
    content: Text(message),
    actions: <Widget>[
      FlatButton(
        child: const Text('CLOSE'),
        onPressed: () {
          Navigator.pop(context, false);
        },
      ),
      FlatButton(
        child: const Text('SHOW'),
        onPressed: () {
          Navigator.pop(context, true);
        },
      ),
    ],
  );
}